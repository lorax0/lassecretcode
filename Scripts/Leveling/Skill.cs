using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Leveling
{
    [CreateAssetMenu(fileName = "New Skill", menuName = "Skills/Skill")]
    public class Skill : ScriptableObject
    {
        public Action OnLevelUp;
        public Action OnExperienceChange;

        public float CurrentLevel => this.currentLevel;
        public float ExperienceToPreviousLevel => this.experienceToNextLevel / this.nextLevelModifier;

        [SerializeField] private string skillName;
        [SerializeField] private string skillDescription;
        [SerializeField] private float baseExperienceToNextLevel;
        [SerializeField] private float nextLevelModifier;

        private float experienceToNextLevel;
        private float currentExperience;
        private float currentLevel;

        public void SetStartExperienceToNextLevel() => this.experienceToNextLevel = this.baseExperienceToNextLevel;

        public void IncreaseSkill(int experienceToAdd)
        {
            this.currentExperience += experienceToAdd;
            if (this.currentExperience >= this.experienceToNextLevel)
            {
                this.currentLevel++;
                this.IncreaseExperienceToNextLevel();
                this.OnLevelUp?.Invoke();
            }
            this.OnExperienceChange?.Invoke();

        }

        public float GetExperienceFillAmount()
        {
            if (this.currentLevel == 1) return this.currentExperience / this.experienceToNextLevel;
            return (this.currentExperience - this.ExperienceToPreviousLevel) / (this.experienceToNextLevel - this.ExperienceToPreviousLevel);
        }

        public void IncreaseExperienceToNextLevel() => this.experienceToNextLevel = (int)(this.experienceToNextLevel * this.nextLevelModifier);
    }
}