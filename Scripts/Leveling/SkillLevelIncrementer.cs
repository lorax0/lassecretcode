using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Leveling
{
    public class SkillLevelIncrementer : MonoBehaviour
    {
        [SerializeField] private Skill skill;

        public void IncrementSkill(int experience)
        {
            this.skill.IncreaseSkill(experience);
        }
    }
}