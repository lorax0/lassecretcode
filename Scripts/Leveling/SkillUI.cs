using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace LasSecretRPG.Leveling
{
    public class SkillUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI skillName;
        [SerializeField] private TextMeshProUGUI skillLevelText;
        [SerializeField] private Image experienceBar;

        private Skill skill;

        public void InitializeSkillUIData(Skill skill)
        {
            this.skill = skill;
            this.skill.SetStartExperienceToNextLevel();
            this.UpdateLevelText();
            this.UpdateExperienceAmount();
            this.skill.OnLevelUp += this.UpdateLevelText;
            this.skill.OnExperienceChange += this.UpdateExperienceAmount;
        }

        private void UpdateLevelText() => this.skillLevelText.text = this.skill.CurrentLevel.ToString();

        private void UpdateExperienceAmount()
        {
            var fillAmount = this.skill.GetExperienceFillAmount();
            this.experienceBar.fillAmount = fillAmount == 1f ? 0f : fillAmount;
        }

        private void OnDisable()
        {
            this.skill.OnLevelUp -= this.UpdateLevelText;
            this.skill.OnExperienceChange -= this.UpdateExperienceAmount;
        }

    }
}