using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Leveling
{
    public class SkillTree : MonoBehaviour
    {
        [SerializeField] private List<Skill> skills = new List<Skill>();
        [SerializeField] private SkillUI skillUIPrefab;

        void Start()
        {
            this.InitializeSkills();
        }

        private void InitializeSkills()
        {
            for (int i = 0; i < this.skills.Count; ++i)
            {
                SkillUI skillUI = Instantiate(this.skillUIPrefab, this.transform);
                skillUI.InitializeSkillUIData(this.skills[i]);
            }
        }
    }
}