﻿using UnityEngine;
using System.Collections;
using System;
using LasSecretRPG.Statistics.Buffs;

namespace LasSecretRPG.Statistics
{
    [Serializable]
    public class StatisticData
    {
        public Statistic Statistic => this.statistic;
        public float StartValue => this.startValue;

        [SerializeField] private Statistic statistic;
        [SerializeField] private float startValue;

        public void InitializeStatistic(IBuffable buffable)
        {
            this.statistic.Values.Add(buffable, this.startValue);
        }
    }
}