using LasSecretRPG.Items;
using LasSecretRPG.Statistics.Buffs;
using LasSecretRPG.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Statistics
{
    [CreateAssetMenu(fileName = "New Statistic", menuName = "Statistic/NewStatistic")]
    public class Statistic : ScriptableObject
    {
        public Dictionary<IBuffable, float> Values => this.values;

        private Dictionary<IBuffable, float> values = new Dictionary<IBuffable, float>();
    }
}