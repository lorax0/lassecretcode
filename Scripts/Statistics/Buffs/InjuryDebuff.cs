﻿using UnityEngine;

namespace LasSecretRPG.Statistics.Buffs
{
    [CreateAssetMenu(fileName = "NewInjuryDebuff", menuName = "Statistic/InjuryDebuff")]
    public class InjuryDebuff : Buff
    {
        public float Interval => this.interval;
        public override float BuffValue => this.damage;
        
        [SerializeField] private float interval;
        [SerializeField] private float damage;
    }
}
