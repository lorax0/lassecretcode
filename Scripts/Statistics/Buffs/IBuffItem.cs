﻿using System;
using System.Collections.Generic;

namespace LasSecretRPG.Statistics.Buffs
{
    public interface IBuffItem
    {
        IEnumerable<Buff> Buffs { get; }
    }
}
