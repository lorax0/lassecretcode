﻿using System;
using System.Collections;
using UnityEngine;

namespace LasSecretRPG.Statistics.Buffs
{
    public class BuffController : MonoBehaviour
    {
        public Buff Buff => this.buff;
        public Action OnTimeEnd;

        private Buff buff;
        private float duration = 0f;
        private float interval = 0f;
        private IBuffable buffable;

        public void InitializeBuff(Buff newBuff, IBuffable buffable)
        {
            this.buff = newBuff;
            this.buffable = buffable;
            this.duration = this.buff.Duration;
            if (newBuff is BonusBuff buff)
            {
                this.SetBuff(buff);
            }
            else if(newBuff is InjuryDebuff debuff)
            {
                this.SetDebuff(debuff);
            }
            this.StartCoroutine(this.StartTimer());
        }

        private void SetBuff(Buff buff)
        {
            float bonusStatisticValue = buff.BuffValue / 100f;
            this.buff.Statistic.Values[this.buffable] *= (1 + bonusStatisticValue);
            this.OnTimeEnd += () => this.buff.Statistic.Values[this.buffable] /= (1 + bonusStatisticValue);
        }

        private void SetDebuff(InjuryDebuff debuff)
        {
            this.interval = debuff.Interval;
            this.MakeDebuffAction(debuff);
        }

        private void MakeDebuffAction(Buff debuff) => this.buff.Statistic.Values[this.buffable] -= debuff.BuffValue;

        public void IncreaseDuration(float duration) => this.duration += duration;

        public bool CanIncreaseDuration(Buff buff) => buff.Statistic == this.buff.Statistic && buff.BuffValue == this.buff.BuffValue;

        public void EndBuff() => this.OnTimeEnd?.Invoke();

        private IEnumerator StartTimer()
        {
            if (!this.buff.HaveTime) yield break;
            while (this.duration > 0)
            {
                yield return new WaitForFixedUpdate();
                this.duration -= Time.deltaTime;
            }
            this.OnTimeEnd?.Invoke();
        }

        private void UpdateInterval()
        {
            if (this.interval < 0) this.MakeDebuffAction(this.buff);
            else this.interval -= Time.deltaTime;
        }
    }
}
