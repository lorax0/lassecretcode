﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LasSecretRPG.Statistics.Buffs
{
    [Serializable]
    public class BuffsService
    {
        public IBuffable Buffable { get => this.buffable; set => this.buffable = value; }

        [SerializeField] private BuffController buffImagePrefab;
        [SerializeField] private GameObject buffsInterface;

        private List<BuffController> availableBuffs = new List<BuffController>();
        private IBuffable buffable;

        public void AddBuffs(IEnumerable<Buff> buffs)
        {
            foreach (var buff in buffs)
            {
                this.AddBuff(buff);
            }
        }

        public void RemoveBuffs(IEnumerable<Buff> buffs)
        {
            foreach (var buff in buffs)
            {
                this.RemoveBuff(buff);
            }
        }

        public void AddBuff(Buff buff)
        {
            //BuffController similarBuffController = this.availableBuffs.Where(t => t.CanIncreaseDuration(buff)).FirstOrDefault();
            //if(similarBuffController != null)
            //{
            //    similarBuffController.IncreaseDuration(buff.Duration);
            //    return;
            //}
            BuffController buffController = MonoBehaviour.Instantiate(this.buffImagePrefab, this.buffsInterface.transform);
            this.availableBuffs.Add(buffController);
            buffController.InitializeBuff(buff, this.buffable);
            buffController.OnTimeEnd += () => this.availableBuffs.Remove(buffController);
            buffController.OnTimeEnd += () => MonoBehaviour.Destroy(buffController.gameObject);
        }

        public void RemoveBuff(Buff buff)
        {
            BuffController buffController = this.availableBuffs.FirstOrDefault(t => t.Buff == buff);
            if (buffController == null) return;
            buffController.EndBuff();
        }
    }
}
