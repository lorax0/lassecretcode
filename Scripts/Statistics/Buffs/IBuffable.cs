﻿using LasSecretRPG.Statistics;

namespace LasSecretRPG.Statistics.Buffs
{
    public interface IBuffable
    {
        BuffsService BuffsService { get; }
    }
}
