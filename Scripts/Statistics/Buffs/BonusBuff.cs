﻿using UnityEngine;

namespace LasSecretRPG.Statistics.Buffs
{
    [CreateAssetMenu(fileName = "NewBuff", menuName = "Statistic/Buff")]
    public class BonusBuff : Buff
    {
        public override float BuffValue => this.percentageBuff;
        
        [SerializeField] private float percentageBuff;
    }
}
