﻿using System;
using UnityEngine;

namespace LasSecretRPG.Statistics.Buffs
{
    public class Buff : ScriptableObject
    {
        public Statistic Statistic => this.buffableStatistic;
        public virtual float BuffValue => 0;
        public float Duration => this.buffDuration;
        public bool HaveTime => this.buffDuration != 0;

        [SerializeField] private Statistic buffableStatistic;
        [Min(0)] [SerializeField] private float buffDuration;

    }
}
