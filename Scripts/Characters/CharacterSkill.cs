using UnityEngine;
using System;

namespace LasSecretRPG.Characters
{
    [Serializable]
    public class CharacterSkill
    {
        public string SkillName => this.skillName;
        public int CurrentLevel => this.currentLevel;

        [SerializeField] private string skillName;
        [SerializeField] private int currentLevel;

    }
}