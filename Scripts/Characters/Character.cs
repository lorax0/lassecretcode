using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LasSecretRPG.Characters
{
    [CreateAssetMenu(fileName = "New Character", menuName = "Characters/Character")]
    public class Character : ScriptableObject
    {
        public GameObject Player => this.player;
        public List<CharacterSkill> CharacterSkills => this.characterSkills;
        public Sprite CharacterSprite => this.characterSprite;
        public string CharacterFeatures => this.characterFeatures;
        public string CharacterName => this.characterName;
        public string CharacterBackstory => this.characterBackstory;

        [TextArea] [SerializeField] private string characterFeatures;
        [TextArea] [SerializeField] private string characterName;
        [TextArea] [SerializeField] private string characterBackstory;
        [SerializeField] private Sprite characterSprite;
        [SerializeField] private List<CharacterSkill> characterSkills;
        [SerializeField] private GameObject player;

        private void Awake()
        {
            this.UpdateSkillsOrder();
        }

        public void UpdateSkillsOrder() => this.characterSkills = this.characterSkills.OrderBy(x => x.CurrentLevel).ToList();
    }
}