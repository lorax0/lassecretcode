using UnityEngine;
using System.Collections.Generic;
using LasSecretRPG.Player;
using TMPro;
using UnityEngine.UI;
using System.Linq;

namespace LasSecretRPG.Characters
{
    public class CharacterCreatorManager : MonoBehaviour
    {
        public Character CurrentCharacter => this.currentCharacter;

        [SerializeField] private List<Character> characters;
        [SerializeField] private TextMeshProUGUI characterFeatures;
        [SerializeField] private TextMeshProUGUI characterName;
        [SerializeField] private TextMeshProUGUI characterBackstory;
        [SerializeField] private TextMeshProUGUI characterSkills;
        [SerializeField] private TextMeshProUGUI numberOfCharacters;
        [SerializeField] private Image characterSprite;

        private Character currentCharacter;

        private void Start()
        {
            this.SetChanges(0);
        }

        public void MoveRight()
        {
            int indexOfCurrentCharacter = this.characters.IndexOf(this.currentCharacter);
            if (indexOfCurrentCharacter == this.characters.Count - 1)
            {
                this.SetChanges(0);
                return;
            }
            this.SetChanges(++indexOfCurrentCharacter);
        }

        public void MoveLeft()
        {
            int indexOfCurrentCharacter = this.characters.IndexOf(this.currentCharacter);
            if (indexOfCurrentCharacter == 0)
            {
                this.SetChanges(this.characters.Count - 1);
                return;
            }
            this.SetChanges(--indexOfCurrentCharacter);
        }

        private void SetChanges(int index)
        {
            this.currentCharacter = this.characters[index];

            this.characterFeatures.text = this.currentCharacter.CharacterFeatures;
            this.characterName.text = this.currentCharacter.CharacterName;
            this.characterBackstory.text = this.currentCharacter.CharacterBackstory;

            this.numberOfCharacters.text = this.characters.IndexOf(this.currentCharacter) + 1 + "/" + this.characters.Count;

            this.AddSkillText();
            this.characterSprite.sprite = this.currentCharacter.CharacterSprite;
        }

        private void AddSkillText()
        {
            foreach (CharacterSkill skill in this.currentCharacter.CharacterSkills)
            {
                this.characterSkills.text += skill.SkillName + " " + skill.CurrentLevel + "LV" + "\n";
            }
        }
    }
}