using LasSecretRPG.Utilities;
using LasSecretRPG.Damageable;
using System.Collections.Generic;
using UnityEngine;
using LasSecretRPG.Utilities.Time;
using Zenject;

namespace LasSecretRPG.Plant
{
    public class TreeController : Destroyable, IGrowable
    {
        public IEnumerable<GrowableLevel> GrowableLevels => this.growableLevels;
        public int GrowLevel { get => this.growLevel; set => this.growLevel = value; }

        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private List<GrowableLevel> growableLevels;

        private int dayCounter;
        private int growLevel;
        [Inject] private TimeManager timeManager;

        public override void Start()
        {
            base.Start();
            this.SubscribeGrowing();
        }

        //TODO: Tree cutting

        private void SubscribeGrowing()
        {
            this.timeManager.DayCounter.AddNewPlant(this, this.CheckGrow);
        }

        private void UnsubscribeGrowing()
        {
            this.timeManager.DayCounter.RemovePlant(this);
        }

        public void Grow()
        {
            this.dayCounter = 0;
            if (this.growableLevels.Count <= this.growLevel + 1)
            {
                this.UnsubscribeGrowing();
                return;
            }
            this.spriteRenderer.sprite = this.growableLevels[this.growLevel + 1].Sprite;
            this.growLevel++;
        }

        public void CheckGrow()
        {
            this.dayCounter++;
            GrowableLevel currentGrowableLevel = this.growableLevels[this.growLevel];
            if (this.dayCounter >= currentGrowableLevel.DayToNextGrow)
                this.Grow();
        }
    }
}