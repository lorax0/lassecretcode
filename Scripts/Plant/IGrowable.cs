﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Plant
{
    public interface IGrowable
    {
        int GrowLevel { get; set; }
        IEnumerable<GrowableLevel> GrowableLevels { get; }
    }
}