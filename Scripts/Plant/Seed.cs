﻿using LasSecretRPG.Items;
using LasSecretRPG.Player;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Plant
{
    [CreateAssetMenu(fileName = "New Seed", menuName = "Items/Seed")]
    public class Seed : Item, IUseable, IGrowable
    {
        public IEnumerable<GrowableLevel> GrowableLevels => this.growableLevels;
        public int GrowLevel { get => this.growLevel; set => this.growLevel = value; }

        [SerializeField] private List<GrowableLevel> growableLevels;

        private int growLevel;

        public void Use(PlayerController playerController)
        {
            playerController.BuildingService.Plant(this);
        }

        public bool CanUse(PlayerController playerController) => playerController.BuildingService.CanPlant();
    }
}
