﻿using LasSecretRPG.Buildings;
using LasSecretRPG.Player;
using LasSecretRPG.Utilities;
using LasSecretRPG.Utilities.Time;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace LasSecretRPG.Plant
{
    public class FarmField : BuildingController
    {
        public IGrowable Growable => this.growable;

        [SerializeField] private SpriteRenderer spriteRenderer;

        private IGrowable growable;
        private int dayCounter;
        [Inject] private TimeManager timeManager;
        
        public void Plant(IGrowable growable)
        {
            this.growable = growable;
            this.spriteRenderer.sprite = this.growable.GrowableLevels.First().Sprite;
            this.SubscribeGrowing();
        }
        
        private void SubscribeGrowing()
        {
            this.timeManager.DayCounter.AddNewPlant(this.growable, this.CheckGrow);
        }

        private void UnsubscribeGrowing()
        {
            this.timeManager.DayCounter.RemovePlant(this.growable);
        }
        
        public void Grow()
        {
            this.dayCounter = 0;
            if (this.growable.GrowableLevels.Count() <= this.growable.GrowLevel + 1)
            {
                this.UnsubscribeGrowing();
                return;
            }
            this.spriteRenderer.sprite = this.growable.GrowableLevels.ElementAt(this.growable.GrowLevel + 1).Sprite;
            this.growable.GrowLevel++;
        }

        public void CheckGrow()
        {
            this.dayCounter++;
            GrowableLevel currentGrowableLevel = this.growable.GrowableLevels.ElementAt(this.growable.GrowLevel);
            if (this.dayCounter >= currentGrowableLevel.DayToNextGrow)
                this.Grow();
        }

        public bool IsReadyToTake() => this.spriteRenderer.sprite == this.growable.GrowableLevels.Last().Sprite;
    }
}
