﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace LasSecretRPG.Plant
{
    [Serializable]
    public class GrowableLevel
    {
        public Sprite Sprite => this.sprite;
        public int DayToNextGrow => this.dayToNextGrow;
        public int HourToGrow => this.hourToGrow;

        [SerializeField] private Sprite sprite;
        [SerializeField] private int dayToNextGrow;
        [SerializeField] private int hourToGrow;
    }
}
