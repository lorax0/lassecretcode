﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace LasSecretRPG.Quest
{
    public class NodeSearchWindow : ScriptableObject, ISearchWindowProvider
    {
        protected QuestGraph questGraph;
        protected EditorWindow window;

        public void Initialize(EditorWindow window, QuestGraph questGraph)
        {
            this.questGraph = questGraph;
            this.window = window;
        }

        public List<SearchTreeEntry> CreateSearchTree(SearchWindowContext context)
        {
            var tree = new List<SearchTreeEntry>
            {
                new SearchTreeGroupEntry(new GUIContent("Create Elements"), 0),
                new SearchTreeGroupEntry(new GUIContent("Quest"), 1),
                new SearchTreeEntry(new GUIContent("Base quest node")) {userData = QuestNodeType.Default, level = 2},
                new SearchTreeEntry(new GUIContent("Item quest node")) {userData = QuestNodeType.Item, level = 2},
                new SearchTreeEntry(new GUIContent("Kill quest node")) {userData = QuestNodeType.Kill, level = 2},
                new SearchTreeEntry(new GUIContent("Waypoint quest node")) {userData = QuestNodeType.Waypoint, level = 2},

            };
            return tree;
        }

        public bool OnSelectEntry(SearchTreeEntry SearchTreeEntry, SearchWindowContext context)
        {
            var worldMousePosition = this.window.rootVisualElement.ChangeCoordinatesTo(this.window.rootVisualElement.parent, context.screenMousePosition - this.window.position.position);
            var localMousePosition = this.questGraph.contentViewContainer.WorldToLocal(worldMousePosition);
            switch (SearchTreeEntry.userData)
            {
                case QuestNodeType.Item:
                    this.questGraph.CreateNode("Item quest node", new QuestNodeData { questType = QuestNodeType.Item, Position = localMousePosition });
                    return true;
                case QuestNodeType.Kill:
                    this.questGraph.CreateNode("Enemy quest node", new QuestNodeData { questType = QuestNodeType.Kill, Position = localMousePosition });
                    return true;
                case QuestNodeType.Waypoint:
                    this.questGraph.CreateNode("Waypoint quest node", new QuestNodeData { questType = QuestNodeType.Waypoint, Position = localMousePosition });
                    return true;
                case QuestNodeType.Default:
                    this.questGraph.CreateNode("Quest node", new QuestNodeData { questType = QuestNodeType.Default, Position = localMousePosition });
                    return true;
                default:
                    return false;
            }
        }
    }
}