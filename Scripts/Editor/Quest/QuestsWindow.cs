using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace LasSecretRPG.Quest
{
    public class QuestsWindow : EditorWindow
    {
        protected QuestGraph questGraph;
        protected string fileName = "Quest graph";

        [MenuItem("Tools/QuestWindow")]
        public static void OpenWindow()
        {
            var window = GetWindow<QuestsWindow>();
            window.titleContent = new GUIContent("QuestWindow");
        }

        private void OnEnable()
        {
            this.CreateQuestGraph();
            this.GenerateToolbar();
            this.GenerateMinimap();
        }

        private void OnDisable()
        {
            this.rootVisualElement.Remove(this.questGraph);
        }

        private void GenerateMinimap()
        {
            var minimap = new MiniMap { anchored = true };
            minimap.SetPosition(new Rect(10, 30, 200, 140));
            this.questGraph.Add(minimap);
        }

        private void CreateQuestGraph()
        {
            this.questGraph = new QuestGraph(this);
            this.questGraph.name = "Quest graph";

            this.questGraph.StretchToParentSize();
            this.rootVisualElement.Add(this.questGraph);
        }
        
        private void GenerateToolbar()
        {
            var toolbar = new Toolbar();
            
            toolbar.Add(new Button(() => this.RequestDialogSaverOperation(true)) { text = "Save quest graph" });
            toolbar.Add(new Button(() => this.RequestDialogSaverOperation(false)) { text = "Load quest graph" });
            this.rootVisualElement.Add(toolbar);
        }

        private void RequestDialogSaverOperation(bool save)
        {
            if (string.IsNullOrEmpty(this.fileName))
            {
                EditorUtility.DisplayDialog("Invalid file name", "Enter valid file name", "ok");
            }

            var saver = QuestGraphSaver.GetInstance(this.questGraph);

            if (save) saver.SaveQuestGraph(this.fileName);
            else saver.LoadQuestGraph(this.fileName);
        }
    }
}