using LasSecretRPG.Enemies;
using LasSecretRPG.Items;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace LasSecretRPG.Quest
{
    public class QuestNode : Node, IQuestNode
    {
        public string Id { get => this.id; set => this.id = value; }
        public string Title => this.title;
        public Vector2 Position => this.GetPosition().position;
        public bool EntryPoint { get => this.entryPoint; set => this.entryPoint = value; }
        public QuestNodeType Type { get => this.type; set => this.type = value; }

        public Item Item => this.item;
        public int AmountOfItem => this.amountOfItem;
        public int AmountOfEnemy => this.amountOfEnemy;
        public GameObject UnitToKill => this.unitToKill;
        public GameObject Waypoint => this.waypoint;

        protected Item item;
        protected int amountOfItem;
        public int amountOfEnemy;
        public GameObject unitToKill;
        public GameObject waypoint;
        
        protected string id;
        protected bool entryPoint;
        protected QuestNodeType type;
        protected List<VisualElement> fields = new List<VisualElement>();

        public void SetQuestData(QuestNodeData nodeData, string name)
        {
            this.id = Guid.NewGuid().ToString();
            this.title = name;
            this.type = nodeData.questType;
            this.item = nodeData.Item;
            this.amountOfItem = nodeData.AmountOfItem;
            this.amountOfEnemy = nodeData.AmountOfEnemy;
            this.unitToKill = nodeData.UnitToKill;
            this.waypoint = nodeData.Waypoint;
        }

        public void SetQuestFieldsParameter(QuestNodeData nodeData, QuestNodeType type)
        {
            this.RemoveAllFields();
            switch (type)
            {
                case QuestNodeType.Item:
                    {
                        this.CreateItemDataField(nodeData);
                    }
                    break;
                case QuestNodeType.Kill:
                    {
                        this.CreateEnemyDataField(nodeData);
                    }
                    break;
                case QuestNodeType.Waypoint:
                    {
                        this.CreateWaypointDataField(nodeData);
                    }
                    break;
                default:
                    {
                        this.RemoveAllFields();
                    }
                    break;
            }

        }

        private void RemoveAllFields()
        {
            foreach (var field in this.fields)
            {
                this.mainContainer.Remove(field);
            }
            this.fields.Clear();
        }

        private void CreateItemDataField(QuestNodeData nodeData)
        {
            var itemField = new ObjectField();
            itemField.objectType = typeof(Item);
            itemField.value = nodeData.Item;
            itemField.RegisterValueChangedCallback(t => this.item = (Item)t.newValue);

            var amountSlider = new SliderInt(0, 100);
            var amountField = new TextElement();
            amountSlider.RegisterValueChangedCallback(t => { amountField.text = t.newValue.ToString(); this.amountOfItem = t.newValue; });
            amountSlider.value = this.amountOfItem;
            amountField.text = this.amountOfItem.ToString();

            this.mainContainer.Add(itemField);
            this.fields.Add(itemField);
            this.CreateSliderForQuest(amountSlider, amountField);
        }

        private void CreateEnemyDataField(QuestNodeData nodeData)
        {
            var enemyField = new ObjectField();
            enemyField.objectType = typeof(GameObject);
            enemyField.value = nodeData.UnitToKill;
            enemyField.RegisterValueChangedCallback(t => this.unitToKill = (GameObject)t.newValue);

            var amountSlider = new SliderInt(0, 100);
            var amountField = new TextElement();
            amountSlider.RegisterValueChangedCallback(t => { amountField.text = t.newValue.ToString(); this.amountOfEnemy = t.newValue; });
            amountSlider.value = this.amountOfEnemy;
            amountField.text = this.amountOfEnemy.ToString();

            this.mainContainer.Add(enemyField);
            this.fields.Add(enemyField);
            this.CreateSliderForQuest(amountSlider, amountField);
        }

        private void CreateWaypointDataField(QuestNodeData nodeData)
        {
            var waypointField = new ObjectField();
            waypointField.objectType = typeof(GameObject);
            waypointField.value = nodeData.Waypoint;
            waypointField.RegisterValueChangedCallback(t => this.waypoint = (GameObject)t.newValue);

            this.mainContainer.Add(waypointField);
            this.fields.Add(waypointField);
        }

        private void CreateSliderForQuest(SliderInt amountSlider, TextElement amountField)
        {
            this.mainContainer.Add(amountSlider);
            this.mainContainer.Add(amountField);
            this.fields.Add(amountSlider);
            this.fields.Add(amountField);
        }

    }
}