﻿using System;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;
using UnityEngine;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;

namespace LasSecretRPG.Quest
{
    public class QuestGraph : GraphView
    {
        public readonly Vector2 DefaultNodeSize = new Vector2(150, 200);
        protected NodeSearchWindow nodeSearchWindow;

        public QuestGraph(EditorWindow window)
        {
            this.SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);
            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());

            this.AddSearchWindow(window);
            this.AddElement(this.CreateBaseNode());
        }

        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            var compatiblePorts = new List<Port>();
            this.ports.ForEach(port =>
            {
                if (startPort != port && startPort.node != port.node)
                    compatiblePorts.Add(port);
            });
            return compatiblePorts;
        }

        public void CreateNode(string name, QuestNodeData nodeData) => this.AddElement(this.CreateQuestNode(name, nodeData));

        public void AddChoisePort(QuestNode node)
        {
            var port = this.CreatePort(node, Direction.Output);

            var oldLabel = port.contentContainer.Q<Label>("type");
            port.contentContainer.Remove(oldLabel);

            var outputPortCount = node.outputContainer.Query("connector").ToList().Count;

            var deleteButton = new Button(() => this.RemovePort(node, port)) { text = "X" };
            port.contentContainer.Add(new Label(" "));
            port.contentContainer.Add(deleteButton);

            node.outputContainer.Add(port);
            node.RefreshPorts();
            node.RefreshExpandedState();
        }

        public QuestNode CreateQuestNode(string name, QuestNodeData nodeData)
        {
            var questNode = new QuestNode();
            questNode.SetQuestData(nodeData, name);
            questNode.SetPosition(new Rect(nodeData.Position, this.DefaultNodeSize));

            var port = this.CreatePort(questNode, Direction.Input);
            port.portName = "Input";
            questNode.inputContainer.Add(port);

            var button = new Button(() => this.AddChoisePort(questNode));
            button.text = "Add next quest";
            questNode.titleContainer.Add(button);

            var dialogTextField = new TextField(string.Empty);
            dialogTextField.RegisterValueChangedCallback(value => { questNode.title = value.newValue; });
            dialogTextField.SetValueWithoutNotify(questNode.title);
            questNode.mainContainer.Add(dialogTextField);

            var questTypeField = new EnumField(nodeData.questType);
            questTypeField.RegisterValueChangedCallback(t => { questNode.Type = (QuestNodeType)t.newValue; questNode.SetQuestFieldsParameter(nodeData, (QuestNodeType)t.newValue); });
            questNode.mainContainer.Add(questTypeField);
            questNode.SetQuestFieldsParameter(nodeData, nodeData.questType);

            questNode.RefreshExpandedState();
            questNode.RefreshPorts();

            return questNode;
        }

        private void AddSearchWindow(EditorWindow window)
        {
            this.nodeSearchWindow = ScriptableObject.CreateInstance<NodeSearchWindow>();
            this.nodeSearchWindow.Initialize(window, this);
            this.nodeCreationRequest = t => SearchWindow.Open(new SearchWindowContext(t.screenMousePosition), this.nodeSearchWindow);
        }

        private void RemovePort(QuestNode node, Port port)
        {
            var edges = this.edges.ToList().Where(t => t.output == port && t.output.node == port.node);

            node.outputContainer.Remove(port);
            node.RefreshPorts();
            node.RefreshExpandedState();

            if (!edges.Any()) return;
            var edge = edges.First();
            edge.input.Disconnect(edge);
            this.RemoveElement(edge);
        }

        private QuestNode CreateBaseNode()
        {
            var node = new QuestNode();
            node.Id = Guid.NewGuid().ToString();
            node.title = "StartNode";
            node.EntryPoint = true;
            node.SetPosition(new Rect(0, 0, 100, 150));

            var port = this.CreatePort(node, Direction.Output);
            port.portName = "";
            node.outputContainer.Add(port);

            node.RefreshExpandedState();
            node.RefreshPorts();
            return node;
        }

        private Port CreatePort(QuestNode node, Direction direction, Port.Capacity capacity = Port.Capacity.Multi) => node.InstantiatePort(Orientation.Horizontal, direction, capacity, typeof(float));

    }
}