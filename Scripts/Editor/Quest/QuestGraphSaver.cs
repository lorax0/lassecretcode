﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace LasSecretRPG.Quest
{
    public class QuestGraphSaver
    {
        protected List<Edge> Edges => this.targetQuestGraph.edges.ToList();
        protected List<QuestNode> Nodes => this.targetQuestGraph.nodes.ToList().Cast<QuestNode>().ToList();

        protected QuestGraph targetQuestGraph;
        protected QuestContainer questCache;

        public static QuestGraphSaver GetInstance(QuestGraph questGraph) => new QuestGraphSaver { targetQuestGraph = questGraph };

        public void SaveQuestGraph(string questName)
        {
            if (!AssetDatabase.IsValidFolder("Assets/Resources/GameData/Quests")) AssetDatabase.CreateFolder("Assets/Resources/GameData", "Quests");
            this.SaveQuests();
            if (!this.Edges.Any()) return;

            var questCache = ScriptableObject.CreateInstance<QuestContainer>();

            var connectedPorts = this.Edges.Where(t => t.input.node != null);
            for (int i = 0; i < connectedPorts.Count(); i++)
            {
                var port = connectedPorts.ElementAt(i);
                var outputNode = port.output.node as QuestNode;
                var inputNode = port.input.node as QuestNode;

                questCache.NodeLinks.Add(new NodeLinkData { BaseNodeId = outputNode.Id, TargetNodeId = inputNode.Id });
            }

            foreach (var node in this.Nodes.Where(t => !t.EntryPoint))
            {
                var questNodeData = new QuestNodeData();
                questNodeData.SetNodeData(node);
                questCache.QuestNodeData.Add(questNodeData);
            }

            AssetDatabase.CreateAsset(questCache, "Assets/Resources/GameData/Quests/" + questName + ".asset");
            AssetDatabase.SaveAssets();
        }

        public void SaveQuests()
        {
            string[] questFolder = { "Assets/Resources/GameData/Quests" };
            foreach (var asset in AssetDatabase.FindAssets("", questFolder))
            {
                var path = AssetDatabase.GUIDToAssetPath(asset);
                AssetDatabase.DeleteAsset(path);
            }
            foreach (var node in this.Nodes.Where(t => !t.EntryPoint))
            {
                var questData = ScriptableObject.CreateInstance<QuestData>();
                questData.name = node.title;
                questData.Type = node.Type;
                questData.NodeId = node.Id;
                questData.Item = node.Item;
                questData.AmountOfItem = node.AmountOfItem;
                questData.AmountOfEnemy = node.AmountOfEnemy;
                questData.UnitToKill = node.UnitToKill;
                questData.Waypoint = node.Waypoint;
                AssetDatabase.CreateAsset(questData, "Assets/Resources/GameData/Quests/" + node.title + ".asset");
            }
            AssetDatabase.SaveAssets();
        }

        public void LoadQuestGraph(string questName)
        {
            this.questCache = Resources.Load<QuestContainer>("GameData/Quests/" + questName);
            if (this.questCache == null)
            {
                EditorUtility.DisplayDialog("Quest not found", "Quest is not exist", "ok");
                return;
            }
            this.ClearGraph();
            this.CreateNodes();
            this.ConnectNodes();
        }

        private void ConnectNodes()
        {
            for (int i = 0; i < this.Nodes.Count; i++)
            {
                var connections = this.questCache.NodeLinks.Where(t => t.BaseNodeId == this.Nodes[i].Id);
                for (int j = 0; j < connections.Count(); j++)
                {
                    var targetNodeId = connections.ElementAt(j).TargetNodeId;
                    var targetNode = this.Nodes.First(t => t.Id == targetNodeId);
                    this.LinkNodes(this.Nodes[i].outputContainer[j].Q<Port>(), (Port)targetNode.inputContainer[0]);

                    targetNode.SetPosition(new Rect(this.questCache.QuestNodeData.First(t => t.NodeId == targetNodeId).Position, this.targetQuestGraph.DefaultNodeSize));
                }
            }
        }

        private void LinkNodes(Port output, Port input)
        {
            var edge = new Edge { output = output, input = input };
            edge?.input.Connect(edge);
            edge?.output.Connect(edge);
            this.targetQuestGraph.Add(edge);
        }

        private void CreateNodes()
        {
            foreach (var nodeData in this.questCache.QuestNodeData)
            {
                var node = this.targetQuestGraph.CreateQuestNode(nodeData.QuestName, nodeData);
                node.Id = nodeData.NodeId;
                this.targetQuestGraph.AddElement(node);

                var nodePorts = this.questCache.NodeLinks.Where(t => t.BaseNodeId == nodeData.NodeId).ToList();
                nodePorts.ForEach(t => this.targetQuestGraph.AddChoisePort(node));
            }
        }

        private void ClearGraph()
        {
            this.Nodes.Find(t => t.EntryPoint).Id = this.questCache.NodeLinks[0].BaseNodeId;

            foreach (var node in this.Nodes)
            {
                if (node.EntryPoint) continue;
                this.Edges.Where(edge => edge.input.node == node).ToList().ForEach(edge => this.targetQuestGraph.RemoveElement(edge));
                this.targetQuestGraph.RemoveElement(node);
            }
        }
    }
}