using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace LasSecretRPG.Translation
{
    public class TranslationEditor : EditorWindow
    {
        protected List<TranslationLanguage> languages = new List<TranslationLanguage>();
        protected const string defaultLanguageName = "New language";
        protected string languageName = defaultLanguageName;

        [MenuItem("Tools/Translation")]
        public static void ShowWindow()
        {
            GetWindow<TranslationEditor>("Translation");
        }

        private void OnEnable()
        {
            if (!AssetDatabase.IsValidFolder("Assets/Resources/GameData/Translation")) AssetDatabase.CreateFolder("Assets/Resources/GameData", "Translation");
            this.LoadLanguages();
        }

        private void LoadLanguages()
        {
            this.languages.Clear();
            foreach (var language in Resources.LoadAll<TranslationLanguage>("GameData/Translation"))
            {
                this.languages.Add(language);
            }
            this.languages.OrderBy(t => t.Fields.Count);
        }

        private void OnGUI()
        {
            this.CreateLanguageButton();
            this.CreateFieldButton();
            this.CreateLanguagesFields();
        }

        private void CreateLanguagesFields()
        {
            if (this.languages.Count == 0) return;
            this.CreateLanguagesNameFields();
            for (int i = 0; i < this.languages[0].Fields.Count; i++)
            {
                this.CreateLanguagesField(i);
            }
        }

        private void CreateLanguagesNameFields()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("");
            foreach (var language in this.languages)
            {
                GUILayout.Label(language.name.ToString());
            }
            GUILayout.EndHorizontal();
        }

        private void CreateLanguageButton()
        {
            GUILayout.BeginHorizontal();
            var addNewLanguage = GUILayout.Button("Add new language");
            //var languageName = defaultLanguageName;
            this.languageName = GUILayout.TextArea(this.languageName);
            GUILayout.EndHorizontal();
            if (!addNewLanguage) return;
            this.AddNewLanguage(this.languageName);
            this.languageName = defaultLanguageName;
            this.LoadLanguages();
        }
        
        private void CreateFieldButton()
        {
            var addNewField = GUILayout.Button("Add new field");
            if (!addNewField) return;
            int languageIndex = this.languages[0].Fields.LastOrDefault() == null ? 0 : this.languages[0].Fields.LastOrDefault().Index + 1;
            foreach (var language in this.languages)
            {
                language.Fields.Add(new TranslationField(languageIndex));
            }
            this.CreateLanguagesField(languageIndex);
        }

        private void CreateLanguagesField(int index)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(index.ToString());
            foreach (var language in this.languages)
            {
                language.Fields[index].TranslationText = GUILayout.TextArea(language.Fields[index].TranslationText);
            }
            GUILayout.EndHorizontal();
        }

        private void AddNewLanguage(string languageName)
        {
            var language = ScriptableObject.CreateInstance<TranslationLanguage>();
            AssetDatabase.CreateAsset(language, "Assets/Resources/GameData/Translation/" + languageName + ".asset");
        }
    }
}