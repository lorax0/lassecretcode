﻿using System;
using System.Collections.Generic;
using UnityEditor;

namespace LasSecretRPG.UnityAttributes
{
    public class FoldoutData
    {
        public HashSet<string> types = new HashSet<string>();
        public List<SerializedProperty> properities = new List<SerializedProperty>();
        public FoldoutGroup attribute;
        public bool expanded;
    }
}
