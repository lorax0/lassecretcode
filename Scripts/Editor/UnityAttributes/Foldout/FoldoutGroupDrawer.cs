﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;
using Object = UnityEngine.Object;

namespace LasSecretRPG.UnityAttributes
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(Object), true, isFallback = true)]
    public class FoldoutGroupDrawer : Editor
    {
        protected Dictionary<string, FoldoutData> cacheFolds = new Dictionary<string, FoldoutData>();
        protected List<SerializedProperty> properitiesWithoutFold = new List<SerializedProperty>();
        protected bool initialized;


        public override void OnInspectorGUI()
        {
            this.Setup();

            if (this.cacheFolds.Count == 0)
            {
                this.DrawDefaultInspector();
                return;
            }

            using (new EditorGUI.DisabledScope("m_Script" == this.properitiesWithoutFold[0].propertyPath))
            {
                EditorGUILayout.PropertyField(this.properitiesWithoutFold[0], true);
            }

            foreach (var pair in this.cacheFolds)
            {
                var foldoutData = pair.Value;

                foldoutData.expanded = EditorGUILayout.Foldout(foldoutData.expanded, foldoutData.attribute.name, false, EditorStyles.foldout);

                if (!foldoutData.expanded) continue;
                EditorGUI.indentLevel = 1;

                for (int i = 0; i < foldoutData.properities.Count; i++)
                {
                    EditorGUILayout.PropertyField(foldoutData.properities[i], true);
                }
                EditorGUI.indentLevel = 0;
            }
            for (var i = 1; i < this.properitiesWithoutFold.Count; i++)
            {
                EditorGUILayout.PropertyField(this.properitiesWithoutFold[i], true);
            }

            this.serializedObject.ApplyModifiedProperties();
            
        }

        private void Setup()
        {
            if (this.initialized) return;
            FoldoutGroup prevFold = default;

            var objectFields = this.Get(this.target);

            for (var i = 0; i < objectFields.Count; i++)
            {
                var fold = Attribute.GetCustomAttribute(objectFields[i], typeof(FoldoutGroup)) as FoldoutGroup;
                FoldoutData foldData;
                if (fold == null)
                {
                    if (prevFold != null && prevFold.foldEverything)
                    {
                        if (!this.cacheFolds.TryGetValue(prevFold.name, out foldData))
                        {
                            this.cacheFolds.Add(prevFold.name, new FoldoutData { attribute = prevFold, types = new HashSet<string> { objectFields[i].Name } });
                        }
                        else
                        {
                            foldData.types.Add(objectFields[i].Name);
                        }
                    }

                    continue;
                }

                prevFold = fold;

                if (!this.cacheFolds.TryGetValue(fold.name, out foldData))
                {
                    var expanded = EditorPrefs.GetBool(string.Format($"{fold.name}{objectFields[i].Name}{this.target.name}"), false);
                    this.cacheFolds.Add(fold.name, new FoldoutData { attribute = fold, types = new HashSet<string> { objectFields[i].Name }, expanded = expanded });
                }
                else foldData.types.Add(objectFields[i].Name);
                
            }

            var property = this.serializedObject.GetIterator();
            var next = property.NextVisible(true);
            if (next)
            {
                do
                {
                    bool shouldBeFolded = false;

                    foreach (var pair in this.cacheFolds)
                    {
                        if (pair.Value.types.Contains(property.name))
                        {
                            var newProperty = property.Copy();
                            shouldBeFolded = true;
                            pair.Value.properities.Add(newProperty);

                            break;
                        }
                    }

                    if (shouldBeFolded == false)
                    {
                        var newProperty = property.Copy();
                        this.properitiesWithoutFold.Add(newProperty);
                    }
                } while (property.NextVisible(false));
            }

            this.initialized = true;
        }

        private List<FieldInfo> Get (Object target)
        {
            var t = target.GetType();
            var hash = t.GetHashCode();
            var objectFields = new List<FieldInfo>();
            var typeTree = EditorTypes.GetTypeTree(t);
            objectFields = target.GetType()
                    .GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.NonPublic)
                    .OrderByDescending(x => typeTree.IndexOf(x.DeclaringType))
                    .ToList();


            return objectFields;
        }

    }
}
