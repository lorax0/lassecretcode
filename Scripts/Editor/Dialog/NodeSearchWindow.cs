﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace LasSecretRPG.Dialog
{
    public class NodeSearchWindow : ScriptableObject, ISearchWindowProvider
    {
        protected DialogGraph dialogGraph;
        protected EditorWindow window;

        public void Initialize(EditorWindow window, DialogGraph dialogGraph)
        {
            this.dialogGraph = dialogGraph;
            this.window = window;
        }

        public List<SearchTreeEntry> CreateSearchTree(SearchWindowContext context)
        {
            var tree = new List<SearchTreeEntry>
        {
            new SearchTreeGroupEntry(new GUIContent("Create Elements"), 0),
            new SearchTreeGroupEntry(new GUIContent("Dialog"), 1),
            new SearchTreeEntry(new GUIContent("Dialog node")) {userData = new DialogNode(), level = 2},

        };
            return tree;
        }

        public bool OnSelectEntry(SearchTreeEntry SearchTreeEntry, SearchWindowContext context)
        {
            var worldMousePosition = this.window.rootVisualElement.ChangeCoordinatesTo(this.window.rootVisualElement.parent, context.screenMousePosition - this.window.position.position);
            var localMousePosition = this.dialogGraph.contentViewContainer.WorldToLocal(worldMousePosition);
            switch (SearchTreeEntry.userData)
            {
                case DialogNode dialogNode:
                    this.dialogGraph.CreateNode("Dialog node", localMousePosition);
                    return true;
                default:
                    return false;
            }
        }
    }
}