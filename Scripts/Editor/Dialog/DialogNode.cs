using System.Collections;
using System.Collections.Generic;
using LasSecretRPG.Quest;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace LasSecretRPG.Dialog
{
    public class DialogNode : Node
    {
        public string Id;
        public string Text;
        public bool EntryPoint;
        public float DialogTextTime;
        public QuestData Quest;
    }
}