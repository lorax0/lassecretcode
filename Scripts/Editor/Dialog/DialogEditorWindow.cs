using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace LasSecretRPG.Dialog
{
    public class DialogEditorWindow : EditorWindow
    {
        protected DialogGraph dialogGraph;
        protected string fileName = "New dialog";

        [MenuItem("Tools/DialogWindow")]
        public static void OpenWindow()
        {
            var window = GetWindow<DialogEditorWindow>();
            window.titleContent = new GUIContent("DialogWindow");
        }

        public void OnEnable()
        {
            this.CreateDialog();
            this.GenerateToolbar();
            this.GenerateMinimap();
        }

        public void OnDisable()
        {
            this.rootVisualElement.Remove(this.dialogGraph);
        }

        private void GenerateMinimap()
        {
            var minimap = new MiniMap { anchored = true };
            minimap.SetPosition(new Rect(10, 30, 200, 140));
            this.dialogGraph.Add(minimap);
        }

        private void GenerateToolbar()
        {
            var toolbar = new Toolbar();

            var fileNameTextField = new TextField("File Name: ");
            fileNameTextField.SetValueWithoutNotify(this.fileName);
            fileNameTextField.MarkDirtyRepaint();
            fileNameTextField.RegisterValueChangedCallback(value => this.fileName = value.newValue);

            toolbar.Add(fileNameTextField);
            toolbar.Add(new Button(() => this.RequestDialogSaverOperation(true)) { text = "Save Dialog" });
            toolbar.Add(new Button(() => this.RequestDialogSaverOperation(false)) { text = "Load Dialog" });
            this.rootVisualElement.Add(toolbar);
        }

        private void RequestDialogSaverOperation(bool save)
        {
            if (string.IsNullOrEmpty(this.fileName))
            {
                EditorUtility.DisplayDialog("Invalid file name", "Enter valid file name", "ok");
            }

            var saver = DialogSaver.GetInstance(this.dialogGraph);

            if (save) saver.SaveDialog(this.fileName);
            else saver.LoadDialog(this.fileName);
        }

        private void CreateDialog()
        {
            this.dialogGraph = new DialogGraph(this);
            this.dialogGraph.name = "Dialog";

            this.dialogGraph.StretchToParentSize();
            this.rootVisualElement.Add(this.dialogGraph);
        }
    }
}