﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace LasSecretRPG.Dialog
{
    public class DialogSaver
    {
        protected DialogGraph targetDialogGraph;
        protected DialogContainer dialogCache;
        protected List<Edge> Edges => this.targetDialogGraph.edges.ToList();
        protected List<DialogNode> Nodes => this.targetDialogGraph.nodes.ToList().Cast<DialogNode>().ToList();

        public static DialogSaver GetInstance(DialogGraph dialogGraph) => new DialogSaver { targetDialogGraph = dialogGraph };

        public void SaveDialog(string dialogName)
        {
            if (!this.Edges.Any()) return;

            var dialogContainer = ScriptableObject.CreateInstance<DialogContainer>();

            var connectedPorts = this.Edges.Where(t => t.input.node != null);
            for (int i = 0; i < connectedPorts.Count(); i++)
            {
                var port = connectedPorts.ElementAt(i);
                var outputNode = port.output.node as DialogNode;
                var inputNode = port.input.node as DialogNode;

                dialogContainer.NodeLinks.Add(new NodeLinkData { BaseNodeId = outputNode.Id, PortName = port.output.portName, TargetNodeId = inputNode.Id, DialogTextTime = outputNode.DialogTextTime });
            }

            foreach (var node in this.Nodes.Where(t => !t.EntryPoint))
            {
                dialogContainer.DialogNodeData.Add(new DialogNodeData { NodeId = node.Id, DialogText = node.Text, Position = node.GetPosition().position, DialogTextTime = node.DialogTextTime, Quest = node.Quest});
            }

            if (!AssetDatabase.IsValidFolder("Assets/Resources")) AssetDatabase.CreateFolder("Assets", "Resources");

            AssetDatabase.CreateAsset(dialogContainer, "Assets/Resources/" + dialogName + ".asset");
            AssetDatabase.SaveAssets();
        }

        public void LoadDialog(string dialogName)
        {
            this.dialogCache = Resources.Load<DialogContainer>(dialogName);
            if (this.dialogCache == null)
            {
                EditorUtility.DisplayDialog("Dialog not found", "Dialog is not exist", "ok");
                return;
            }
            this.ClearGraph();
            this.CreateNodes();
            this.ConnectNodes();
        }

        private void ConnectNodes()
        {
            for (int i = 0; i < this.Nodes.Count; i++)
            {
                var connections = this.dialogCache.NodeLinks.Where(t => t.BaseNodeId == this.Nodes[i].Id);
                for (int j = 0; j < connections.Count(); j++)
                {
                    var targetNodeId = connections.ElementAt(j).TargetNodeId;
                    var targetNode = this.Nodes.First(t => t.Id == targetNodeId);
                    this.LinkNodes(this.Nodes[i].outputContainer[j].Q<Port>(), (Port)targetNode.inputContainer[0]);

                    targetNode.SetPosition(new Rect(this.dialogCache.DialogNodeData.First(t => t.NodeId == targetNodeId).Position, this.targetDialogGraph.DefaultNodeSize));
                }
            }
        }

        private void LinkNodes(Port output, Port input)
        {
            var edge = new Edge { output = output, input = input };
            edge?.input.Connect(edge);
            edge?.output.Connect(edge);
            this.targetDialogGraph.Add(edge);
        }

        private void CreateNodes()
        {
            foreach (var nodeData in this.dialogCache.DialogNodeData)
            {
                var node = this.targetDialogGraph.CreateDialogNode(nodeData.DialogText, Vector2.zero, nodeData.DialogTextTime, nodeData.Quest);
                node.Id = nodeData.NodeId;
                this.targetDialogGraph.AddElement(node);

                var nodePorts = this.dialogCache.NodeLinks.Where(t => t.BaseNodeId == nodeData.NodeId).ToList();
                nodePorts.ForEach(t => this.targetDialogGraph.AddChoisePort(node, t.DialogTextTime, t.PortName));
            }
        }

        private void ClearGraph()
        {
            this.Nodes.Find(t => t.EntryPoint).Id = this.dialogCache.NodeLinks[0].BaseNodeId;

            foreach (var node in this.Nodes)
            {
                if (node.EntryPoint) continue;
                this.Edges.Where(edge => edge.input.node == node).ToList().ForEach(edge => this.targetDialogGraph.RemoveElement(edge));
                this.targetDialogGraph.RemoveElement(node);
            }
        }
    }
}