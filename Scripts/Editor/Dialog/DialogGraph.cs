﻿using System;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;
using UnityEngine;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using LasSecretRPG.Quest;

namespace LasSecretRPG.Dialog
{

    public class DialogGraph : GraphView
    {
        public readonly Vector2 DefaultNodeSize = new Vector2(150, 200);

        protected NodeSearchWindow nodeSearchWindow;

        public DialogGraph(EditorWindow window)
        {
            this.SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);
            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());

            this.AddElement(this.CreateBaseNode());
            this.AddSearchWindow(window);
        }

        private void AddSearchWindow(EditorWindow window)
        {
            this.nodeSearchWindow = ScriptableObject.CreateInstance<NodeSearchWindow>();
            this.nodeSearchWindow.Initialize(window, this);
            this.nodeCreationRequest = t => SearchWindow.Open(new SearchWindowContext(t.screenMousePosition), this.nodeSearchWindow);
        }

        public void CreateNode(string name, Vector2 position) => this.AddElement(this.CreateDialogNode(name, position));

        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            var compatiblePorts = new List<Port>();
            this.ports.ForEach(port =>
            {
                if (startPort != port && startPort.node != port.node)
                    compatiblePorts.Add(port);
            });
            return compatiblePorts;
        }

        public DialogNode CreateDialogNode(string name, Vector2 position, float dialogTextTime = 0, QuestData questData = null)
        {
            var node = new DialogNode();
            node.Id = Guid.NewGuid().ToString();
            node.title = name;
            node.Text = name;
            node.SetPosition(new Rect(position, this.DefaultNodeSize));

            var port = this.CreatePort(node, Direction.Input, Port.Capacity.Multi);
            port.portName = "Input";
            node.inputContainer.Add(port);

            var button = new Button(() => this.AddChoisePort(node));
            button.text = "Add answer";
            node.titleContainer.Add(button);

            var questField = new ObjectField();
            questField.objectType = typeof(QuestData);
            questField.value = questData;
            node.Quest = questData;
            questField.RegisterValueChangedCallback(t => node.Quest = (QuestData)t.newValue);
            node.mainContainer.Add(questField);

            var dialogTextField = new TextField(string.Empty);
            dialogTextField.RegisterValueChangedCallback(value => { node.Text = value.newValue; node.title = value.newValue; });
            dialogTextField.SetValueWithoutNotify(node.title);
            node.mainContainer.Add(dialogTextField);

            var dialogTimeTextField = new TextElement();
            var dialogTimeSlider = new Slider(0, 5);
            dialogTimeTextField.text = dialogTextTime.ToString();
            dialogTimeSlider.value = dialogTextTime;
            node.DialogTextTime = dialogTextTime;
            dialogTimeSlider.RegisterValueChangedCallback(value => { node.DialogTextTime = value.newValue; dialogTimeTextField.text = value.newValue.ToString(); });
            node.mainContainer.Add(dialogTimeSlider);
            node.mainContainer.Add(dialogTimeTextField);

            node.RefreshExpandedState();
            node.RefreshPorts();

            return node;
        }

        public void AddChoisePort(DialogNode node, float dialogTextTime = 0, string portName = "")
        {
            var port = this.CreatePort(node, Direction.Output);

            var oldLabel = port.contentContainer.Q<Label>("type");
            port.contentContainer.Remove(oldLabel);

            var outputPortCount = node.outputContainer.Query("connector").ToList().Count;
            var choisePortName = string.IsNullOrEmpty(portName) ? (outputPortCount + 1).ToString() : portName;

            var textField = new TextField { name = string.Empty, value = choisePortName };
            textField.RegisterValueChangedCallback(value => port.portName = value.newValue);


            var deleteButton = new Button(() => this.RemovePort(node, port)) { text = "X" };
            port.contentContainer.Add(new Label(" "));
            port.contentContainer.Add(textField);
            port.contentContainer.Add(deleteButton);

            port.portName = choisePortName;
            node.outputContainer.Add(port);
            node.RefreshPorts();
            node.RefreshExpandedState();
        }

        private void RemovePort(DialogNode node, Port port)
        {
            var edges = this.edges.ToList().Where(t => t.output.portName == port.portName && t.output.node == port.node);

            if (!edges.Any()) return;
            var edge = edges.First();
            edge.input.Disconnect(edge);
            this.RemoveElement(edge);

            node.outputContainer.Remove(port);
            node.RefreshPorts();
            node.RefreshExpandedState();
        }

        private DialogNode CreateBaseNode()
        {
            var node = new DialogNode();
            node.Id = Guid.NewGuid().ToString();
            node.title = "StartNode";
            node.Text = "Text";
            node.EntryPoint = true;
            node.SetPosition(new Rect(0, 0, 100, 150));

            var port = this.CreatePort(node, Direction.Output);
            port.portName = "";
            node.outputContainer.Add(port);

            node.RefreshExpandedState();
            node.RefreshPorts();

            return node;
        }

        private Port CreatePort(DialogNode node, Direction direction, Port.Capacity capacity = Port.Capacity.Single) => node.InstantiatePort(Orientation.Horizontal, direction, capacity, typeof(float));
    }
}