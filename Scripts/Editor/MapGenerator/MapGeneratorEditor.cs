﻿namespace LasSecretRPG.MapGeneration
{
#if UNITY_EDITOR

	using UnityEngine;
	using System.Collections;
	using UnityEditor;

	[CustomEditor(typeof(MapGenerator))]
	public class MapGeneratorEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			MapGenerator mapGen = (MapGenerator)target;

			//bez tej linijki nic nie pokazuje sie w inspektorze XD
			if (DrawDefaultInspector())
			{

			}

			if (GUILayout.Button("Generate"))
			{
				mapGen.GenerateMap();
			}

		}
	}

#endif
}