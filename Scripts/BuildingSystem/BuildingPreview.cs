﻿using LasSecretRPG.Inputs;
using LasSecretRPG.Player;
using UnityEngine;

namespace LasSecretRPG.Buildings
{
    public class BuildingPreview : MonoBehaviour
    {
        public Sprite Sprite { get => this.buildingPreviewSprite.sprite; set => this.buildingPreviewSprite.sprite = value; }

        [SerializeField] private SpriteRenderer buildingPreviewSprite;

        private void Update() => this.buildingPreviewSprite.transform.position = MouseRaycast.GetPositionForBuilding();

        public void PreviewSprite(bool canBuild)
        {
            if (canBuild)
                this.buildingPreviewSprite.material.color = Color.green;
            else
                this.buildingPreviewSprite.material.color = Color.red;
        }
    }
}