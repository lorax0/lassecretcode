using LasSecretRPG.Inputs;
using LasSecretRPG.Plant;
using LasSecretRPG.Player;
using LasSecretRPG.Tiles;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

namespace LasSecretRPG.Buildings
{
    [Serializable]
    public class BuildingService
    {
        public LayerMask BuildingCollidersLayerMask => this.buildingCollidersLayerMask;
        public LayerMask FloorCollidersLayerMask => this.floorCollidersLayerMask;

        [SerializeField] private LayerMask buildingCollidersLayerMask;
        [SerializeField] private LayerMask floorCollidersLayerMask;
        [SerializeField] private BuildingPreview buildingPreview;
        
        public void Build(BuildingController buildingPrefab)
        {
            var building = MonoBehaviour.Instantiate(buildingPrefab, MouseRaycast.GetPositionForBuilding(), Quaternion.identity);
        }

        public void Plant(IGrowable growable) => MouseRaycast.GetGameobjectsOnMousePosition<FarmField>().FirstOrDefault().Plant(growable);

        public bool CanBuild(LayerMask layer)
        {
            Collider2D[] colliderArray = Physics2D.OverlapCircleAll(MouseRaycast.GetPositionForBuilding(), 0.45f, layer);
            if(colliderArray.Length <= 0)
                return true;

            return false;
        }

        public bool CanPlant()
        {
            var farmFieldOnMousePosition = MouseRaycast.GetGameobjectsOnMousePosition<FarmField>().FirstOrDefault();
            if (farmFieldOnMousePosition == null || farmFieldOnMousePosition.Growable != null) return false;
            return true;
        }
        
        public void EnablePreview(Building building)
        {
            if (building is Floor) this.buildingPreview.PreviewSprite(this.CanBuild(this.floorCollidersLayerMask));
            else this.buildingPreview.PreviewSprite(this.CanBuild(this.buildingCollidersLayerMask));

            this.buildingPreview.gameObject.SetActive(true);
            this.buildingPreview.Sprite = building.Sprite;
        }

        public void DisablePreview()
        {
            this.buildingPreview.gameObject.SetActive(false);
        }

    }
}