using LasSecretRPG.Items;
using LasSecretRPG.Player;
using UnityEngine;

namespace LasSecretRPG.Buildings
{
    [CreateAssetMenu(fileName = "New Building", menuName = "Items/Building/Building")]
    public class Building : Item, IUseable
    {
        [SerializeField] protected BuildingController buildingPrefab;

        public virtual void Use(PlayerController playerController)
        {
            playerController.BuildingService.Build(this.buildingPrefab);
        }

        public virtual bool CanUse(PlayerController playerController) => playerController.BuildingService.CanBuild(playerController.BuildingService.BuildingCollidersLayerMask);

    }
}
