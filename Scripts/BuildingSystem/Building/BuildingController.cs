﻿using LasSecretRPG.Items;
using System;
using UnityEngine;

namespace LasSecretRPG.Buildings
{
    public class BuildingController : MonoBehaviour
    {
        public Building Building => this.building;
        public ItemData ItemToDrop => this.itemToDrop;

        [SerializeField] private Building building;
        [SerializeField] private ItemData itemToDrop;

    }
}
