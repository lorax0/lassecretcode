using LasSecretRPG.Inputs;
using LasSecretRPG.Items;
using LasSecretRPG.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Buildings
{
    [CreateAssetMenu(fileName = "New Shovel", menuName = "Items/Tools/Shovel")]
    public class Shovel : Item, IUseable, IDuratibility
    {
        public int MaxDurability => this.maxDurability;

        [SerializeField] private int maxDurability;
        [SerializeField] private LayerMask shovelLayerMask;

        public void Use(PlayerController playerController)
        {
            var building = MouseRaycast.GetBuildingOnMousePosition<BuildingController>(this.shovelLayerMask);
            //TODO: add item drop from building.itemToDrop
            Destroy(building.gameObject);
        }

        public bool CanUse(PlayerController playerController)
        {
            return true;
        }
    }

}