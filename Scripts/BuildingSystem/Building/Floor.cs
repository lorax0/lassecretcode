using LasSecretRPG.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Buildings
{
    [CreateAssetMenu(fileName = "New Floor", menuName = "Items/Building/Floor")]
    public class Floor : Building
    {
        public override void Use(PlayerController playerController)
        {
            playerController.BuildingService.Build(this.buildingPrefab);
        }

        public override bool CanUse(PlayerController playerController) => playerController.BuildingService.CanBuild(playerController.BuildingService.FloorCollidersLayerMask);
    }

}