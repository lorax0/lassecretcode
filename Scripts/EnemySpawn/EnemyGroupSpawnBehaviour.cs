using LasSecretRPG.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.EnemySpawn
{
    public class EnemyGroupSpawnBehaviour : MonoBehaviour
    {
        [SerializeField] private GameObject[] enemies;
        [SerializeField] private Vector2 spawnSize;
        private void Start()
        {
            for (int i = 0; i < this.enemies.Length; i++)
            {
                Vector2 spawnPos = new Vector2(RandomGenerator.RandomRange(-this.spawnSize.x, this.spawnSize.x), RandomGenerator.RandomRange(-this.spawnSize.y, this.spawnSize.y));
                GameObject spawnedEnemy = Instantiate(this.enemies[i], this.transform.position, Quaternion.identity);
                spawnedEnemy.transform.position += (Vector3)spawnPos;
            }
            Destroy(this.gameObject);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(this.transform.position, this.spawnSize);
        }
    }

}