using UnityEngine;
using LasSecretRPG.MapGeneration;
using System.Linq;
using Zenject;
using LasSecretRPG.Utilities.Time;
using System.Collections.Generic;
using LasSecretRPG.Utilities;

namespace LasSecretRPG.EnemySpawn
{
    public class EnemySpawnerManager : MonoBehaviour
    {
        public int EnemySpawned => this.enemySpawned;

        [SerializeField] private int spawnRate;
        [SerializeField] private int chanceForEnemyGroup;
        [SerializeField] private int maxSpawn;
        [SerializeField] private int enemySpawnPointMultiplier;
        [SerializeField] private float minDistanceFromPlayer;
        [SerializeField] private LayerMask whereEnemyCantSpawn;

        private Transform player;
        private int enemySpawned;
        [Inject] private TimeManager timeManager;

        private const int randomPositionCheck = 10;

        private void Awake()
        {
            this.player = GameObject.FindGameObjectWithTag("Player").transform; //TODO: change searching by tag
            this.CalculateBiomesWeights();
            this.enabled = false;
        }

        private void Update()
        {
            this.CheckForSpawn();
        }

        private void CheckForSpawn()
        {
            //TODO: add check if enemySpawner >= maxSpawn so there's limited enemy amount to spawn

            int random = RandomGenerator.RandomRange(0, this.spawnRate);
            if (random == 0)
            {
                Vector2 enemyPosition = this.PickEnemyPosition();
                if (enemyPosition == Vector2.zero)
                    return;

                int positionX = Mathf.RoundToInt(enemyPosition.x);
                int positionY = Mathf.RoundToInt(enemyPosition.y);
                Biome biome = MapGenerator.Instance.BiomesPositionMap[new Vector2Int(positionX, positionY)];

                int groupRandom = RandomGenerator.RandomRange(0, this.chanceForEnemyGroup);
                this.SpawnMob(enemyPosition, biome, groupRandom == 0);
            }
        }

        private void CalculateBiomesWeights()
        {
            for (int i = 0; i < MapGenerator.Instance.Biomes.Length; i++)
            {
                MapGenerator.Instance.Biomes[i].enemySpawnService.CalculateWeights();
            }
        }

        private void SpawnMob(Vector2 enemyPosition, Biome biome, bool isGroupSpawn)
        {
            WeightedEnemy[] possibleEnemies = this.GetPossibleEnemies(isGroupSpawn, biome);

            WeightedEnemy enemy = biome.enemySpawnService.FindEnemyToSpawn(possibleEnemies, isGroupSpawn);
            Instantiate(enemy.prefab, enemyPosition, Quaternion.identity);
            this.enemySpawned += enemy.enemySpace;
        }

        private WeightedEnemy[] GetPossibleEnemies(bool isGroupSpawn, Biome biome)
        {
            WeightedEnemy[] weightedEnemies = isGroupSpawn ? biome.enemySpawnService.weightedGroupOfEnemies :
                biome.enemySpawnService.weightedSingleEnemies;

            weightedEnemies = weightedEnemies.Where(t => t.neededSeasons.Contains(this.timeManager.CurrentSeason)).ToArray();

            WeightedEnemy[] weightedEventEnemies = isGroupSpawn ? this.timeManager.DayCounter.WorldEventController.EventSpawnService?.weightedGroupOfEnemies :
                this.timeManager.DayCounter.WorldEventController.EventSpawnService?.weightedSingleEnemies;

            return weightedEnemies.Concat(weightedEventEnemies).ToArray();
        }

        private Vector2 PickEnemyPosition()
        {
            Vector2 randomEnemyPosition = this.PickRandomInsideCirclePosition();
            RaycastHit2D raycastHit2D = Physics2D.Raycast(randomEnemyPosition, Vector2.down, 1f, this.whereEnemyCantSpawn);
            if (raycastHit2D.collider == null)
            {
                int spawnX = Mathf.RoundToInt(randomEnemyPosition.x);
                int spawnY = Mathf.RoundToInt(randomEnemyPosition.y);

                if (MapGenerator.Instance.BiomesPositionMap.ContainsKey(new Vector2Int(spawnX, spawnY)))
                {
                    return randomEnemyPosition;
                }
            }

            return Vector2.zero;
        }

        private Vector2 PickRandomInsideCirclePosition()
        {
            Vector2 randomPositionInsideCircle = Vector2.zero;

            for (int i = 0; i < randomPositionCheck; i++)
            {
                randomPositionInsideCircle = Random.insideUnitCircle * this.enemySpawnPointMultiplier + (Vector2)this.player.position;
                if (Vector2.Distance(randomPositionInsideCircle, this.player.transform.position) > this.minDistanceFromPlayer)
                    return randomPositionInsideCircle;
            }

            return randomPositionInsideCircle;
        }
    }

}

