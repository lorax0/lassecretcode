using LasSecretRPG.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LasSecretRPG.EnemySpawn
{
    [System.Serializable]
    public class EnemySpawnService
    {
		public WeightedEnemy[] weightedSingleEnemies;
		public WeightedEnemy[] weightedGroupOfEnemies;

        private float singleEnemyTotalWeight;
        private float groupOfEnemyTotalWeight;

        public WeightedEnemy FindEnemyToSpawn(WeightedEnemy[] weightedEnemy, bool isGroupSpawn)
        {
            float randomNumber = RandomGenerator.RandomRange(0f, 1f);
            float currentWeight = 0f;
            float totalWeight;

            if (isGroupSpawn)
                totalWeight = this.groupOfEnemyTotalWeight;
            else
                totalWeight = this.singleEnemyTotalWeight;

            for (int i = 0; i < weightedEnemy.Length; i++)
            {
                currentWeight += weightedEnemy[i].weight;
                if(randomNumber < currentWeight / totalWeight)
                {
                    return weightedEnemy[i];
                }
            }

            return weightedEnemy[0];
        }

        public void CalculateWeights()
        {
            this.singleEnemyTotalWeight = this.weightedSingleEnemies.Sum(t => t.weight);
            this.groupOfEnemyTotalWeight = this.weightedGroupOfEnemies.Sum(t => t.weight);
        }

    }
}