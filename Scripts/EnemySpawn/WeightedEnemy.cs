using LasSecretRPG.Utilities.Time;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.EnemySpawn
{
    [System.Serializable]
    public class WeightedEnemy
    {
        public GameObject prefab;
        public float weight;
        public int enemySpace = 1;
        public List<Season> neededSeasons;
    }

}