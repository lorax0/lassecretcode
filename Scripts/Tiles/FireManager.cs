using LasSecretRPG.Utilities;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace LasSecretRPG.Tiles
{
    public class FireManager : MonoBehaviour
    {
        [SerializeField] private TileBase ashTile;
        [SerializeField] private Fire firePrefab;
        [SerializeField] private TileManager tileManager;

        private List<Fire> activeFires = new List<Fire>();
        private Tilemap tilemap;

        private void Start()
        {
            this.tilemap = this.tileManager.Tilemap;
            this.Dupa();
        }

        public void TryToSpread(Vector3Int position)
        {
            for (int x = position.x - (int)this.tilemap.cellSize.x; x < position.x + (2 * (int)this.tilemap.cellSize.x); x+= (int)this.tilemap.cellSize.x)
            {
                for (int y = position.y - (int)this.tilemap.cellSize.y; y < position.y + (2 * (int)this.tilemap.cellSize.y); y += (int)this.tilemap.cellSize.y)
                {
                    this.TryToBurnTile(new Vector3Int(x, y, 0));
                }   
            }
        }

        private void TryToBurnTile(Vector3Int tilePosition)
        {
            if (this.activeFires.Where(t => t.Position == tilePosition).FirstOrDefault() != null) return;

            TileData data = this.tileManager.GetTileData(tilePosition);

            if (data != null && data.CanBurn)
            {
                if (RandomGenerator.RandomRange(0f, 100f) <= data.SpreadChance)
                    this.SetTileOnFire(tilePosition, data);
            }
        }

        private void SetTileOnFire(Vector3Int tilePosition, TileData data)
        {
            Fire newFire = Instantiate(this.firePrefab);
            newFire.transform.position = this.tilemap.GetCellCenterWorld(tilePosition);
            newFire.StartBurning(tilePosition, data);
            this.activeFires.Add(newFire);
            newFire.OnBurningEnd += () => this.DestroyFire(newFire);
            newFire.OnBurningEnd += () => this.SetTileAsAsh(newFire.Position);
            newFire.OnSpreadTimerEnd += () => this.TryToSpread(newFire.Position);

        }

        private void SetTileAsAsh(Vector3Int position) => this.tilemap.SetTile(position, this.ashTile);

        private void DestroyFire(Fire fire)
        {
            Destroy(fire.gameObject);
            this.activeFires.Remove(fire);
        }

        private void Dupa()
        {
            Vector3Int gridPosition = this.tilemap.WorldToCell(new Vector3(3,0,0));

            TileData data = this.tileManager.GetTileData(gridPosition);

            this.SetTileOnFire(gridPosition, data);
        }
    }
}