using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace LasSecretRPG.Tiles
{
    public class TileManager : MonoBehaviour
    {
        public static TileManager Instance => instance;
        private static TileManager instance;

        public Tilemap Tilemap => this.tilemap;

        [SerializeField] private List<TileData> tilesData;
        [SerializeField] private Tilemap tilemap;

        private Dictionary<TileBase, TileData> dataForTile = new Dictionary<TileBase, TileData>();

        private void Awake()
        {
            if (instance != null) Destroy(this.gameObject);
            else instance = this;
            this.InitiateDataFromTile();
        }

        private void InitiateDataFromTile()
        {
            foreach (var tileData in this.tilesData)
            {
                foreach (var tileBase in tileData.TileBases)
                {
                    this.dataForTile.Add(tileBase, tileData);
                }
            }
        }

        public TileBase GetTileFromPosition(Vector2 position)
        {
            Vector3Int gridPosition = this.tilemap.WorldToCell(position);
            return this.tilemap.GetTile(gridPosition);
        }

        public float GetSpeedOnTile(Vector3 position)
        {
            TileBase tile = this.GetTileFromPosition(position);
            return this.dataForTile[tile].MovementSpeedInPercentage;
        }

        public TileData GetTileData(Vector3Int tilePosition)
        {
            TileBase tile = this.tilemap.GetTile(tilePosition);
            if (tile == null) return null;
            return this.dataForTile[tile];
        }
    }
}