using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace LasSecretRPG.Tiles
{
    [CreateAssetMenu(fileName = "New Tile Data", menuName = "Tiles/Tile Data")]
    public class TileData : ScriptableObject
    {
        public float MovementSpeedInPercentage => (float)this.movementSpeedInPercentage / 100;
        public IEnumerable<TileBase> TileBases => this.tileBases;

        public bool CanBurn => this.canBurn;
        public float SpreadChance => this.spreadChance;
        public float SpreadIntervall => this.spreadIntervall;
        public float BurnTime => this.burnTime;

        [Range(1, 250)] [SerializeField] private int movementSpeedInPercentage = 100;
        [SerializeField] private List<TileBase> tileBases;

        [SerializeField] private bool canBurn;
        [SerializeField] private float spreadChance;
        [SerializeField] private float spreadIntervall;
        [SerializeField] private float burnTime;
    }
}