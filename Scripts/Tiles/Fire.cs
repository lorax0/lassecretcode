using System;
using UnityEngine;

namespace LasSecretRPG.Tiles
{
    public class Fire : MonoBehaviour
    {
        public Action OnBurningEnd;
        public Action OnSpreadTimerEnd;

        public Vector3Int Position => this.position;

        private Vector3Int position;
        private TileData tileData;

        private float burnTimeCounter;
        private float spreadIntervallCounter;

        public void StartBurning(Vector3Int position, TileData data)
        {
            this.position = position;
            this.tileData = data;

            this.burnTimeCounter = this.tileData.BurnTime;
            this.spreadIntervallCounter = this.tileData.SpreadIntervall;
        }

        private void Update()
        {
            this.burnTimeCounter -= Time.deltaTime;
            if (this.burnTimeCounter <= 0)
            {
                this.OnBurningEnd?.Invoke();
            }

            this.spreadIntervallCounter -= Time.deltaTime;
            if (this.spreadIntervallCounter <= 0)
            {
                this.OnSpreadTimerEnd?.Invoke();
                this.spreadIntervallCounter = this.tileData.SpreadIntervall;
            }

        }
    }
}