using UnityEngine;
using UnityEngine.InputSystem;
using LasSecretRPG.Damageable;
using LasSecretRPG.Buildings;
using LasSecretRPG.Enemies;
using LasSecretRPG.Quest;
using System.Linq;
using System.Collections.Generic;
using LasSecretRPG.Tiles;
using LasSecretRPG.Cooking;
using LasSecretRPG.Map;
using LasSecretRPG.UI;
using LasSecretRPG.Statistics.Buffs;
using LasSecretRPG.Damageable.Knockback;
using LasSecretRPG.Inventory.Chest;

namespace LasSecretRPG.Player
{
    public class PlayerController : DamageableUnit, IBuffable
    {
        public Inventory.Inventory Inventory => this.inventory;
        public HungerService HungerService => this.hungerService;
        public StaminaService StaminaService => this.staminaService;
        public BuildingService BuildingService => this.buildingService;
        public BuffsService BuffsService => this.buffsService;
        public CookingService CookingService => this.cookingService;
        public MapController MapController => this.mapController;
        public ChestManager ChestManager => this.chestManager;

        [SerializeField] private ScriptableObject characterData;
        [SerializeField] private Animator animator;
        [SerializeField] private Inventory.Inventory inventory;
        [SerializeField] private ChestManager chestManager;
        [SerializeField] private BuildingService buildingService;
        [SerializeField] private WeaponRotator weaponRotator;
        [SerializeField] private MovementService movementService;
        [SerializeField] private HungerService hungerService;
        [SerializeField] private StaminaService staminaService;
        [SerializeField] private DashService dashService;
        [SerializeField] private BuffsService buffsService;
        [SerializeField] private CookingService cookingService;
        [SerializeField] private MapController mapController;
        [SerializeField] private InputActionReference moveAction;

        private TileManager tileManager;

        private void Awake()
        {
            this.dashService.ResetDash();
            this.healthService.Transform = this.transform;
            this.hungerService.OnStarving += this.healthService.TakeDamage;
            this.hungerService.OnOverEaten += this.healthService.IncreaseHealth;
            this.cookingService.OnCookingPanelActivityChange += (bool isActive) => this.movementService.CanMove = !isActive;
            this.mapController.OnMapActive += (bool isActive) => this.movementService.CanMove = !isActive;
            this.healthService.GetDamageWithoutBlocked += this.inventory.ShieldController.GetDamageWithoutBlocked;
            this.movementService.MoveSpeed.InitializeStatistic(this);
            this.staminaService.Initialize(this);
            this.buffsService.Buffable = this;
            this.inventory.PlayerController = this;
            this.chestManager.Inventory = this.inventory;
        }

        public override void Start()
        {
            base.Start();
            this.tileManager = TileManager.Instance;
        }

        private void OnEnable()
        {
            this.moveAction.action.Enable();
        }

        private void OnDisable()
        {
            this.moveAction.action.Disable();
        }

        private void FixedUpdate()
        {
            this.dashService.DecreaseCooldown();
            Vector3 moveVector = this.movementService.CanMove ? this.movementService.GetMoveVector(this.moveAction, this) * this.tileManager.GetSpeedOnTile(this.transform.position) : Vector3.zero;
            
            if (this.dashService.CanDash())
            {
                //TODO: Add dash animation
                this.transform.position += this.dashService.GetDashVector(moveVector);
                this.dashService.ResetDash();
            }
            else this.transform.position += moveVector;
            this.animator.SetBool("isMoving", this.movementService.IsMoving(moveVector));

        }

        private void Update()
        {
            this.hungerService.LosingHunger();
            this.weaponRotator.RotateWeapon(this.transform);
        }

    }
}