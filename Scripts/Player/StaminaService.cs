﻿using LasSecretRPG.UI;
using System;
using System.Collections;
using UnityEngine;

namespace LasSecretRPG.Player
{
    [Serializable]
    public class StaminaService
    {
        [SerializeField] private float staminaOnFrame;
        [SerializeField] private float delay;
        [SerializeField] private float maxStamina;
        [SerializeField] private ImageBar staminaBar;

        [SerializeField] private float currentStamina;
        private Coroutine staminaCoroutine;
        private PlayerController playerController;

        public void Initialize(PlayerController playerController)
        {
            this.playerController = playerController;
            this.currentStamina = this.maxStamina;
        }

        public void IncreaseStamina(float stamina)
        {
            this.currentStamina = Mathf.Min(this.currentStamina + stamina, this.maxStamina);
            this.staminaBar.UpdateBar(this.currentStamina, this.maxStamina);
        }
        
        public void DecreaseStamina(float stamina)
        {
            this.currentStamina = Mathf.Max(0, this.currentStamina - stamina);
            this.staminaBar.UpdateBar(this.currentStamina, this.maxStamina);
            if (this.staminaCoroutine != null) this.playerController.StopCoroutine(this.staminaCoroutine);
            this.staminaCoroutine = this.playerController.StartCoroutine(this.StartStaminaDelay());
        }

        public bool HaveStamina() => this.currentStamina > 0;

        private IEnumerator StartStaminaDelay()
        {
            yield return new WaitForSeconds(this.delay);
            this.staminaCoroutine = this.playerController.StartCoroutine(this.StartStaminaIncreasing());
        }

        private IEnumerator StartStaminaIncreasing()
        {
            while(this.currentStamina < this.maxStamina)
            {
                this.IncreaseStamina(this.staminaOnFrame);
                yield return new WaitForFixedUpdate();
            }
        }
    }
}
