﻿using LasSecretRPG.Inputs;
using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace LasSecretRPG.Player
{
    [Serializable]
    public class DashService
    {
        [SerializeField] private float dashSpeed;
        [SerializeField] private float dashCooldown;
        [SerializeField] private InputActionReference dashAction;

        private float dashCounter;

        public Vector3 GetDashVector(Vector3 moveVector) => moveVector * this.dashSpeed;

        public void DecreaseCooldown()
        {
            if(this.dashCounter > 0f)  this.dashCounter -= Time.deltaTime;
        }

        public void ResetDash() => this.dashCounter = this.dashCooldown;

        public bool CanDash() => InputActionManager.IsPressedButton(this.dashAction.action) && this.dashCounter <= 0f;
    }
}