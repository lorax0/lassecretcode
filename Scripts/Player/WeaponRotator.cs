﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace LasSecretRPG.Player
{
    [Serializable]
    public class WeaponRotator
    {
        [SerializeField] private Transform toolsHandlerPoint;
        [SerializeField] private Transform bodySr;

        public void RotateWeapon(Transform transform)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Mouse.current.position.ReadValue());

            Vector2 lookDir = mousePos - (Vector2)transform.position;
            float swordAngle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;
            this.toolsHandlerPoint.rotation = Quaternion.Euler(0, 0, swordAngle);

            bool isRotate = swordAngle < -90 || swordAngle > 90;
            bodySr.rotation = isRotate ? Quaternion.Euler(0,0,0) : Quaternion.Euler(0,180,0);
  
        }
    }
}