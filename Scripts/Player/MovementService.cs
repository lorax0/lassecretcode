﻿using LasSecretRPG.Statistics;
using LasSecretRPG.Statistics.Buffs;
using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace LasSecretRPG.Player
{
    [Serializable]
    public class MovementService
    {
        public StatisticData MoveSpeed => this.moveSpeed;
        public bool CanMove { get => this.canMove; set => this.canMove = value; }

        [SerializeField] private StatisticData moveSpeed;

        private bool canMove = true;
        
        public Vector3 GetMoveVector(InputAction action, IBuffable buffable) => this.GetMoveInput(action) * this.moveSpeed.Statistic.Values[buffable] *  Time.deltaTime;
        
        public bool IsMoving(Vector2 moveInput) => moveInput.x != 0 || moveInput.y != 0;

        private Vector3 GetMoveInput(InputAction action) => action.ReadValue<Vector2>().normalized;
    }
}