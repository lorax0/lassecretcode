using LasSecretRPG.UI;
using System;
using UnityEngine;

namespace LasSecretRPG.Player
{
    [Serializable]
    public class HungerService
    {
        public Action<float> OnStarving;
        public Action<float> OnOverEaten;

        [SerializeField] private float currentHunger;
        [SerializeField] private float maxHunger;
        [SerializeField] private float maxOverEatenHunger;
        [SerializeField] private float hungerRate;
        [SerializeField] private float damageOnStarving;
        [SerializeField] private float bonusHealthOnOverEaten;
        [SerializeField] private float lostHungryForVomiting;
        [SerializeField] private ImageBar hungerBar;

        public void Eat(float foodValue)
        {
            this.currentHunger = Mathf.Clamp(this.currentHunger + foodValue, 0, this.maxOverEatenHunger);
            if (this.currentHunger >= this.maxOverEatenHunger) this.Vomit();
            else if (this.currentHunger >= this.maxHunger) this.OverEaten();
        }

        public void LosingHunger()
        {
            if (this.currentHunger <= 0) this.Starving();
            else
            {
                this.currentHunger -= this.hungerRate * Time.deltaTime;
                this.hungerBar.UpdateBar(this.currentHunger, this.maxHunger);
            }
        }

        private void Vomit()
        {
            //TODO: Animacja rzygania
            this.currentHunger -= this.lostHungryForVomiting;
        }

        private void OverEaten()
        {
            this.OnOverEaten?.Invoke(this.bonusHealthOnOverEaten);
        }

        private void Starving()
        {
            this.OnStarving?.Invoke(this.damageOnStarving);
        }

    }
}