﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

namespace LasSecretRPG.Inputs
{
    public static class MouseRaycast
    {
        public static IEnumerable<T> GetGameobjectsOnMousePosition<T>() where T : MonoBehaviour => GetGameobjectsOnMousePosition<T>(Camera.main);

        public static IEnumerable<T> GetGameobjectsOnMousePosition<T>(Camera camera) where T : MonoBehaviour =>
            Physics2D.RaycastAll(GetWorldMousePosition(camera), Vector2.zero).Select(t => t.transform.GetComponent<T>()).Where(t => t != null);

        public static Vector2 GetRoundMousePosition()
        {
            var mousePosition = GetWorldMousePosition();
            return new Vector2(Mathf.Round(mousePosition.x), Mathf.Round(mousePosition.y));
        }

        public static Vector2 GetMousePosition() => Mouse.current.position.ReadValue();

        public static Vector2 GetWorldMousePosition() => GetWorldMousePosition(Camera.main);

        public static Vector2 GetWorldMousePosition(Camera camera) => camera.ScreenToWorldPoint(GetMousePosition());

        public static T GetBuildingOnMousePosition<T>(LayerMask layerMask) => Physics2D.RaycastAll(GetPositionForBuilding(), Vector2.zero, Mathf.Infinity, layerMask).
            Select(t => t.transform.GetComponentInParent<T>()).Where(t => t!=null).FirstOrDefault();
        
        public static Vector2 GetPositionForBuilding()
        {
            Vector2 position = GetWorldMousePosition();
            position.x = Mathf.Floor(position.x) + 0.5f;
            position.y = Mathf.Floor(position.y) + 0.5f;
            return position;
        }

    }
}
