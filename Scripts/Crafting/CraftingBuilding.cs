using LasSecretRPG.Buildings;
using UnityEngine;

namespace LasSecretRPG.Crafting
{
    [CreateAssetMenu(fileName = "New Crafting Building", menuName = "Items/Building/CraftingBuilding")]
    public class CraftingBuilding : Building
    {

    }
}
