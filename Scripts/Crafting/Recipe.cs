﻿using LasSecretRPG.Items;
using UnityEngine;
using System.Collections.Generic;

namespace LasSecretRPG.Crafting
{
    public abstract class Recipe : ScriptableObject
    {
        public IEnumerable<ItemData> NeededItems => this.neededItems;
        public ItemData Result => this.result;

        [SerializeField] protected List<ItemData> neededItems;
        [SerializeField] protected ItemData result;
    }
}