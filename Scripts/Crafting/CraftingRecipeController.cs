﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using LasSecretRPG.Inventory.Slots;

namespace LasSecretRPG.Crafting
{
    public class CraftingRecipeController : MonoBehaviour
    {
        public Action<CraftingRecipe> OnCreateRecipe;

        public CraftingRecipe Recipe { get => this.recipe; set => this.recipe = value; }

        [SerializeField] private Button createButton;
        [SerializeField] private ImageTextConnection resultImage;
        [SerializeField] private List<ImageTextConnection> neededItemsImageAndText;

        private CraftingRecipe recipe;

        private void Start()
        {
            this.SetItemsImage();
        }

        private void SetItemsImage()
        {
            this.resultImage.UpdateSlotImage(this.recipe.Result.Item.Sprite);
            this.resultImage.UpdateSlotAmountText(this.recipe.Result.Amount);
            var neededItems = this.recipe.NeededItems;
            for (int i = 0; i < this.neededItemsImageAndText.Count; i++)
            {
                if (neededItems.Count() <= i)
                {
                    this.neededItemsImageAndText[i].DisactiveImageText();
                    continue;
                }
                var neededItem = neededItems.ElementAt(i);
                var neededItemImageAndText = this.neededItemsImageAndText[i];
                neededItemImageAndText.UpdateSlotImage(neededItem.Item.Sprite);
                neededItemImageAndText.UpdateSlotAmountText(neededItem.Amount);
            }
        }

        public bool CanBeActivate(List<CraftingBuilding> craftingBuildings)
        {
            if (this.recipe.CraftingBuilding == null) return true;
            foreach(var craftingBuilding in craftingBuildings)
            {
                if (craftingBuilding == this.recipe.CraftingBuilding) return true;
            }
            return false;
        }

        public void ActivateCreateButton()
        {
            this.createButton.onClick.AddListener(() => this.CreateRecipe());
        }

        public void DisactivateCreateButton()
        {
            this.createButton.onClick.RemoveAllListeners();
        }

        public void CreateRecipe()
        {
            this.OnCreateRecipe?.Invoke(this.recipe);
        }
    }
}