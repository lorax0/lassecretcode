using LasSecretRPG.Buildings;
using LasSecretRPG.Items;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Crafting
{
    public class CraftingController : MonoBehaviour
    {
        [SerializeField] private GameObject craftingWindow;
        [SerializeField] private Inventory.Inventory inventory;
        [SerializeField] private CraftingRecipeController craftingRecipePrefab;
        [SerializeField] private List<CraftingRecipe> recipes;

        private List<CraftingRecipeController> recipeControllers = new List<CraftingRecipeController>();
        private List<CraftingBuilding> craftingBuildingsInRange = new List<CraftingBuilding>();

        private void Start()
        {
            this.CreateRecipesInWindow();
        }

        private void CreateRecipesInWindow()
        {
            foreach(var recipe in this.recipes)
            {
                var recipeController = Instantiate(this.craftingRecipePrefab, this.craftingWindow.transform);
                recipeController.Recipe = recipe;
                recipeController.gameObject.SetActive(false);
                recipeController.OnCreateRecipe += this.AddCreatedItemToInventory;
                this.recipeControllers.Add(recipeController);
            }
        }

        public void ActivateCorrectRecipesInWindow()
        {
            foreach(var recipeController in this.recipeControllers)
            {
                if (!recipeController.CanBeActivate(this.craftingBuildingsInRange))
                {
                    recipeController.gameObject.SetActive(false);
                    return;
                }
                recipeController.DisactivateCreateButton();
                if (this.inventory.HaveItems(recipeController.Recipe.NeededItems)) recipeController.ActivateCreateButton();
                recipeController.gameObject.SetActive(true);
            }
        }

        private void AddCreatedItemToInventory(CraftingRecipe recipe)
        {
            var itemData = recipe.Result;
            itemData.RandomizeModifier();
            itemData.RandomizeElement();
            if (itemData.Item is IDuratibility duratibility) itemData.SetDurability(duratibility.MaxDurability);
            this.inventory.RemoveItems(recipe.NeededItems);
            this.inventory.AddItem(itemData);
            this.ActivateCorrectRecipesInWindow();
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            CraftingBuilding building = collider.GetComponent<BuildingController>()?.Building as CraftingBuilding;
            if (building == null) return;
            this.craftingBuildingsInRange.Add(building);
            this.ActivateCorrectRecipesInWindow();
        }

        private void OnTriggerExit2D(Collider2D collider)
        {
            CraftingBuilding building = collider.GetComponent<BuildingController>()?.Building as CraftingBuilding;
            if (building == null) return;
            this.craftingBuildingsInRange.Remove(building);
            this.ActivateCorrectRecipesInWindow();
        }
    }
}