﻿using LasSecretRPG.Items;
using UnityEngine;
using System.Collections.Generic;

namespace LasSecretRPG.Crafting
{
    [CreateAssetMenu(fileName = "New Crafting Recipe", menuName = "Crafting/Recipe")]
    public class CraftingRecipe : Recipe
    {
        public CraftingBuilding CraftingBuilding => this.craftingBuilding;
        
        [SerializeField] protected CraftingBuilding craftingBuilding;
    }
}