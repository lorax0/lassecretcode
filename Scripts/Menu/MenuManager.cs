using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Menu
{
    public class MenuManager : MonoBehaviour
    {
        [SerializeField] private Object gameScene;

        public void LoadGameScene() 
        {
            SceneLoader.Instance.LoadScene(this.gameScene);
        }

        public void ExitGame() => Application.Quit();

    }
}