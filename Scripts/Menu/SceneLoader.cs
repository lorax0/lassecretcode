using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LasSecretRPG.Menu
{
    public class SceneLoader : MonoBehaviour
    {
        public static SceneLoader Instance => instance;
        private static SceneLoader instance;

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
                return;
            }
            instance = this;
            DontDestroyOnLoad(this);
        }
        
        public IEnumerator LoadYourAsyncScene(Object sceneToLoad)
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneToLoad.name);

            while (!asyncLoad.isDone)
            {
                yield return null;
            }
        }

        public void LoadScene(Object gameScene) => this.StartCoroutine(this.LoadYourAsyncScene(gameScene));
    }
}
