using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Utilities
{
    public class ObjectPool<T> where T : MonoBehaviour
    {
        private Func<T> CreateObject;

        private Queue<T> pool = new Queue<T>();

        public void Initialize(Func<T> CreateObject, int amount)
        {
            this.CreateObject = CreateObject;
            for (int i = 0; i < amount; i++)
            {
                T newObject = this.CreateObject?.Invoke();
                this.pool.Enqueue(newObject);
            }
        }

        public T GetObject()
        {
            T poolObject = this.pool.Count == 0 ? this.CreateObject?.Invoke() : this.pool.Dequeue();
            poolObject.gameObject.SetActive(true);
            return poolObject;
        }

        public void ReturnObject(T poolObject)
        {
            poolObject.gameObject.SetActive(false);
            this.pool.Enqueue(poolObject);
        }
    }
}