using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Utilities
{
    [Serializable]
    public class MouseCursorChanger
    {
        public static Texture2D defaultCursor;

        [SerializeField] private Texture2D newMouseTexture;
        
        public void SetCursorAsDefault() => defaultCursor = this.newMouseTexture;

        public void SetNewCursor() => Cursor.SetCursor(this.newMouseTexture, Vector2.zero, CursorMode.Auto);

        public void ReturnToDefaultCursor() => Cursor.SetCursor(defaultCursor, Vector2.zero, CursorMode.Auto);
    }
}