using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Utilities
{
    public class WindowController : MonoBehaviour
    {
        [SerializeField] private List<GameObject> windows;

        public void ChangeWindow(int windowToActivate)
        {
            foreach (var window in this.windows)
            {
                window.SetActive(false);
            }
            this.windows[windowToActivate].SetActive(true);
        }
    }
}