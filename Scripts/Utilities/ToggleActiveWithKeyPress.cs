﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace LasSecretRPG.Utilities
{
    public class ToggleActiveWithKeyPress : MonoBehaviour
    {
        [SerializeField] private GameObject gameObjectToToggle = null;
        [SerializeField] private InputActionReference input = null;
        [SerializeField] private UnityEvent OnActive;

        private void Start()
        {
            this.input.action.performed += action => this.Activate();
        }

        private void OnEnable()
        {
            this.input.action.Enable();
        }

        private void OnDisable()
        {
            this.input.action.Disable();
        }
        
        private void Activate()
        {
            this.gameObjectToToggle.SetActive(!this.gameObjectToToggle.activeSelf);
            this.OnActive?.Invoke();
        }
    }
}
