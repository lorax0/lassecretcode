using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Utilities
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance => instance;
        private static GameManager instance;

        [SerializeField] private MouseCursorChanger mouseCursorChanger;

        private void Start()
        {
            this.mouseCursorChanger.SetNewCursor();
            this.mouseCursorChanger.SetCursorAsDefault();
            this.SetSingleton();
        }

        private void SetSingleton()
        {
            if (instance == null) instance = this;
            else Destroy(this.gameObject);
        }
    }
}