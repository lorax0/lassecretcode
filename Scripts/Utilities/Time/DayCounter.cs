﻿using System;
using UnityEngine;
using TMPro;
using System.Collections;
using UnityEngine.Rendering.Universal;
using System.Collections.Generic;
using LasSecretRPG.Plant;
using System.Linq;
using LasSecretRPG.Utilities.Events;

namespace LasSecretRPG.Utilities.Time
{
    [Serializable]
    public class DayCounter
    {
        public float Hour => this.hour;
        public int DayDuration => this.dayHourEnd - this.dayHourStart;
        public int DayHourStart { get => this.dayHourStart; set => this.dayHourStart = value; }
        public int DayHourEnd { get => this.dayHourEnd; set => this.dayHourEnd = value; }
        public WorldEvent WorldEvent { get => this.worldEvent; set => this.worldEvent = value; }
        public float DurationOfMinuteInRealSecond => this.durationOfMinuteInRealSecond;
        public WorldEventController WorldEventController => this.worldEventController;

        public Action OnDayEnd;

        [SerializeField] private float durationOfMinuteInRealSecond;
        [SerializeField] private TextMeshProUGUI textMeshPro;
        [SerializeField] private Light2D globalLight;
        [SerializeField] private float minimalIntensity;
        [SerializeField] private float maximalIntensity;
        [SerializeField] private int hour = 1;
        [SerializeField] private WorldEventController worldEventController;

        private int minutes;
        private int dayHourStart;
        private int dayHourEnd;
        private WorldEvent worldEvent;
        private Dictionary<IGrowable, Action> plants = new Dictionary<IGrowable, Action>();
        
        protected const int MinutesInHour = 60;
        protected const int HourInDay = 24;

        public void InitializeTimer()
        {
            this.UpdateDataText();
        }

        public IEnumerator TimeCounter()
        {
            while(this.hour < HourInDay)
            {
                yield return new WaitForSeconds(this.durationOfMinuteInRealSecond);
                this.minutes++;
                if (this.minutes == MinutesInHour)
                {
                    this.IncrementHour();
                    this.ResetMinutes();
                }
                this.UpdateDataText();
                this.ChangeLightIntensity();
            }
        }

        public void Reset()
        {
            this.hour = 0;
            this.ResetMinutes();
        }

        public bool IsDay() => this.hour < this.dayHourEnd && this.hour > this.dayHourStart;

        public void SetStartLightIntensity()
        {
            if (this.IsDay())
            {
                this.SetStartDayLightIntensity();
            }
            else
            {
                this.SetStartNightLightIntensity();
                
            }
        }

        private void SetStartNightLightIntensity()
        {
            float oneMinuteIntensityChange = this.GetCurrentTimeIntensityChange(HourInDay - this.DayDuration) * MinutesInHour * 2;
            int nightMiddleHour = (this.dayHourEnd + (HourInDay - this.DayDuration) / 2) % HourInDay;
            if (nightMiddleHour > this.dayHourEnd)
            {
                if (this.hour > nightMiddleHour)
                {
                    int overMiddle = this.hour - nightMiddleHour;
                    this.globalLight.intensity = this.minimalIntensity + overMiddle * oneMinuteIntensityChange;
                }
                else
                {
                    int missingHourToMiddle = nightMiddleHour - this.hour;
                    this.globalLight.intensity = this.minimalIntensity + missingHourToMiddle * oneMinuteIntensityChange;
                }
            }
            else
            {
                if (this.hour > nightMiddleHour && this.hour > this.dayHourEnd)
                {
                    int missingHourToMiddle = (HourInDay - this.hour) + nightMiddleHour;
                    this.globalLight.intensity = this.minimalIntensity + missingHourToMiddle * oneMinuteIntensityChange;
                }
                else if (this.hour > nightMiddleHour && this.hour < this.dayHourStart)
                {
                    int overMiddle = this.hour - nightMiddleHour;
                    this.globalLight.intensity = this.minimalIntensity + overMiddle * oneMinuteIntensityChange;
                }
                else
                {
                    int overMiddle = nightMiddleHour - this.hour;
                    this.globalLight.intensity = this.minimalIntensity + overMiddle * oneMinuteIntensityChange;
                }
            }
        }

        private void SetStartDayLightIntensity()
        {
            float oneMinuteIntensityChange = this.GetCurrentTimeIntensityChange(this.DayDuration) * MinutesInHour * 2;
            int dayMiddleHour = this.dayHourStart + this.DayDuration / 2;
            if (this.hour > dayMiddleHour)
            {
                int overMiddle = this.hour - dayMiddleHour;
                this.globalLight.intensity = this.maximalIntensity - overMiddle * oneMinuteIntensityChange;
            }
            else
            {
                int missingHourToMiddle = dayMiddleHour - this.hour;
                this.globalLight.intensity = this.maximalIntensity - missingHourToMiddle * oneMinuteIntensityChange;
            }
        }

        private void ResetMinutes() => this.minutes = 0;
        
        private void IncrementHour()
        {
            this.hour++;
            this.CheckPlantsGrow();
            if(this.hour == this.worldEvent?.StartHour)
            {
                this.worldEvent.StartEvent(this.worldEventController);
            }
            if (this.hour == HourInDay)
            {
                this.OnDayEnd?.Invoke();
            }
        }

        private void UpdateDataText()
        {
            string minutes = this.minutes >= 10? this.minutes.ToString() : "0" + this.minutes.ToString();
            string hour = this.hour >= 10? this.hour.ToString() : "0" + this.hour.ToString();
            this.textMeshPro.text = hour + ":" + minutes;
        }
        
        private void ChangeLightIntensity()
        {
            if (this.IsDay())
            {
                float oneMinuteIntensityChange = this.GetCurrentTimeIntensityChange(this.DayDuration);
                if (this.IsFirstDayHalf())
                {
                    this.IncrementLightIntensity(oneMinuteIntensityChange);
                }
                else
                {
                    this.DecrementLightIntensity(oneMinuteIntensityChange);
                }
            }
            else
            {
                float oneMinuteIntensityChange = this.GetCurrentTimeIntensityChange(HourInDay - this.DayDuration);
                if (this.IsFirstNightHalf())
                {
                    this.DecrementLightIntensity(oneMinuteIntensityChange);
                }
                else
                {
                    this.IncrementLightIntensity(oneMinuteIntensityChange);
                }
            }
        }

        private void DecrementLightIntensity(float oneMinuteIntensityChange)
        {
            this.globalLight.intensity = Mathf.Max(this.globalLight.intensity - oneMinuteIntensityChange, this.minimalIntensity);
        }

        private void IncrementLightIntensity(float oneMinuteIntensityChange)
        {
            this.globalLight.intensity = Mathf.Min(this.globalLight.intensity + oneMinuteIntensityChange, this.maximalIntensity);
        }

        private bool IsFirstDayHalf() => this.hour < this.dayHourStart + (this.DayDuration / 2);
        
        private bool IsFirstNightHalf()
        {
            bool isHourBeforeMiddleNightHour = this.hour < (this.dayHourEnd + (HourInDay - this.DayDuration) / 2) % HourInDay;
            return isHourBeforeMiddleNightHour || this.hour >= this.dayHourEnd;
        }

        private float GetCurrentTimeIntensityChange(int currentTimeDuration) => (this.maximalIntensity - this.minimalIntensity) / ((float)currentTimeDuration * MinutesInHour);
        
        private void CheckPlantsGrow()
        {
            foreach (var plant in this.plants)
            {
                int growLevel = plant.Key.GrowLevel;
                if (this.hour != plant.Key.GrowableLevels.ElementAt(growLevel).HourToGrow) continue;
                plant.Value?.Invoke();
            }
        }

        public void AddNewPlant(IGrowable growable, Action action)
        {
            this.plants.Add(growable, action);
        }

        public void RemovePlant(IGrowable growable) => this.plants.Remove(growable);
    }
}