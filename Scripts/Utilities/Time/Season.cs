﻿using LasSecretRPG.Utilities.Events;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Utilities.Time
{
    [Serializable]
    public class Season
    {
        public Sprite SeasonImage => this.seasonImage;
        public int DayHourStart => this.dayHourStart;
        public int DayHourEnd => this.dayHourEnd;
        public IEnumerable<WorldEvent> PossibleSeasonEvents => this.possibleSeasonEvents;

        [SerializeField] private Sprite seasonImage;
        [SerializeField] private int dayHourStart;
        [SerializeField] private int dayHourEnd;
        [SerializeField] private List<WorldEvent> possibleSeasonEvents;

    }
}
