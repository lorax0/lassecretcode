using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LasSecretRPG.Utilities.Events;

namespace LasSecretRPG.Utilities.Time
{
    public class TimeManager : MonoBehaviour
    {
        public static TimeManager Instance => instance;
        protected static TimeManager instance;

        public DayCounter DayCounter => this.dayCounter;
        public Season CurrentSeason => this.currentSeason;

        [SerializeField] private Image seasonImage;
        [SerializeField] private List<Season> seasons = new List<Season>(4);
        [Min(1)] [SerializeField] private int dayInSeason = 1;
        [SerializeField] private DayCounter dayCounter;

        private Season currentSeason;
        private int currentDay = 1;

        private void Awake()
        {
            this.currentSeason = this.seasons[0];
            this.SetDayDuration();
            this.InitializeDayCounter();
            this.SetSeasonDataUI();
            this.SetSingleton();
        }

        private void SetSingleton()
        {
            if (instance == null) instance = this;
            else Destroy(this.gameObject);
        }

        private void InitializeDayCounter()
        {
            this.dayCounter.SetStartLightIntensity();
            this.dayCounter.OnDayEnd += this.GoToNextDay;
            this.dayCounter.InitializeTimer();
            this.StartCoroutine(this.dayCounter.TimeCounter());
        }
        
        private void GoToNextDay()
        {
            this.dayCounter.Reset();
            this.currentDay++;
            if (this.currentDay > this.dayInSeason) this.GoToNextSeason();
            this.SetSeasonDataUI();
            this.RandomizeDayEvent();
        }

        private void SetSeasonDataUI()
        {
            this.seasonImage.sprite = this.currentSeason.SeasonImage;
        }

        private void GoToNextSeason()
        {
            int indexOfNewSeason = this.seasons.IndexOf(this.currentSeason) + 1;
            this.currentSeason = this.seasons.Count - 1 < indexOfNewSeason ? this.seasons[0] : this.seasons[indexOfNewSeason];
            this.currentDay = 1;
            this.SetDayDuration();
        }

        private void SetDayDuration()
        {
            this.dayCounter.DayHourStart = this.currentSeason.DayHourStart;
            this.dayCounter.DayHourEnd = this.currentSeason.DayHourEnd;
        }

        private void RandomizeDayEvent()
        {
            foreach(var worldEvent in this.currentSeason.PossibleSeasonEvents)
            {
                float randomNumber = RandomGenerator.RandomRange(0, 100);
                if(randomNumber <= worldEvent.Percentage)
                {
                    this.dayCounter.WorldEvent = worldEvent;
                    return;
                }
            }
        }

    }
}