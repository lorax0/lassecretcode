﻿using UnityEngine;
using System;
using System.Collections;

namespace LasSecretRPG.Utilities.Events
{
    [CreateAssetMenu(fileName = "New ParticleEvent ", menuName = "Events/ParticleEvent")]
    public class ParticleEvent : WorldEvent
    {
        public int DurationInHour => this.durationInHour;
        
        [SerializeField] private int durationInHour;
        [SerializeField] private ParticleSystem particleSystem;

        protected Transform particleSystemController;

        public override void StartEvent(WorldEventController worldEventController)
        {
            base.StartEvent(worldEventController);
            this.particleSystemController = Instantiate(this.particleSystem.gameObject, worldEventController.transform).transform;
        }

        public override void EndEvent()
        {
            base.EndEvent();
            Destroy(this.particleSystemController.gameObject);
        }

        private IEnumerator StartEventCounter()
        {
            float counterDuration = 60 * this.durationInHour * this.worldEventController.TimeManager.DayCounter.DurationOfMinuteInRealSecond;
            yield return new WaitForSeconds(counterDuration);
            this.EndEvent();
        }
    }
}