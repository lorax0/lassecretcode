﻿using UnityEngine;
using System;
using LasSecretRPG.Enemies;

namespace LasSecretRPG.Utilities.Events
{
    [CreateAssetMenu(fileName = "NewBossEvent ", menuName = "Events/BossEvent")]
    public class BossEvent : WorldEvent
    {
        [SerializeField] private Enemy bossToSpawn;

        public override void StartEvent(WorldEventController worldEventController)
        {
            base.StartEvent(worldEventController);
            worldEventController.SpawnBoss(this.bossToSpawn);
        }

        public override void EndEvent()
        {
            base.EndEvent();
        }
    }
}