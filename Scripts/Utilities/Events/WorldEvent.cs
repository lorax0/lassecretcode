﻿using UnityEngine;
using System;

namespace LasSecretRPG.Utilities.Events
{
    public class WorldEvent : ScriptableObject
    {
        public int Percentage => this.percentage;
        public int StartHour => this.startHour;

        [SerializeField] protected int percentage;
        [SerializeField] protected int startHour;

        protected WorldEventController worldEventController;

        public virtual void StartEvent(WorldEventController worldEventController)
        {
            this.worldEventController = worldEventController;
        }

        public virtual void EndEvent()
        {

        }
    }
}