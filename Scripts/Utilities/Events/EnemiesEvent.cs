﻿using UnityEngine;
using System;
using LasSecretRPG.Enemies;
using LasSecretRPG.EnemySpawn;

namespace LasSecretRPG.Utilities.Events
{
    [CreateAssetMenu(fileName = "NewEnemiesEvent ", menuName = "Events/EnemiesEvent")]
    public class EnemiesEvent : WorldEvent
    {
        [SerializeField] private EnemySpawnService enemySpawnService;

        public override void StartEvent(WorldEventController worldEventController)
        {
            base.StartEvent(worldEventController);
            worldEventController.EventSpawnService = this.enemySpawnService;
        }

        public override void EndEvent()
        {
            base.EndEvent();
            this.worldEventController.EventSpawnService = null;
        }
    }
}