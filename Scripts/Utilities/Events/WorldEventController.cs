﻿using LasSecretRPG.Enemies;
using LasSecretRPG.EnemySpawn;
using LasSecretRPG.Utilities.Time;
using UnityEngine;
using Zenject;

namespace LasSecretRPG.Utilities.Events
{
    public class WorldEventController : MonoBehaviour
    {
        public EnemySpawnService EventSpawnService { get => this.eventSpawnService; set => this.eventSpawnService = value; }
        public TimeManager TimeManager => this.timeManager;

        private EnemySpawnService eventSpawnService;
        [Inject] private TimeManager timeManager;

        public void SpawnBoss(Enemy boss)
        {

        }

    }
}