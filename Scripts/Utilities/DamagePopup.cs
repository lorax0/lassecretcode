using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

namespace LasSecretRPG.Utilities
{
    public class DamagePopup : MonoBehaviour
    {
        public static DamagePopup Instance => instance;
        private static DamagePopup instance;

        [SerializeField] private TextMeshProUGUI popupText;
        [SerializeField] private float maxOffset;
        [SerializeField] private float visibleTime = 1f;

        private void Awake()
        {
            if (instance != null && instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                instance = this;
            }
        }

        public void DisplayText(string text, Vector3 position)
        {
            this.popupText.text = text;
            this.transform.position = Camera.main.WorldToScreenPoint(position);
            this.popupText.gameObject.SetActive(true);
            this.transform.position += new Vector3(RandomGenerator.RandomRange(-this.maxOffset, this.maxOffset), RandomGenerator.RandomRange(-this.maxOffset, this.maxOffset), 0f);
            this.StartCoroutine(this.Unactive());
        }

        private IEnumerator Unactive()
        {
            yield return new WaitForSeconds(this.visibleTime);
            this.popupText.gameObject.SetActive(false);
        }
    }

}