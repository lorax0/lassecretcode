﻿using LasSecretRPG.Bestiary;
using LasSecretRPG.MapInstances;
using LasSecretRPG.Utilities.Time;
using UnityEngine;
using Zenject;

namespace LasSecretRPG.Utilities
{
    public class GameplayInstaller : MonoInstaller<GameplayInstaller>
    {
        [SerializeField] private BestiaryController bestiaryController;
        [SerializeField] private TimeManager timeManager;

        public override void InstallBindings()
        {
            this.Container.Bind<BestiaryController>().FromInstance(this.bestiaryController).AsSingle();
            this.Container.Bind<TimeManager>().FromInstance(this.timeManager).AsSingle();
        }
    }
}
