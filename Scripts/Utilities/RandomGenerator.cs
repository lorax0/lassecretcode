using System;

namespace LasSecretRPG.Utilities
{
    public static class RandomGenerator
    {
        private static Random seedRandom;

        public static void Initialize(int seed)
        {
            seedRandom = new Random(seed);
        }

        public static int RangeBySeed(int minInclusive, int maxInclusive) => seedRandom.Next(minInclusive, maxInclusive);

        public static float RangeBySeed(float minInclusive, float maxInclusive) => (float)seedRandom.NextDouble() * (maxInclusive - minInclusive) + maxInclusive;

        public static int RandomRange(int minInclusive, int maxInclusive) => new Random().Next(minInclusive, maxInclusive);

        public static float RandomRange(float minInclusive, float maxInclusive) => (float)new Random().NextDouble() * (maxInclusive - minInclusive) + maxInclusive;

        public static double NextDoubleBySeed() => seedRandom.NextDouble();
    }
}