using System.Collections;
using UnityEngine;

namespace LasSecretRPG.Utilities
{
    [RequireComponent(typeof(Camera))]
    public class ScreenShaker : MonoBehaviour
    {
        public static ScreenShaker Instance => instance;
        private static ScreenShaker instance;

        [SerializeField] private float rotationMultiplier;

        private void Awake()
        {
            if (instance == null) instance = this;
            else Destroy(this.gameObject);
        }

        private void Start()
        {
            this.StartShake(2f, .5f);
        }

        public void StartShake(float duration, float power)
        {
            this.StartCoroutine(this.Shake(duration, power));
        }

        private IEnumerator Shake(float duration, float power)
        {
            float shakeFadeTime = power / duration;
            float shakeRotation = power * this.rotationMultiplier;
            while(duration > 0f)
            {
                shakeRotation = Mathf.MoveTowards(shakeRotation, 0f, shakeFadeTime * this.rotationMultiplier * UnityEngine.Time.deltaTime);

                this.transform.rotation = Quaternion.Euler(0f, 0f, shakeRotation * RandomGenerator.RandomRange(-1f, 1f));

                yield return new WaitForFixedUpdate();
                duration -= UnityEngine.Time.deltaTime;
            }
        }
    }
}