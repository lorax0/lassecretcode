﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace LasSecretRPG.Utilities
{
    public class InputManager : MonoBehaviour
    {
        public static InputManager Instance => instance;
        private static InputManager instance;

        [SerializeField] private InputActionAsset inputActionsAsset;

        private void Awake()
        {
            if (instance == null) instance = this;
            else Destroy(this.gameObject);
        }

        public void ActiveActionMap(string actionMapName)
        {
            foreach (var actionMap in this.inputActionsAsset)
            {
                actionMap.Disable();
            }
            this.inputActionsAsset.FindActionMap(actionMapName).Enable();
        }
    }
}
