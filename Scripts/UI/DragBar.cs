using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace LasSecretRPG.UI
{
    public class DragBar : MonoBehaviour, IDragHandler, IPointerDownHandler
    {
        [SerializeField] private Canvas windowCanvas;
        [SerializeField] private RectTransform draggableWindow;

        public void OnDrag(PointerEventData eventData)
        {
            this.draggableWindow.anchoredPosition += eventData.delta / this.windowCanvas.scaleFactor;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            this.draggableWindow.SetAsLastSibling();
        }
    }
}