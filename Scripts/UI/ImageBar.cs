using LasSecretRPG.Utilities;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace LasSecretRPG.UI
{
    [Serializable]
    public class ImageBar
    {
        public bool IsSet => this.frontBar != null && this.backBar != null;

        [SerializeField] private Image frontBar;
        [SerializeField] private Image backBar;
        [SerializeField] private float chipSpeed;
        
        private Coroutine updateBar;

        public void UpdateBar(float currentValue, float maxValue)
        {
            float valuePercentage = currentValue / maxValue;

            if (this.updateBar != null) GameManager.Instance.StopCoroutine(this.updateBar);

            if (this.frontBar.fillAmount < valuePercentage)
            {
                this.updateBar = GameManager.Instance.StartCoroutine(this.UpdateBar(this.frontBar, valuePercentage, 1));
                this.backBar.fillAmount = this.frontBar.fillAmount;
            }

            else if (this.backBar.fillAmount > valuePercentage || this.frontBar.fillAmount > valuePercentage)
            {
                this.frontBar.fillAmount = valuePercentage;
                this.updateBar = GameManager.Instance.StartCoroutine(this.UpdateBar(this.backBar, valuePercentage, this.chipSpeed));
            }
        }

        private IEnumerator UpdateBar(Image bar, float valuePercentage, float speed)
        {
            float lerpTimer = 0f;
            while (bar.fillAmount != valuePercentage)
            {
                lerpTimer += Time.deltaTime;
                float percentege = lerpTimer / speed;
                bar.fillAmount = Mathf.Lerp(bar.fillAmount, valuePercentage, percentege);
                yield return new WaitForFixedUpdate();
            }
            this.updateBar = null;
        }
    }
}