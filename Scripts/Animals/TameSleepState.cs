﻿using LasSecretRPG.Player;
using UnityEngine;

namespace LasSecretRPG.Animals
{
    public class TameSleepState : State
    {
        private PlayerController player;

        public TameSleepState(PlayerController player, Animal animal)
        {
            Debug.Log("D");
            this.animal = animal;
            this.player = player;
        }

        public override void DoStateAction()
        {
            this.Wait();
        }
        
        private void Wait()
        {
            if(Vector2.Distance(this.animal.transform.position, this.player.transform.position) > this.animal.StartFollowingPlayerDistance)
            {
                this.animal.ChangeState(new TameWalkingState(this.player, this.animal), this.animal.WalkingAnimation);
            }
        }
    }
}