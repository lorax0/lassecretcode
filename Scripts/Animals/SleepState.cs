﻿using LasSecretRPG.Damageable;
using LasSecretRPG.Utilities;
using UnityEngine;

namespace LasSecretRPG.Animals
{
    public class SleepState : State
    {
        private float counter;

        public SleepState(Animal animal)
        {
            this.animal = animal;
            this.counter = this.RandomizeTime();
        }

        public override void DoStateAction()
        {
            if (this.counter <= 0f) this.animal.ChangeState(new WalkingState(this.animal), this.animal.WalkingAnimation);
            this.counter -= Time.deltaTime;
        }

        private float RandomizeTime() => RandomGenerator.RandomRange(this.animal.MinTimeToMove, this.animal.MaxTimeToMove);
    }
}