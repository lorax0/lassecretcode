﻿using LasSecretRPG.Damageable;
using LasSecretRPG.Utilities;
using UnityEngine;

namespace LasSecretRPG.Animals
{
    public class WalkingState : State
    {
        private Vector2 targetPosition;

        public WalkingState(Animal animal)
        {
            this.animal = animal;
            this.targetPosition = this.GetRandomDirection() * RandomGenerator.RandomRange(this.animal.MinDistanceToMove, this.animal.MaxDistanceToMove);
        }

        public override void DoStateAction()
        {
            this.Move();
        }
        
        private void Move()
        {
            if (Vector2.Distance(this.animal.transform.position, this.targetPosition) < 0.1)
            {
                this.animal.ChangeState(new SleepState(this.animal), this.animal.IdleAnimation);
                return;
            }
            Vector2 direction = (Vector3)this.targetPosition - this.animal.transform.position;

            this.Rotate(direction);
            this.Move(direction);
        }

        public Vector2 GetRandomDirection()
        {
            float random = RandomGenerator.RandomRange(0f, 260f);
            return new Vector2(Mathf.Cos(random), Mathf.Sin(random));
        }
    }
}