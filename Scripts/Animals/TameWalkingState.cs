﻿using LasSecretRPG.Player;
using UnityEngine;

namespace LasSecretRPG.Animals
{
    public class TameWalkingState : State
    {
        private PlayerController player;

        public TameWalkingState(PlayerController player, Animal animal)
        {
            Debug.Log("D");
            this.animal = animal;
            this.player = player;
        }

        public override void DoStateAction()
        {
            this.Follow();
        }
        
        private void Follow()
        {
            if (Vector2.Distance(this.animal.transform.position, this.player.transform.position) < this.animal.EndFollowingPlayerDistance)
            {
                this.animal.ChangeState(new TameSleepState(this.player, this.animal), this.animal.IdleAnimation);
                return;
            }
            Vector2 direction = this.player.transform.position - this.animal.transform.position;

            this.Rotate(direction);
            this.Move(direction);
        }
    }
}