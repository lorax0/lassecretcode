using LasSecretRPG.Damageable;
using LasSecretRPG.Damageable.Knockback;
using LasSecretRPG.Items;
using LasSecretRPG.Items.Consumable;
using LasSecretRPG.Player;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Animals
{
    public class Animal : DamageableUnit
    {
        public float MovementSpeed => this.movementSpeed;
        public float MinimalDistanceToKiler => this.minimalDistanceToKiler;
        public float MinTimeToMove => this.minTimeToMove;
        public float MaxTimeToMove => this.maxTimeToMove;
        public float MinDistanceToMove => this.minDistanceToMove;
        public float MaxDistanceToMove => this.maxDistanceToMove;
        public float StartFollowingPlayerDistance => this.startFollowingPlayerDistance;
        public float EndFollowingPlayerDistance => this.endFollowingPlayerDistance;
        public AnimationClip WalkingAnimation => this.walkingAnimation;
        public AnimationClip IdleAnimation => this.idleAnimation;
        public SpriteRenderer SpriteRenderer => this.spriteRenderer;

        [SerializeField] protected float movementSpeed;
        [SerializeField] protected float minTimeToMove;
        [SerializeField] protected float maxTimeToMove;
        [SerializeField] protected float minDistanceToMove;
        [SerializeField] protected float maxDistanceToMove;
        [SerializeField] protected float minimalDistanceToKiler;
        [SerializeField] protected float endFollowingPlayerDistance;
        [SerializeField] protected float startFollowingPlayerDistance;
        [SerializeField] protected List<Food> foodToTame;

        [SerializeField] protected AnimationClip idleAnimation;
        [SerializeField] protected AnimationClip walkingAnimation;
        [SerializeField] protected AnimationClip hitAnimation;
        [SerializeField] private Animator animator;
        [SerializeField] private SpriteRenderer spriteRenderer;

        protected State currentState;
        private AnimationClip currentAnimation;

        public override void Start()
        {
            base.Start();
            this.ChangeState(new SleepState(this), this.idleAnimation);
            this.healthService.OnTakeDamageEvent += (Vector3 killerPosition, PowerKnockbackTier powerKnockbackTier, float damage) => this.ChangeState(new EscapeState(this, killerPosition), this.idleAnimation);
        }

        public virtual void Update()
        {
            this.currentState.DoStateAction();
        }

        public void ChangeState(State state, AnimationClip animation)
        {
            this.currentState = state;
            this.ChangeAnimationState(animation);
        }

        public void Feed(PlayerController player)
        {
            this.ChangeState(new TameWalkingState(player, this), this.idleAnimation);
        }

        public bool CanTame(Food food) => this.foodToTame.Contains(food);

        public void ChangeAnimationState(AnimationClip newAnimationState)
        {
            if (this.currentAnimation == newAnimationState)
                return;

            this.animator.Play(newAnimationState.name);
            this.currentAnimation = newAnimationState;
        }
    }
}