﻿using UnityEngine;

namespace LasSecretRPG.Animals
{
    public abstract class State
    {
        public Animal Animal { get => this.animal; set => this.animal = value; }

        protected Animal animal;
        protected const float movementMultiplier = 0.1f;

        public abstract void DoStateAction();

        public void Move(Vector2 direction)
        {
            this.animal.transform.position += (Vector3)direction.normalized * this.animal.MovementSpeed * Time.deltaTime * movementMultiplier;
        }

        public void Rotate(Vector2 direction) => this.animal.SpriteRenderer.flipX = direction.normalized.x > 0;

    }
}