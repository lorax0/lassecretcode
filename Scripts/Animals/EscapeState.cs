﻿using LasSecretRPG.Damageable;
using UnityEngine;

namespace LasSecretRPG.Animals
{
    public class EscapeState : State
    {
        private Vector3 enemyPosition;

        public EscapeState(Animal animal, Vector3 enemyPosition)
        {
            this.animal = animal;
            this.enemyPosition = enemyPosition;
        }

        public override void DoStateAction() => this.EscapeFromTarget();

        private void EscapeFromTarget()
        {
            if (Vector2.Distance(this.animal.transform.position, this.enemyPosition) > this.animal.MinimalDistanceToKiler)
            {
                this.animal.ChangeState(new SleepState(this.animal), this.animal.IdleAnimation);
                return;
            }

            Vector2 direction = this.enemyPosition - this.animal.transform.position;
            this.Rotate(direction);
            this.Move(direction);
        }
    }
}