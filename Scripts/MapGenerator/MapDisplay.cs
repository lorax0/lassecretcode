﻿namespace LasSecretRPG.MapGeneration
{
	using UnityEngine;
	using System.Collections;
	using UnityEngine.Tilemaps;

	public class MapDisplay : MonoBehaviour
	{
		[SerializeField] private Renderer textureRender;

		public void DrawTexture(Texture2D texture)
		{
			this.textureRender.sharedMaterial.mainTexture = texture;
			this.textureRender.transform.localScale = new Vector3(texture.width, 1, texture.height);
		}
	}

}