namespace LasSecretRPG.MapGeneration
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Tilemaps;

    public class MapRendering : MonoBehaviour
    {
        [SerializeField] private int renderDistance;
        [SerializeField] private Tilemap tilemap;
        [SerializeField] private Transform player;
        [SerializeField] private MapGenerator mapGenerator;

        private void Update()
        {
            this.RenderMap();
        }

        private void RenderMap()
        {
            if (this.mapGenerator.TerrainMap == null || this.mapGenerator.BiomeMap == null)
            {
                Debug.Log("Please wait for generating map");
                return;
            }

            int playerPositionX = Mathf.RoundToInt(this.player.transform.position.x);
            int playerPositionY = Mathf.RoundToInt(this.player.transform.position.y);

            for (int x = playerPositionX - this.renderDistance; x < playerPositionX + this.renderDistance; x++)
            {
                for (int y = playerPositionY - this.renderDistance; y < playerPositionY + this.renderDistance; y++)
                {
                    if (this.tilemap.GetTile(new Vector3Int(x, y, 0)) != null)
                    {
                        continue;
                    }

                    if (x > this.mapGenerator.MapSize || x < 0 || y > this.mapGenerator.MapSize || y < 0)
                        continue;

                    if (this.mapGenerator.TerrainMap[x, y] > this.mapGenerator.Regions[0].height)
                    {
                        for (int i = 0; i < this.mapGenerator.Biomes.Length; i++)
                        {
                            float currentBiome = this.mapGenerator.BiomeMap[x, y];
                            if (currentBiome <= this.mapGenerator.Biomes[i].height)
                            {
                                this.tilemap.SetTile(new Vector3Int(x, y, 0), this.mapGenerator.Biomes[i].tile);
                                break;
                            }
                        }
                    }
                    else
                    {
                        this.tilemap.SetTile(new Vector3Int(x, y, 0), this.mapGenerator.Regions[0].tile);
                    }

                }
            }
        }
    }

}