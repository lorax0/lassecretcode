using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.MapGeneration
{
    public interface IResourceGenerator
    {
        List<WeightedSpawn> StructurePrefabs { get; }

        WorldStructure GetObjectToSpawn();
        float CalculateWeight();
        void Generate(Color[] biomeColourMap, Biome treeBiome, float worldScale, int mapSize, bool useFalloff);
    }
}
