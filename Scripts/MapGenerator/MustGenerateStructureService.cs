using LasSecretRPG.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LasSecretRPG.MapGeneration
{
    [Serializable]
    public class MustGenerateStructureService : IResourceGenerator
    {
        public List<WeightedSpawn> StructurePrefabs => this.structurePrefabs;

        [Min(1)] [SerializeField] private int minStructuresToSpawn;
        [Min(1)] [SerializeField] private int maxStructuresToSpawn;
        [SerializeField] private List<WeightedSpawn> structurePrefabs;
        
        private float totalWeight;

        public void Generate(Color[] biomeColourMap, Biome treeBiome, float worldScale, int mapSize, bool useFalloff)
        {
            if (this.structurePrefabs.Count == 0)
                return;

            this.totalWeight = this.CalculateWeight();
            int structuresToSpawn = RandomGenerator.RangeBySeed(this.minStructuresToSpawn, this.maxStructuresToSpawn);

            for (int i = 0; i < structuresToSpawn; i++)
            {
                ResourceInfo resourceInfo = new ResourceInfo(this.GetObjectToSpawn(), Vector2.zero, true);
                do
                {
                    float xOffset = (float)RandomGenerator.NextDoubleBySeed();
                    float yOffset = (float)RandomGenerator.NextDoubleBySeed();
                    int index = RandomGenerator.RangeBySeed(0, treeBiome.BiomeLocations.Count - 1);
                    
                    Vector2Int locationToSpawn = treeBiome.BiomeLocations[index];
                    Vector2 position = new Vector2(locationToSpawn.x + xOffset, locationToSpawn.y + yOffset) * worldScale;
                    resourceInfo.position = position;
                } while (DrawChunkResources.Instance.IsStructureOnPosition(resourceInfo, true));
                
                DrawChunkResources.Instance.GetChunk(resourceInfo.position).ChunkResources.Push(resourceInfo);
            }
        }

        public WorldStructure GetObjectToSpawn()
        {
            float randomNumber = (float)RandomGenerator.NextDoubleBySeed();
            float currentWeight = 0f;
            for (int i = 0; i < this.structurePrefabs.Count; i++)
            {
                currentWeight += this.structurePrefabs[i].weight;
                if (randomNumber < currentWeight / this.totalWeight)
                {
                    return this.structurePrefabs[i].worldStructure;
                }
            }
            return this.structurePrefabs[0].worldStructure;
        }
        
        public float CalculateWeight() => this.structurePrefabs.Sum(t => t.weight);
    }
}