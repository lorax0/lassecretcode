using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.MapGeneration
{
    public class ResourceInfo
    {
        public WorldStructure worldStructure;
        public Vector2 position;
        public bool isPrimary;

        public ResourceInfo(WorldStructure worldStructure, Vector2 position, bool isPrimary)
        {
            this.worldStructure = worldStructure;
            this.position = position;
            this.isPrimary = isPrimary;
        }
    }

}