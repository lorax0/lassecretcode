﻿namespace LasSecretRPG.MapGeneration
{
    using System.Collections.Generic;
    using UnityEngine;
	using UnityEngine.Tilemaps;
	using LasSecretRPG.EnemySpawn;

	[CreateAssetMenu(fileName = "new Biome", menuName = "WorldGeneration/Biomes")]
	public class Biome : ScriptableObject
	{
        public List<Vector2Int> BiomeLocations => this.biomeLocations;

		public float height;
		public Color colour;
		//TODO: zamienic tile na scriptable objecty ktore przechowuja sprite'y i inne info o biomie
		public TileBase tile;

		public EnemySpawnService enemySpawnService;
		public MustGenerateStructureService[] mustGenerateStructureService;
		public ResourceGeneratorService[] resourceGeneratorService;

		private List<Vector2Int> biomeLocations = new List<Vector2Int>();
    }
}