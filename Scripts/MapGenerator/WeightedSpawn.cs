using UnityEngine;

namespace LasSecretRPG.MapGeneration
{
    [System.Serializable]
    public class WeightedSpawn
    {
        public WorldStructure worldStructure;
        public float weight;
    }
}