﻿namespace LasSecretRPG.MapGeneration
{
	using UnityEngine;

	[System.Serializable]
	public class NoiseValues
	{
		public float scale;

		public int octaves;
		[Range(0, 1)]
		public float persistance;
		public float lacunarity;

		public Vector2 offset;
	}

}