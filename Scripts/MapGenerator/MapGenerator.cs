﻿namespace LasSecretRPG.MapGeneration
{
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Debug = UnityEngine.Debug;
    using System.Threading;
    using System.Threading.Tasks;
    using LasSecretRPG.EnemySpawn;
    using LasSecretRPG.Utilities;
    using System;

    public class MapGenerator : MonoBehaviour
    {
        public static MapGenerator Instance { get; private set; }
        public int MapSize => this.mapSize;
        public TerrainType[] Regions => this.regions;
        public Biome[] Biomes => this.biomes;
        public float[,] TerrainMap => this.terrainMap;
        public float[,] BiomeMap => this.biomeMap;
        public int MaxTreeOffset => this.maxTreeOffset;
        public LayerMask WhatIsStructure => this.whatIsStructure;
        public Dictionary<Vector2Int, Biome> BiomesPositionMap => this.biomesPositionMap;

        [SerializeField] private EnemySpawnerManager enemySpawnerManager;
        [SerializeField] private MapDisplay display;
        [SerializeField] private DrawMode drawMode;
        [SerializeField] private bool useFalloff;
        [SerializeField] private int mapSize;
        [SerializeField] private int seed;
        [SerializeField] private NoiseValues heightMapNoiseValues;
        [SerializeField] private BiomeNoiseValues biomeMapNoiseValues;
        [SerializeField] private TerrainType[] regions;
        [SerializeField] private Biome[] biomes;

        [SerializeField] private float worldScale;
        [SerializeField] private int maxTreeOffset;
        [SerializeField] private LayerMask whatIsStructure;

        [SerializeField] private Transform resourcesParent;
        private float[,] terrainMap;
        private float[,] biomeMap;
        private Dictionary<Vector2Int, Biome> biomesPositionMap = new Dictionary<Vector2Int, Biome>();

        private void Awake()
        {
            if(Instance == null)
                Instance = this;
            else
                Destroy(Instance);

            RandomGenerator.Initialize(this.seed);
            this.GenerateMap();
        }

        public void GenerateMap()
        {
            _ = this.GenerateMapAsync();
        }

        private async Task GenerateMapAsync()
        {
            Debug.Log("Generating map started. Please wait...");

            Stopwatch worldGeneratingWatch = new Stopwatch();
            worldGeneratingWatch.Start();

            Task terrainThread = Task.Factory.StartNew(() => this.GenerateTerrainValues());
            Task biomeThread = Task.Factory.StartNew(() => this.GenerateBiomeValues());
            await Task.WhenAll(terrainThread, biomeThread);

            this.RenderMap();

            Debug.Log($"Generating whole world took: {worldGeneratingWatch.ElapsedMilliseconds / 1000f} sec");
        }

        private void RenderMap()
        {
            Stopwatch renderMapWatch = new Stopwatch();
            renderMapWatch.Start();

            if (this.drawMode == DrawMode.PlayMode)
            {
                Color[] colourMap = this.GetColourMapValues();
                this.display.DrawTexture(TextureGenerator.TextureFromColourMap(colourMap, this.mapSize, this.mapSize));

                //Must generate structures first
                foreach (var biome in this.biomes)
                {
                    foreach (var service in biome.mustGenerateStructureService)
                    {
                        service.Generate(colourMap, biome, this.worldScale, this.mapSize, this.useFalloff);
                    }
                }

                //Executing on seperate threads
                foreach (var biome in this.biomes)
                {
                    foreach (var service in biome.resourceGeneratorService)
                    {
                        service.Generate(colourMap, biome, this.worldScale, this.mapSize, this.useFalloff);
                    }
                }
                DrawChunkResources.Instance.CheckStructures();
                this.enemySpawnerManager.enabled = true;
            }
            else if (this.drawMode == DrawMode.NoiseMap)
            {
                this.display.DrawTexture(TextureGenerator.TextureFromHeightMap(this.terrainMap));
            }
            else if (this.drawMode == DrawMode.NoiseMapWithBiomes)
            {
                float[,] mapValues = this.GetMapValues();
                this.display.DrawTexture(TextureGenerator.TextureFromHeightMap(mapValues));
            }
            else if (this.drawMode == DrawMode.ColourMap)
            {
                Color[] colourMap = this.GetColourMapValues();
                this.display.DrawTexture(TextureGenerator.TextureFromColourMap(colourMap, this.mapSize, this.mapSize));
            }
            else if (this.drawMode == DrawMode.StructuresMap)
            {
                Color[] colourMap = this.GetColourMapValues();

                for (int i = 0; i < this.biomes.Length; i++)
                {
                    for (int j = 0; j < this.biomes[i].resourceGeneratorService.Length; j++)
                    {
                        float[,] treeMap = Noise.GenerateNoiseMap(this.mapSize, RandomGenerator.RangeBySeed(0, 100), this.biomes[i].resourceGeneratorService[j].TreeNoiseValues, this.useFalloff);
                        colourMap = TextureGenerator.ColorMapFromTreeMap(colourMap, this.biomes[i], this.terrainMap, treeMap, this.biomes[i].resourceGeneratorService[j].TreeMinHeight,
                            this.biomes[i].resourceGeneratorService[j].TreeSpawnThreshold, this.worldScale, this.biomes[i].resourceGeneratorService[j].TreeCurve);
                    }
                }
                this.display.DrawTexture(TextureGenerator.TextureFromColourMap(colourMap, this.mapSize, this.mapSize));
            }
            else if (this.drawMode == DrawMode.FalloffMap)
            {
                this.display.DrawTexture(TextureGenerator.TextureFromHeightMap(FalloffGenerator.GenerateFalloffMap(this.mapSize)));
            }

            renderMapWatch.Stop();
            var timeForRendering = renderMapWatch.ElapsedMilliseconds;
            Debug.Log("Rendering map took: " + timeForRendering / 1000f + " sec");
        }

        private void GenerateBiomeValues()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            this.biomeMap = Noise.GenerateBiomeMap(this.mapSize, this.seed, this.biomeMapNoiseValues);

            watch.Stop();
            var time = watch.ElapsedMilliseconds;
            Debug.Log("Generating biome map took: " + time / 1000f + " sec");
        }

        private void GenerateTerrainValues()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            this.terrainMap = Noise.GenerateNoiseMap(this.mapSize, this.seed, this.heightMapNoiseValues, this.useFalloff);

            watch.Stop();
            var time = watch.ElapsedMilliseconds;
            Debug.Log("Generating terrain map took: " + time / 1000f + " sec");
        }

        private Color[] GetColourMapValues()
        {
            foreach (var biome in this.biomes)
            {
                biome.BiomeLocations.Clear();
            }
            Color[] colourMap = new Color[this.mapSize * this.mapSize];
            for (int y = 0; y < this.mapSize; y++)
            {
                for (int x = 0; x < this.mapSize; x++)
                {
                    if (this.terrainMap[x, y] > this.regions[0].height)
                    {
                        for (int i = 0; i < this.biomes.Length; i++)
                        {
                            float currentBiome = this.biomeMap[x, y];
                            if (currentBiome <= this.biomes[i].height)
                            {
                                this.biomesPositionMap.Add(new Vector2Int(x, y), this.biomes[i]);
                                this.biomes[i].BiomeLocations.Add(new Vector2Int(x, y));
                                colourMap[y * this.mapSize + x] = this.biomes[i].colour;
                                break;
                            }
                        }
                    }
                    else
                    {
                        colourMap[y * this.mapSize + x] = this.regions[0].colour;
                    }
                }
            }
            return colourMap;
        }

        private float[,] GetMapValues()
        {
            float[,] mapValues = new float[this.mapSize, this.mapSize];
            for (int x = 0; x < this.mapSize; x++)
            {
                for (int y = 0; y < this.mapSize; y++)
                {
                    if (this.terrainMap[x, y] > this.regions[0].height)
                    {
                        for (int i = 0; i < this.biomes.Length; i++)
                        {
                            float currentBiome = this.biomeMap[x, y];
                            if (currentBiome <= this.biomes[i].height)
                            {
                                mapValues[x, y] = this.biomes[i].height;
                                break;
                            }
                        }
                    }
                    else
                    {
                        mapValues[x, y] = this.regions[0].height;
                    }
                }
            }

            return mapValues;
        }

        private void OnValidate()
        {
            if (this.mapSize < 1)
            {
                this.mapSize = 1;
            }

            if (this.heightMapNoiseValues.lacunarity < 1)
            {
                this.heightMapNoiseValues.lacunarity = 1;
            }
            if (this.heightMapNoiseValues.octaves < 0)
            {
                this.heightMapNoiseValues.octaves = 0;
            }

            this.display = FindObjectOfType<MapDisplay>();
        }
    }
}