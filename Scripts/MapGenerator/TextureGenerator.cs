﻿using UnityEngine;
using System.Collections;
using UnityEngine.Tilemaps;
using System;
using LasSecretRPG.Utilities;

namespace LasSecretRPG.MapGeneration
{
	public static class TextureGenerator
	{
		public static Texture2D TextureFromColourMap(Color[] colourMap, int width, int height)
		{
			Texture2D texture = new Texture2D(width, height);
			texture.filterMode = FilterMode.Point;
			texture.wrapMode = TextureWrapMode.Clamp;
			texture.SetPixels(colourMap);
			texture.Apply();
			return texture;
		}

		public static Texture2D TextureFromHeightMap(float[,] heightMap)
		{
			int width = heightMap.GetLength(0);
			int height = heightMap.GetLength(1);

			Color[] colourMap = new Color[width * height];
			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					colourMap[y * width + x] = Color.Lerp(Color.black, Color.white, heightMap[x, y]);
				}
			}

			return TextureFromColourMap(colourMap, width, height);
		}

		public static Color[] ColorMapFromTreeMap(Color[] colourMap, Biome biome, float[,] terrrainMap, float[,] treeMap, float minSpawnHeight, float spawnThreshold, float worldScale, AnimationCurve animationCurve)
		{
			int width = treeMap.GetLength(0);
			int height = treeMap.GetLength(1);

			float topLeftX = (float)(width - 1) / -2f;
			float topLeftY = (float)(height - 1) / 2f;

			Color treeColor = new Color(RandomGenerator.RangeBySeed(0f, 1f), RandomGenerator.RangeBySeed(0f, 1f), RandomGenerator.RangeBySeed(0f, 1f));

			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					if(biome.colour != colourMap[y * width + x])
                    {
						continue;
                    }


					if (treeMap[x, y] < minSpawnHeight)
                    {
						continue;
					}

					float currentTree = treeMap[x, y];
					if (currentTree < spawnThreshold)
                    {
						continue;
					}
					currentTree = (currentTree - spawnThreshold) / (1f - spawnThreshold);
					float num = animationCurve.Evaluate(RandomGenerator.RangeBySeed(0f, 1f));
					if(!(num > 1f - currentTree))
                    {
						continue;
                    }

					int rand = RandomGenerator.RangeBySeed(0, 10);

					colourMap[(y * width) + rand + (x + rand)] = treeColor;
                    //Vector3 treePosition = new Vector2(topLeftX + (float)x, topLeftY - (float)y) * worldScale;
					//Debug.DrawRay(treePosition, treePosition, Color.red, 10f);
                }
            }

			return colourMap;
		}
	}

}