using LasSecretRPG.Utilities;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

namespace LasSecretRPG.MapGeneration
{
    public class DrawChunkResources : MonoBehaviour
    {
        public static DrawChunkResources Instance;

        public List<Chunk> chunks = new List<Chunk>();

        [SerializeField] private float renderDistance;
        [SerializeField] private Transform structuresParent;
        [SerializeField] private float distanceToUpdate;
        [SerializeField] private float updateFrequency;
        [SerializeField] private int poolsStartAmount;
        [SerializeField] private int chunkSize;
        
        private Transform player;
        private List<Chunk> currentChunks = new List<Chunk>();
        private Dictionary<string, ObjectPool<WorldStructure>> worldStructurePools = new Dictionary<string, ObjectPool<WorldStructure>>();
        
        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(Instance);

            //TODO: Change searching by tag
            this.player = GameObject.FindWithTag("Player").transform;
            this.CreateChunks();
            this.InvokeRepeating(nameof(this.UpdateChunk), 0f, this.updateFrequency);
        }

        private void CreateChunks()
        {
            int mapSize = MapGenerator.Instance.MapSize;
            for (int y = 0; y < mapSize; y += this.chunkSize)
            {
                for (int x = 0; x < mapSize; x += this.chunkSize)
                {
                    Chunk chunk = new Chunk();
                    chunk.ChunkPosition = new Vector2(x, y);
                    this.chunks.Add(chunk);
                }
            }
            this.currentChunks.Add(null);
        }
        
        private void UpdateChunk()
        {
            if (this.GetChunk(this.player.position) == this.currentChunks[0]) return;
            List<Chunk> oldChunks = new List<Chunk>(this.currentChunks);
            this.currentChunks = this.GetChunksToGenerate(this.GetChunk(this.player.position), oldChunks);

            IEnumerable<Chunk> chunksToClear = oldChunks.Except(this.currentChunks);
            IEnumerable<Chunk> chunksToCreate = this.currentChunks.Except(oldChunks);
            this.ClearChunks(chunksToClear);
            this.CreateChunks(chunksToCreate);
        }

        private void ClearChunks(IEnumerable<Chunk> chunksToClear)
        {
            foreach (Chunk chunk in chunksToClear)
            {
                if (chunk == null) continue;
                chunk.SpawnedStructures.ForEach(t => this.DestroyStructure(t));
                chunk.SpawnedStructures.Clear();
            }
        }

        private void CreateChunks(IEnumerable<Chunk> chunksToCreate)
        {
            foreach (Chunk chunk in chunksToCreate)
            {
                foreach (ResourceInfo currentResourceToDraw in chunk.ChunkResources)
                {
                    this.ActiveWorldStructure(currentResourceToDraw, chunk);
                }
            }
        }

        public bool IsStructureOnPosition(ResourceInfo currentResourceToDraw, bool includePrimaryStructure = false)
        {
            IEnumerable<Chunk> chunksToCheck = this.GetChunksToGenerate(this.GetChunk(currentResourceToDraw.position));
            return this.IsStructureOnPosition(currentResourceToDraw, chunksToCheck, includePrimaryStructure);
        }
        
        private void ActiveWorldStructure(ResourceInfo currentResourceToDraw, Chunk chunk)
        {
            string structureName = currentResourceToDraw.worldStructure.name;
            if (!this.worldStructurePools.ContainsKey(structureName))
            {
                this.worldStructurePools.Add(structureName, new ObjectPool<WorldStructure>());
                this.worldStructurePools[structureName].Initialize(() => this.SpawnObject(currentResourceToDraw), this.poolsStartAmount);
            }
            WorldStructure spawnedResource = this.worldStructurePools[structureName].GetObject();
            spawnedResource.transform.position = currentResourceToDraw.position;
            Physics2D.SyncTransforms();
            chunk.SpawnedStructures.Add(spawnedResource);
        }

        public void CheckStructures()
        {
            bool isStructures = true;
            do
            {
                isStructures = this.RemoveStructureOnSamePosition().Count() > 0;
            } while (isStructures);
        }

        private IEnumerable<ResourceInfo> RemoveStructureOnSamePosition()
        {
            double randomValue = RandomGenerator.NextDoubleBySeed();
            List<ResourceInfo> structures = new List<ResourceInfo>();
            foreach (var chunk in this.chunks)
            {
                IEnumerable<Chunk> chunksToCheck = this.GetChunksToGenerate(chunk);
                ConcurrentStack<ResourceInfo> resources = new ConcurrentStack<ResourceInfo>();
                foreach (var resource in chunk.ChunkResources.OrderBy(t => randomValue))
                {
                    bool isStructure = this.IsStructureOnPosition(resource, chunksToCheck);
                    if (!isStructure) resources.Push(resource);
                    else structures.Add(resource);
                }
                chunk.ChunkResources = resources;
            }
            return structures;
        }

        private bool IsStructureOnPosition(ResourceInfo currentResourceToDraw, IEnumerable<Chunk> chunksToCheck, bool includePrimaryStructure = false)
        {
            foreach (var chunk in chunksToCheck)
            {
                if (chunk.IsStructureOnPosition(currentResourceToDraw, includePrimaryStructure)) return true;
            }
            return false;
        }

        private WorldStructure SpawnObject(ResourceInfo currentResourceToDraw)
        {
            WorldStructure spawnedResource = Instantiate(currentResourceToDraw.worldStructure, currentResourceToDraw.position, Quaternion.identity, this.structuresParent);
            spawnedResource.name = currentResourceToDraw.worldStructure.name;
            spawnedResource.gameObject.SetActive(false);
            return spawnedResource;
        }
        
        private List<Chunk> GetChunksToGenerate(Chunk baseChunk, List<Chunk> oldChunks)
        {
            List<Chunk> newChunks = new List<Chunk>();
            List<Vector2> vectorsAround = new List<Vector2>()
            {
                new Vector2(this.chunkSize, 0), new Vector2(this.chunkSize, this.chunkSize), new Vector2(0, this.chunkSize), new Vector2(-this.chunkSize, this.chunkSize),
                new Vector2(-this.chunkSize, 0), new Vector2(-this.chunkSize, -this.chunkSize), new Vector2(0, -this.chunkSize), new Vector2(this.chunkSize, -this.chunkSize)
            };
            newChunks.Add(baseChunk);
            foreach (var vector in vectorsAround)
            {
                Vector2 position = baseChunk.ChunkPosition + vector;
                Chunk chunk = oldChunks.Where(t => t?.ChunkPosition == position).FirstOrDefault();
                if (chunk == null) chunk = this.GetChunk(position);
                if (chunk == null) continue;
                newChunks.Add(chunk);
            }
            return newChunks;
        }

        private List<Chunk> GetChunksToGenerate(Chunk baseChunk)
        {
            return this.GetChunksToGenerate(baseChunk, new List<Chunk>());
        }

        public Chunk GetChunk(Vector2 position) => this.chunks.Where(t => position.x < t.ChunkPosition.x + this.chunkSize && position.x >= t.ChunkPosition.x &&
            position.y < t.ChunkPosition.y + this.chunkSize && position.y >= t.ChunkPosition.y).FirstOrDefault();
        
        private void DestroyStructure(WorldStructure worldStructure)
        {
            this.worldStructurePools[worldStructure.name].ReturnObject(worldStructure);
        }
    }

}