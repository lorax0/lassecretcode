﻿namespace LasSecretRPG.MapGeneration
{
	using UnityEngine;
	using UnityEngine.Tilemaps;

	[System.Serializable]
	public struct TerrainType
	{
		public string name;
		public float height;
		public Color colour;
		public TileBase tile;
		public bool isLand;
	}

}