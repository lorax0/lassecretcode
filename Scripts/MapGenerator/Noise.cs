﻿namespace LasSecretRPG.MapGeneration
{
	using UnityEngine;
	using System.Collections;

	public static class Noise
	{
		public static float[,] GenerateNoiseMap(int mapSize, int seed, NoiseValues noiseValues, bool useFalloff)
		{
			float[,] falloffMap = new float[mapSize, mapSize];
			float[,] noiseMap = new float[mapSize, mapSize];

			if (useFalloff)
			{
				falloffMap = FalloffGenerator.GenerateFalloffMap(mapSize);
			}

			System.Random prng = new System.Random(seed);
			Vector2[] octaveOffsets = new Vector2[noiseValues.octaves];
			for (int i = 0; i < noiseValues.octaves; i++)
			{
				float offsetX = prng.Next(-100000, 100000) + noiseValues.offset.x;
				float offsetY = prng.Next(-100000, 100000) + noiseValues.offset.y;
				octaveOffsets[i] = new Vector2(offsetX, offsetY);
			}

			if (noiseValues.scale <= 0)
			{
				noiseValues.scale = 0.0001f;
			}

			float maxNoiseHeight = float.MinValue;
			float minNoiseHeight = float.MaxValue;

			float halfWidth = mapSize / 2f;
			float halfHeight = mapSize / 2f;


			for (int y = 0; y < mapSize; y++)
			{
				for (int x = 0; x < mapSize; x++)
				{

					float amplitude = 1;
					float frequency = 1;
					float noiseHeight = 0;

					for (int i = 0; i < noiseValues.octaves; i++)
					{
						float sampleX = (x - halfWidth) / noiseValues.scale * frequency + octaveOffsets[i].x;
						float sampleY = (y - halfHeight) / noiseValues.scale * frequency + octaveOffsets[i].y;

						float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
						noiseHeight += perlinValue * amplitude;

						amplitude *= noiseValues.persistance;
						frequency *= noiseValues.lacunarity;
					}

					if (noiseHeight > maxNoiseHeight)
					{
						maxNoiseHeight = noiseHeight;
					}
					else if (noiseHeight < minNoiseHeight)
					{
						minNoiseHeight = noiseHeight;
					}
					noiseMap[x, y] = noiseHeight;
				}
			}

			for (int y = 0; y < mapSize; y++)
			{
				for (int x = 0; x < mapSize; x++)
				{
					noiseMap[x, y] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noiseMap[x, y]);
					if (useFalloff)
					{
						noiseMap[x, y] = Mathf.Clamp01(noiseMap[x, y] - falloffMap[x, y]);
					}
				}
			}

			return noiseMap;
		}

		public static float[,] GenerateBiomeMap(int mapSize, int seed, BiomeNoiseValues biomeNoiseValues)
		{
			FastNoiseLite noise = new FastNoiseLite(seed);

			//general config
			noise.SetNoiseType(biomeNoiseValues.generalNoiseType);
			noise.SetFrequency(biomeNoiseValues.generalFrequency);

			//fractal config
			noise.SetFractalType(biomeNoiseValues.fractalType);
			noise.SetFractalOctaves(biomeNoiseValues.fractalOctaves);
			noise.SetFractalLacunarity(biomeNoiseValues.fractalLacunarity);
			noise.SetFractalGain(biomeNoiseValues.fractalGain);
			noise.SetFractalWeightedStrength(biomeNoiseValues.fractalWeightedStrength);
			noise.SetFractalPingPongStrength(biomeNoiseValues.fractalPingPongStrengh);

			//celluar config
			noise.SetCellularDistanceFunction(biomeNoiseValues.cellularDistanceFunction);
			noise.SetCellularReturnType(biomeNoiseValues.cellularReturnType);
			noise.SetCellularJitter(biomeNoiseValues.celluarJitter);


			//domain warp config
			FastNoiseLite warpNoise = new FastNoiseLite(seed);
			warpNoise.SetDomainWarpType(biomeNoiseValues.domainWarpType);
			warpNoise.SetDomainWarpAmp(biomeNoiseValues.domainWarpAmplitude);
			warpNoise.SetFrequency(biomeNoiseValues.domainWarpFrequency);

			//domain warp fractal config
			warpNoise.SetFractalType(biomeNoiseValues.domainWarpFractalType);
			warpNoise.SetFractalOctaves(biomeNoiseValues.domainWarpFractalOctaves);
			warpNoise.SetFractalLacunarity(biomeNoiseValues.domainWarpFractalLacunarity);
			warpNoise.SetFractalGain(biomeNoiseValues.domainWarpFractalGain);

			float[,] biomeMap = new float[mapSize, mapSize];

			for (int y = 0; y < mapSize; y++)
			{
				for (int x = 0; x < mapSize; x++)
				{
					float xf = x;
					float yf = y;
					warpNoise.DomainWarp(ref xf, ref yf);

					biomeMap[x, y] = noise.GetNoise(xf, yf) * 0.5f + 0.5f; //Multiplying by 0.5f and adding 0.5f fixed the problem :)
				}
			}

			return biomeMap;
		}

	}

}