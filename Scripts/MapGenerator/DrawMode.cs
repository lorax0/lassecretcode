namespace LasSecretRPG.MapGeneration
{
    public enum DrawMode
    {
        NoiseMap,
        NoiseMapWithBiomes,
        ColourMap,
        StructuresMap,
        PlayMode,
        FalloffMap
    };

}