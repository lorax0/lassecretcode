﻿namespace LasSecretRPG.MapGeneration
{
	using UnityEngine;

	[System.Serializable]
	public struct BiomeNoiseValues
	{
		//general config
		[Header("General")]
		public FastNoiseLite.NoiseType generalNoiseType;
		public float generalFrequency;

		//fractal config
		[Header("Fractal")]
		public FastNoiseLite.FractalType fractalType;
		public int fractalOctaves;
		public float fractalLacunarity;
		public float fractalGain;
		public float fractalWeightedStrength;
		public float fractalPingPongStrengh;

		//celluar config
		[Header("Celluar")]
		public FastNoiseLite.CellularDistanceFunction cellularDistanceFunction;
		public FastNoiseLite.CellularReturnType cellularReturnType;
		public float celluarJitter;

		//domain warp config
		[Header("Domain warp")]
		public FastNoiseLite.DomainWarpType domainWarpType;
		public float domainWarpAmplitude;
		public float domainWarpFrequency;

		//domain warp fractal config
		[Header("Domain warp fractal")]
		public FastNoiseLite.FractalType domainWarpFractalType;
		public int domainWarpFractalOctaves;
		public float domainWarpFractalLacunarity;
		public float domainWarpFractalGain;
	}

}