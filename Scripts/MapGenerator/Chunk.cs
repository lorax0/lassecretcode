﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LasSecretRPG.MapGeneration
{
    public class Chunk
    {
        public Vector2 ChunkPosition { get => this.chunkPosition; set => this.chunkPosition = value; }
        public ConcurrentStack<ResourceInfo> ChunkResources { get => this.chunkResources; set => this.chunkResources = value; }
        public List<WorldStructure> SpawnedStructures => this.spawnedStructures;

        private Vector2 chunkPosition;
        private ConcurrentStack<ResourceInfo> chunkResources = new ConcurrentStack<ResourceInfo>();
        private List<WorldStructure> spawnedStructures = new List<WorldStructure>();

        public bool IsStructureOnPosition(ResourceInfo currentResource, bool includePrimaryStructure = false)
        {
            if (this.chunkResources.Count == 0) return false;
            if (currentResource.isPrimary && !includePrimaryStructure) return false;
            foreach (var resource in this.chunkResources)
            {
                if (resource == currentResource) return false;
                Vector2 topLeftOfResource = new Vector2(resource.position.x - resource.worldStructure.colliderSize.x / 2, resource.position.y + resource.worldStructure.colliderSize.y / 2);
                Vector2 bottomRightOfResource = new Vector2(resource.position.x + resource.worldStructure.colliderSize.x / 2, resource.position.y - resource.worldStructure.colliderSize.y / 2);
                Vector2 topRight = new Vector2(currentResource.position.x + currentResource.worldStructure.colliderSize.x / 2, currentResource.position.y + currentResource.worldStructure.colliderSize.y / 2);
                Vector2 topLeft = new Vector2(currentResource.position.x - currentResource.worldStructure.colliderSize.x / 2, currentResource.position.y + currentResource.worldStructure.colliderSize.y / 2);
                Vector2 bottomRight = new Vector2(currentResource.position.x + currentResource.worldStructure.colliderSize.x / 2, currentResource.position.y - currentResource.worldStructure.colliderSize.y / 2);
                Vector2 bottomLeft = new Vector2(currentResource.position.x - currentResource.worldStructure.colliderSize.x / 2, currentResource.position.y - currentResource.worldStructure.colliderSize.y / 2);
                if (this.IsPointInBox(topRight, topLeftOfResource, bottomRightOfResource)) return true;
                if (this.IsPointInBox(topLeft, topLeftOfResource, bottomRightOfResource)) return true;
                if (this.IsPointInBox(bottomRight, topLeftOfResource, bottomRightOfResource)) return true;
                if (this.IsPointInBox(bottomLeft, topLeftOfResource, bottomRightOfResource)) return true;
            }
            return false;
        }

        private bool IsPointInBox(Vector2 point, Vector2 topLeftBoxPoint, Vector2 bottomRightBoxPoint) =>
            point.x >= topLeftBoxPoint.x && point.x <= bottomRightBoxPoint.x && point.y <= topLeftBoxPoint.y && point.y >= bottomRightBoxPoint.y;
    }
}
