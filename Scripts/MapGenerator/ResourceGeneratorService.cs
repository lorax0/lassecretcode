using LasSecretRPG.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LasSecretRPG.MapGeneration
{
    [System.Serializable]
    public class ResourceGeneratorService : IResourceGenerator
    {
        public List<WeightedSpawn> StructurePrefabs => this.structurePrefabs;

        public NoiseValues TreeNoiseValues => this.treeNoiseValues;
        public float TreeMinHeight => this.treeMinHeight;
        public float TreeSpawnThreshold => this.treeSpawnThreshold;
        public AnimationCurve TreeCurve => this.treeCurve;

        [SerializeField] private List<WeightedSpawn> structurePrefabs;
        [SerializeField] private NoiseValues treeNoiseValues;
        [SerializeField] private AnimationCurve treeCurve;
        [Range(0, 1)] [SerializeField] private float treeSpawnThreshold;
        [Range(0, 1)] [SerializeField] private float treeMinHeight;
        [Range(0, 1)] [SerializeField] private float treeMaxHeight;
        
        private float totalWeight;

        public void Generate(Color[] biomeColourMap, Biome treeBiome, float worldScale, int mapSize, bool useFalloff)
        {
            if (this.structurePrefabs.Count == 0)
                return;

            this.totalWeight = this.CalculateWeight();
            
            float[,] treeMap = Noise.GenerateNoiseMap(mapSize, RandomGenerator.RangeBySeed(0, 100), this.treeNoiseValues, useFalloff);

            for (int y = 0; y < mapSize; y++)
            {
                for (int x = 0; x < mapSize; x++)
                {
                    if (treeBiome.colour != biomeColourMap[y * mapSize + x])
                    {
                        continue;
                    }

                    if (treeMap[x, y] < this.treeMinHeight)
                    {
                        continue;
                    }

                    float currentTree = treeMap[x, y];
                    if (currentTree < this.treeSpawnThreshold)
                    {
                        continue;
                    }

                    currentTree = (currentTree - this.treeSpawnThreshold) / (1f - this.treeSpawnThreshold);

                    float randomFloat = (float)RandomGenerator.NextDoubleBySeed();
                    float num = this.treeCurve.Evaluate(randomFloat);
                    if (!(num > 1f - currentTree))
                    {
                        continue;
                    }

                    float xOffset = RandomGenerator.RangeBySeed(0, MapGenerator.Instance.MaxTreeOffset) + (float)RandomGenerator.NextDoubleBySeed();
                    float yOffset = RandomGenerator.RangeBySeed(0, MapGenerator.Instance.MaxTreeOffset) + (float)RandomGenerator.NextDoubleBySeed();


                    Vector2 position = new Vector2(x + xOffset, y + yOffset) * worldScale;
                    ResourceInfo resourceInfo = new ResourceInfo(this.GetObjectToSpawn(), position, false);
                    
                    DrawChunkResources.Instance.GetChunk(resourceInfo.position).ChunkResources.Push(resourceInfo);
                }
            }
        }

        public WorldStructure GetObjectToSpawn()
        {
            float randomNumber = (float)RandomGenerator.NextDoubleBySeed();
            float currentWeight = 0f;
            for (int i = 0; i < this.structurePrefabs.Count; i++)
            {
                currentWeight += this.structurePrefabs[i].weight;
                if (randomNumber < currentWeight / this.totalWeight)
                {
                    return this.structurePrefabs[i].worldStructure;
                }
            }
            return this.structurePrefabs[0].worldStructure;
        }

        public float CalculateWeight() => this.structurePrefabs.Sum(t => t.weight);
    }

}
