using LasSecretRPG.MapGeneration;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Attach this script to every generated GameObject from generator like trees, rocks etc.
/// </summary>
namespace LasSecretRPG.MapGeneration
{
    public class WorldStructure : MonoBehaviour
    {
        public Vector2 colliderSize;
        private Transform player;

        private void Awake()
        {
            //TODO: change searching by tag
            this.player = GameObject.FindGameObjectWithTag("Player").transform;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(this.transform.position, this.colliderSize);
        }
    }
}