﻿using LasSecretRPG.Bestiary;
using LasSecretRPG.Damageable;
using LasSecretRPG.Statistics;
using LasSecretRPG.Statistics.Buffs;
using LasSecretRPG.Utilities;
using UnityEngine;
using Zenject;

namespace LasSecretRPG.Enemies
{
    public abstract class Enemy : DamageableUnit, IBuffable
    {
        public float AttackRadius => this.attackRadius;
        public float ChaseRadius => this.chaseRadius;
        public float ChaseMinimalRadius => this.chaseMinimalRadius;
        public StatisticData MovementSpeed => this.movementSpeed;
        public bool IsRangeEnemy => (this as RangeEnemy) != null;
        public BuffsService BuffsService => this.buffsService;
        public AnimationClip IdleAnimation => this.idleAnimation;
        public AnimationClip WalkingAnimation => this.walkingAnimation;
        public AnimationClip AttackAnimation => this.attackAnimation;
        public AnimationClip HitAnimation => this.hitAnimation;

        [SerializeField] protected float attackRadius;
        [SerializeField] protected float chaseMinimalRadius;
        [SerializeField] protected float chaseRadius;
        [SerializeField] protected StatisticData movementSpeed;
        [SerializeField] protected int minDamage;
        [SerializeField] protected int maxDamage;
        [SerializeField] protected BuffsService buffsService; 

        [SerializeField] protected AnimationClip idleAnimation;
        [SerializeField] protected AnimationClip walkingAnimation;
        [SerializeField] protected AnimationClip attackAnimation;
        [SerializeField] protected AnimationClip hitAnimation;
        [SerializeField] private Animator animator;

        [Inject] protected BestiaryController bestiaryController;
        protected AnimationClip currentAnimation;
        protected State currentState;
        protected float counter;

        public override void Start()
        {
            base.Start();
            this.ChangeState(new SleepState(), this.idleAnimation);
            this.buffsService.Buffable = this;
            this.movementSpeed.InitializeStatistic(this);
        }

        public virtual void Update()
        {
            this.currentState.DoStateAction();
            this.UpdateCounter();
        }

        private void UpdateCounter()
        {
            if(this.counter > 0f)
            {
                this.counter -= Time.deltaTime;
            }
        }

        public bool CanAttack() => this.counter <= 0f;

        public void SetTimer(float time) => this.counter = time;

        public void ChangeState(State state, AnimationClip animation)
        {
            state.Target = this.currentState?.Target;
            state.Enemy = this.currentState?.Enemy == null ? this : this.currentState.Enemy;
            this.ChangeAnimationState(animation);
            this.currentState = state;
        }

        public int GetRandomDamage() => RandomGenerator.RandomRange(this.minDamage, this.maxDamage);

        private void ChangeAnimationState(AnimationClip newAnimationState)
        {
            if (this.currentAnimation == newAnimationState)
                return;

            this.animator.Play(newAnimationState.name);
            this.currentAnimation = newAnimationState;
        }
    }
}
