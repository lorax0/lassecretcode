﻿using LasSecretRPG.Items;
using LasSecretRPG.UnityAttributes;
using LasSecretRPG.MapGeneration;
using UnityEngine;
using LasSecretRPG.Bestiary;
using LasSecretRPG.Damageable.Dropping;
using System.Collections.Generic;

namespace LasSecretRPG.Enemies
{
    public class RangeEnemy : Enemy, IBestiarable
    {
        public Projectile Bullet => this.bullet;
        public Transform FirePoint => this.firePoint;
        public float ShootCooldown => this.shootCooldown;
        public string Name => this.name;
        public BestiaryValue Biome => this.biome;
        public BestiaryValue BestiaryImage => this.bestiaryImage;

        [FoldoutGroup("Range enemy data", true)]
        [SerializeField] private Transform firePoint;
        [SerializeField] private float shootCooldown;
        [SerializeField] private Projectile bullet;

        [FoldoutGroup("Bestiary Data", true)]
        [SerializeField] private BestiaryValue biome;
        [SerializeField] private BestiaryValue bestiaryImage;

        public override void Start()
        {
            base.Start();
            this.healthService.OnDieEvent += (IEnumerable<PercentageDropBonus> percentageDropBonuses) => this.bestiaryController.IncrementKilledEnemies(this);
        }

        public override void Update()
        {
            base.Update();
            var target = this.currentState.Target;
            if (target == null) return;
            Vector2 lookDir = (Vector2)target.HealthService.Transform.position - (Vector2)this.transform.position;
            float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;
            this.firePoint.rotation = Quaternion.Euler(0, 0, angle);
        }
    }
}