﻿using LasSecretRPG.Bestiary;
using LasSecretRPG.Damageable;
using LasSecretRPG.Damageable.Dropping;
using LasSecretRPG.Damageable.Knockback;
using LasSecretRPG.MapGeneration;
using LasSecretRPG.Player;
using LasSecretRPG.UnityAttributes;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Enemies
{
    public class MeleeEnemy : Enemy, IBestiarable
    {
        public float AtackCooldown => this.atackCooldown;
        public string Name => this.name;
        public BestiaryValue Biome => this.biome;
        public BestiaryValue BestiaryImage => this.bestiaryImage;

        [FoldoutGroup("Melee enemy data", true)]
        [SerializeField] private Transform firePoint;
        [SerializeField] private float atackCooldown;
        [SerializeField] private PowerKnockbackTier powerKnockbackTier;

        [FoldoutGroup("Bestiary Data", true)]
        [SerializeField] private BestiaryValue biome;
        [SerializeField] private BestiaryValue bestiaryImage;

        public override void Start()
        {
            base.Start();
            this.healthService.OnDieEvent += (IEnumerable<PercentageDropBonus> percentageDropBonuses) => this.bestiaryController.IncrementKilledEnemies(this);
        }

        public override void Update()
        {
            base.Update();
            var target = this.currentState.Target;
            if (target == null) return;
            Vector2 lookDir = (Vector2)target.HealthService.Transform.position - (Vector2)this.transform.position;
            float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg;
            this.firePoint.rotation = Quaternion.Euler(0, 0, angle);
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            var damageable = collision.GetComponent<PlayerController>();
            if (damageable == this.currentState.Target)
            {
                if (!this.CanAttack()) return;
                damageable.HealthService.TakeDamage(this.GetRandomDamage(), this.transform.position, new List<PercentageDropBonus>(), this.powerKnockbackTier);
                this.SetTimer(this.atackCooldown);
            }
        }
    }
}