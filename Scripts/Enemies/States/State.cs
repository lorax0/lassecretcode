﻿using LasSecretRPG.Damageable;
using LasSecretRPG.Player;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Enemies
{
    public abstract class State
    {
        public Enemy Enemy { get => this.enemy; set => this.enemy = value; }
        public PlayerController Target { get => this.target; set => this.target = value; }

        protected Enemy enemy;
        protected PlayerController target;

        public abstract void DoStateAction();

        public PlayerController GetTargetInRange(Enemy enemy, float radius)
        {
            Collider2D[] collisions = Physics2D.OverlapCircleAll(enemy.transform.position, radius);

            foreach (Collider2D collision in collisions)
            {
                var target = collision.GetComponent<PlayerController>();
                if (target != null) return target;
            }
            return null;
        }

        public bool IsTargetOnRange(float range) => Vector2.Distance(this.enemy.transform.position, this.target.HealthService.Transform.position) <= range;

    }
}
