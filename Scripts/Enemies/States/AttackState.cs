﻿using LasSecretRPG.Damageable;
using LasSecretRPG.Damageable.Dropping;
using LasSecretRPG.Items;
using LasSecretRPG.Player;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Enemies
{
    public class AttackState : State
    {
        public AttackState(PlayerController target)
        {
            this.target = target;
            this.target.HealthService.OnDieEvent += this.ChangeForSleepState;
        }

        public override void DoStateAction()
        {
            if (!this.IsTargetOnRange(this.enemy.AttackRadius)) this.ChangeForChaseState();
            if (!this.enemy.CanAttack()) return;
            if (this.enemy.IsRangeEnemy) this.Shoot();
        }

        private void ChangeForChaseState()
        {
            this.enemy.ChangeState(new ChaseState(), this.enemy.WalkingAnimation);
            this.target.HealthService.OnDieEvent -= this.ChangeForSleepState;
        }

        private void ChangeForSleepState(IEnumerable<PercentageDropBonus> percentageDropBonuses)
        {
            this.enemy.ChangeState(new SleepState(), this.enemy.WalkingAnimation);
            this.target.HealthService.OnDieEvent -= this.ChangeForSleepState;
        }
        
        private void Shoot()
        {
            var rangeEnemy = this.enemy as RangeEnemy;
            var projectile = MonoBehaviour.Instantiate(rangeEnemy.Bullet, rangeEnemy.FirePoint.position, rangeEnemy.FirePoint.rotation);
            projectile.Damage = this.enemy.GetRandomDamage();
            projectile.Owner = this.enemy.gameObject;
            projectile.OnHit += () => MonoBehaviour.Destroy(projectile.gameObject);
            projectile.OnCounterEnd += () => MonoBehaviour.Destroy(projectile.gameObject);
            this.enemy.SetTimer(rangeEnemy.ShootCooldown);
        }
    }
}