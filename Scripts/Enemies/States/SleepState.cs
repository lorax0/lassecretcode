﻿using LasSecretRPG.Damageable;
using System;

namespace LasSecretRPG.Enemies
{
    public class SleepState : State
    {
        public override void DoStateAction()
        {
            this.target = this.GetTargetInRange(this.enemy, this.enemy.ChaseRadius);
            if (this.target != null) this.ChangeForChaseState();
        }

        private void ChangeForChaseState()
        {
            this.enemy.ChangeState(new ChaseState(), this.enemy.WalkingAnimation);
        }
    }
}