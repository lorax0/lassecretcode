﻿using System;
using LasSecretRPG.Damageable;
using LasSecretRPG.Tiles;
using UnityEngine;

namespace LasSecretRPG.Enemies
{
    public class ChaseState : State
    {
        private TileManager tileManager;

        public ChaseState()
        {
            this.tileManager = TileManager.Instance;
        }

        public override void DoStateAction()
        {
            this.MoveEnemy();
            var target = this.GetTargetInRange(this.enemy, this.enemy.ChaseMinimalRadius);
            if (target != null)
            {
                this.target = target;
                this.ChangeForAttackState();
            }
            else if (!this.IsTargetOnRange(this.enemy.ChaseRadius)) this.ChangeForSleepState();
        }

        private void ChangeForAttackState()
        {
            this.enemy.ChangeState(new AttackState(this.target), this.enemy.AttackAnimation);
        }

        private void ChangeForSleepState()
        {
            this.enemy.ChangeState(new SleepState(), this.enemy.IdleAnimation);
        }

        private void MoveEnemy()
        {
            if (this.target.HealthService.Transform.position.x > this.enemy.transform.position.x)
            {
                this.enemy.transform.eulerAngles = new Vector3(0f, 180f, 0f);
            }
            else
            {
                this.enemy.transform.eulerAngles = new Vector3(0f, 0f, 0f);
            }
            this.enemy.transform.position = Vector2.MoveTowards(this.enemy.transform.position, this.target.HealthService.Transform.position, 
                this.enemy.MovementSpeed.Statistic.Values[this.enemy] * Time.deltaTime * this.tileManager.GetSpeedOnTile(this.enemy.transform.position));

        }
    }
}