﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace LasSecretRPG.UnityAttributes
{
    public class FoldoutGroup : PropertyAttribute
    {
        public string name;
        public bool foldEverything;

        public FoldoutGroup(string name, bool foldEverything = false)
        {
            this.foldEverything = foldEverything;
            this.name = name;
        }
    }
}
