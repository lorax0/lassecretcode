﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Translation
{
    public class TranslationService : MonoBehaviour
    {
        public static TranslationService Instance => instance;
        protected static TranslationService instance;

        [SerializeField] private TranslationLanguage defaultLanguage;
        [SerializeField] private TranslationLanguage language;

        private void Awake()
        {
            this.SetSingleton();
        }

        private void SetSingleton()
        {
            if (instance == null) instance = this;
            else Destroy(this.gameObject);
        }

        public string GetTranslatedText(string text)
        {
            int index = this.defaultLanguage.Fields.Find(t => t.TranslationText == text).Index;
            return this.language.Fields.Find(t => t.Index == index).TranslationText;
        }
    }
}
