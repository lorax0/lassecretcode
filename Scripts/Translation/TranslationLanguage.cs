using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Translation
{
    public class TranslationLanguage : ScriptableObject
    {
        public List<TranslationField> Fields => this.fields;

        private List<TranslationField> fields = new List<TranslationField>();
    }
}