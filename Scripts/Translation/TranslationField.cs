﻿using System;

namespace LasSecretRPG.Translation
{
    [Serializable]
    public class TranslationField
    {
        public int Index;
        public string TranslationText;

        public TranslationField(int languageIndex)
        {
            this.Index = languageIndex;
        }
    }
}