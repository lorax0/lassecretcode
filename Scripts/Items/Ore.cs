using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace LasSecretRPG.Items
{
    [CreateAssetMenu(fileName = "New Ore", menuName = "Items/Ore")]
    public class Ore : Item
    {
        [SerializeField] private float meltingSpeed;
    }
}
