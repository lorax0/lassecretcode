using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace LasSecretRPG.Items
{
    [CreateAssetMenu(fileName = "New Fuel", menuName = "Items/Fuel")]
    public class Fuel : Item
    {
        [SerializeField] private float burnSpeed;
    }
}
