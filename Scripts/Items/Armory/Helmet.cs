﻿using System;
using UnityEngine;

namespace LasSecretRPG.Items.Armory
{
    [CreateAssetMenu(fileName = "New Helmet", menuName = "Items/Armor/Helmet")]
    public class Helmet : Item, IArmor
    {
        public int DefencePoints => this.defencePoints;
        public Sprite ArmorSprite => this.armorSprite;

        [SerializeField] protected int defencePoints;
        [SerializeField] protected Sprite armorSprite;
    }
}
