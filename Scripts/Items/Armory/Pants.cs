﻿using System;
using UnityEngine;

namespace LasSecretRPG.Items.Armory
{
    [CreateAssetMenu(fileName = "New Pants", menuName = "Items/Armor/Pants")]
    public class Pants : Item, IArmor
    {
        public int DefencePoints => this.defencePoints;
        public Sprite ArmorSprite => this.armorSprite;

        [SerializeField] protected int defencePoints;
        [SerializeField] protected Sprite armorSprite;
    }
}
