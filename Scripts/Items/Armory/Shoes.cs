﻿using System;
using UnityEngine;

namespace LasSecretRPG.Items.Armory
{
    [CreateAssetMenu(fileName = "New Shoes", menuName = "Items/Armor/Shoes")]
    public class Shoes : Item, IArmor
    {
        public int DefencePoints => this.defencePoints;
        public Sprite ArmorSprite => this.armorSprite;

        [SerializeField] protected int defencePoints;
        [SerializeField] protected Sprite armorSprite;
    }
}
