﻿using System;
using UnityEngine;

namespace LasSecretRPG.Items.Armory
{
    [CreateAssetMenu(fileName = "New Necklace", menuName = "Items/Armor/Necklace")]
    public class Necklace : Item, IArmor
    {
        public int DefencePoints => this.defencePoints;
        public Sprite ArmorSprite => this.armorSprite;

        [SerializeField] protected int defencePoints;
        [SerializeField] protected Sprite armorSprite;
    }
}
