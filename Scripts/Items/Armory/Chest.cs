﻿using System;
using UnityEngine;

namespace LasSecretRPG.Items.Armory
{
    [CreateAssetMenu(fileName = "New Chest", menuName = "Items/Armor/Chest")]
    public class Chest : Item, IArmor
    {
        public int DefencePoints => this.defencePoints;
        public Sprite ArmorSprite => this.armorSprite;

        [SerializeField] protected int defencePoints;
        [SerializeField] protected Sprite armorSprite;
    }
}
