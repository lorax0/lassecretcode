﻿using UnityEngine;

namespace LasSecretRPG.Items.Armory
{
    public interface IArmor : IDefencePoints
    {
        Sprite ArmorSprite { get; }
    }
}