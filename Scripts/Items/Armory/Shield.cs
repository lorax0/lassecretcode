using LasSecretRPG.Damageable.Knockback;
using LasSecretRPG.Statistics.Buffs;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Items.Armory
{
    [CreateAssetMenu(fileName = "New Shield", menuName = "Items/Weapon/Shield")]
    public class Shield : Item, IDuratibility, IDefencePoints
    {
        public int MaxDurability => this.maxDurability;
        public Sprite ShieldSprite => this.shieldSprite;
        public int DefencePoints => this.defencePoints;
        public float DamageBlock => this.damageBlock;
        public float MaxPercentageDamageBlock => this.maxPercentageDamageBlock / 100f;
        public float StaminaUsing => this.staminaUsing;
        public IEnumerable<Buff> WearBuffs => this.wearBuffs;
        public IEnumerable<Buff> BlockingBuffs => this.blockingBuffs;
        public IEnumerable<Buff> HandBuffs => this.handBuffs;
        public ResistanceKnockbackTier ResistanceKnockbackTier => this.resistanceKnockbackTier;

        [SerializeField] private int maxDurability;
        [SerializeField] private Sprite shieldSprite;
        [SerializeField] private int defencePoints;
        [SerializeField] private float damageBlock;
        [SerializeField] private float maxPercentageDamageBlock;
        [SerializeField] private float staminaUsing;
        [SerializeField] private List<Buff> wearBuffs;
        [SerializeField] private List<Buff> handBuffs;
        [SerializeField] private List<Buff> blockingBuffs;
        [SerializeField] private ResistanceKnockbackTier resistanceKnockbackTier;
    }

}