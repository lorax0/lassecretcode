﻿namespace LasSecretRPG.Items.Armory
{
    public interface IDefencePoints
    {
        int DefencePoints { get; }
    }
}