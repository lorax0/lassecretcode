using LasSecretRPG.Damageable;
using LasSecretRPG.Damageable.Dropping;
using LasSecretRPG.Damageable.Knockback;
using LasSecretRPG.Player;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Items
{
    public class Projectile : MonoBehaviour
    {
        public Action OnHit;
        public Action OnCounterEnd;

        public int Damage { get => this.damage; set => this.damage = value; }
        public GameObject Owner { get => this.owner; set => this.owner = value; }
        public IEnumerable<PercentageDropBonus> PercentageDropBonuses { get => this.percentageDropBonuses; set => this.percentageDropBonuses = value; }

        [SerializeField] private float moveSpeed;
        [SerializeField] private float flyingTime;
        [SerializeField] private PowerKnockbackTier powerKnockbackTier;

        private IEnumerable<PercentageDropBonus> percentageDropBonuses;
        private int damage;
        private GameObject owner;
        private float counter;

        private void Start()
        {
            this.SetCounter();
        }

        private void SetCounter()
        {
            if (this.flyingTime != 0) this.counter = this.flyingTime;
            else this.counter = Mathf.Infinity;
        }

        protected void Update()
        {
            this.Move();
            this.UpdateCounter();
        }

        private void UpdateCounter()
        {
            if (this.counter > 0f) this.counter -= Time.deltaTime;
            else this.OnCounterEnd?.Invoke();
        }

        private void Move()
        {
            this.transform.position += this.transform.right * this.moveSpeed * Time.deltaTime;
        }

        protected void OnTriggerEnter2D(Collider2D collider)
        {
            if (collider.gameObject == this.owner) return;

            if (collider.gameObject.TryGetComponent(out DamageableUnit damageable))
            {
                damageable.HealthService.TakeDamage(this.damage, this.transform.position, this.percentageDropBonuses, this.powerKnockbackTier);
            }
            this.OnHit?.Invoke();
        }

    }
}