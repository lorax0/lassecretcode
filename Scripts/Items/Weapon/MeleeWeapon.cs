using System.Linq;
using UnityEngine;
using LasSecretRPG.Player;
using LasSecretRPG.Utilities;
using LasSecretRPG.Inputs;
using System.Collections.Generic;
using LasSecretRPG.Damageable;
using LasSecretRPG.Damageable.Knockback;
using LasSecretRPG.Damageable.Dropping;
using LasSecretRPG.Items.Modifiers;

namespace LasSecretRPG.Items.Weapon
{
    [CreateAssetMenu(fileName = "New Melee Weapon", menuName = "Items/Weapon/MeleeWeapon")]
    public class MeleeWeapon : Item, IMeleeWeapon, IToolVisible
    {
        public int MaxDamage => this.maxDamage;
        public int MinDamage => this.minDamage;
        public Sprite HandSprite => this.handSprite;
        public int MaxDurability => this.maxDurability;
        public DamageType DamageType => this.damageType;
        public bool CanUseShield => this.canUseShield;

        [SerializeField] private int minDamage;
        [SerializeField] private int maxDamage;
        [SerializeField] private int maxDurability;
        [SerializeField] private Sprite handSprite;
        [SerializeField] private LayerMask enemyMask;
        [SerializeField] private DamageType damageType;
        [SerializeField] private List<PercentageDropBonus> percentageDropBonuses;
        [SerializeField] private PowerKnockbackTier powerKnockbackTier;
        [SerializeField] private bool canUseShield;

        public void Attack(IDamageable damageable, Vector2 position)
        {
            damageable.HealthService.TakeDamage(this.RandomDamage(), position, this.percentageDropBonuses, this.powerKnockbackTier);
        }

        public int RandomDamage() => RandomGenerator.RandomRange(this.minDamage, this.maxDamage);
    }

}