﻿using LasSecretRPG.Damageable;
using LasSecretRPG.Player;
using UnityEngine;

namespace LasSecretRPG.Items.Weapon
{
    public interface IMeleeWeapon : IWeapon
    {
        bool CanUseShield { get; }

        void Attack(IDamageable damageable, Vector2 position);
    }
}