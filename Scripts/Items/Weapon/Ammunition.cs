﻿using System.Text;
using UnityEngine;

namespace LasSecretRPG.Items.Weapon
{
    [CreateAssetMenu(fileName = "New Ammunition", menuName = "Items/Weapon/Ammunition")]
    public class Ammunition : Item
    {
        public GameObject AmmunitionPrefab => this.ammunitionPrefab;

        [SerializeField] private GameObject ammunitionPrefab = null;

    }
}
