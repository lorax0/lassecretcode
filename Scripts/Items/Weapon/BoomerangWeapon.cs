using UnityEngine;
using LasSecretRPG.Player;
using LasSecretRPG.Inputs;
using System.Collections.Generic;
using System.Collections;
using LasSecretRPG.Damageable;
using LasSecretRPG.Damageable.Knockback;
using LasSecretRPG.Damageable.Dropping;
using LasSecretRPG.Items.Modifiers;

namespace LasSecretRPG.Items.Weapon
{
    [CreateAssetMenu(fileName = "New Boomerang Weapon", menuName = "Items/Weapon/BoomerangWeapon")]
    public class BoomerangWeapon : Item, IDuratibility, IWeapon
    {
        public int MaxDamage => this.maxDamage;
        public int MinDamage => this.minDamage;
        public int MaxDurability => this.maxDurability;
        public DamageType DamageType => this.damageType;

        [SerializeField] private int minDamage;
        [SerializeField] private int maxDamage;
        [SerializeField] private float throwRange;
        [SerializeField] private float throwPower;
        [SerializeField] private int maxDurability;
        [SerializeField] private BoomerangController boomerangPrefab;
        [SerializeField] private LayerMask enemyMask;
        [SerializeField] private DamageType damageType;
        [SerializeField] private List<PercentageDropBonus> percentageDropBonuses;
        [SerializeField] private PowerKnockbackTier powerKnockbackTier;

        private BoomerangController throwedBoomerang;

        public void ThrowBoomerang(PlayerController playerController)
        {
            if (this.throwedBoomerang != null) return;
            this.throwedBoomerang = Instantiate(this.boomerangPrefab, playerController.transform.position, Quaternion.identity);

            Vector2 destination = this.GetDestinationPosition(this.throwedBoomerang.transform.position);
            this.throwedBoomerang.Initialize(playerController, destination, this.throwPower, this.enemyMask);
            this.throwedBoomerang.OnHit += this.Attack;
            this.throwedBoomerang.OnDestroy += () => Destroy(this.throwedBoomerang.gameObject);
            this.throwedBoomerang.OnDestroy += () => this.throwedBoomerang = null;
        }

        private void Attack(IDamageable damageable, Vector2 position)
        {
            damageable.HealthService.TakeDamage(this.RandomDamage(), position, this.percentageDropBonuses, this.powerKnockbackTier);
        }

        private Vector2 GetDestinationPosition(Vector2 startPosition)
        {
            Vector2 mousePosition = MouseRaycast.GetWorldMousePosition();
            Vector2 direction = (mousePosition - startPosition).normalized;
            return Vector2.Distance(startPosition, mousePosition) <= this.throwRange ? mousePosition : startPosition + direction * this.throwRange;
        }

        public int RandomDamage() => Random.Range(this.minDamage, this.maxDamage);
    }

}