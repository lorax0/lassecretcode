﻿using System.Collections.Generic;
using LasSecretRPG.Player;
using UnityEngine;

namespace LasSecretRPG.Items.Weapon
{
    public interface IRangeWeapon : IWeapon
    {
        IEnumerable<Ammunition> AmmunitionNeeded { get; }

        void Use(Transform clickPosition, PlayerController playerController, Ammunition ammunition);
    }
}