﻿using LasSecretRPG.Damageable;
using LasSecretRPG.Player;
using System;
using System.Collections;
using UnityEngine;

namespace LasSecretRPG.Items
{
    public class BoomerangController : MonoBehaviour
    {
        public Action<IDamageable, Vector2> OnHit;
        public Action OnDestroy;

        [SerializeField] private new Rigidbody2D rigidbody;

        private Coroutine moveCoroutine;
        private PlayerController playerController;
        private float throwPower;
        private LayerMask enemiesLayer;
        
        public void Initialize(PlayerController playerController, Vector2 destination, float throwPower, LayerMask enemiesLayer)
        {
            this.playerController = playerController;
            this.throwPower = throwPower;
            this.enemiesLayer = enemiesLayer;
            this.moveCoroutine = this.StartCoroutine(this.MoveToDestination(destination));
        }

        private IEnumerator MoveToDestination(Vector2 destination)
        {
            while(Vector2.Distance(this.transform.position, destination) > 0.1f)
            {
                yield return new WaitForFixedUpdate();
                this.transform.position = Vector2.MoveTowards(this.transform.position, destination, Time.deltaTime * this.throwPower);
            }
            this.moveCoroutine = this.StartCoroutine(this.ReturnToPlayer());
        }

        private IEnumerator ReturnToPlayer()
        {
            while (Vector2.Distance(this.transform.position, this.playerController.transform.position) > 0.1f)
            {
                yield return new WaitForFixedUpdate();
                this.transform.position = Vector2.MoveTowards(this.transform.position, this.playerController.transform.position, Time.deltaTime * this.throwPower);
            }

            this.OnDestroy?.Invoke();
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            if (((1 << collider.gameObject.layer) & this.enemiesLayer) == 0) return;
            IDamageable damageable = collider.GetComponent<IDamageable>();
            if (damageable != null)
            {
                this.OnHit?.Invoke(damageable, this.transform.position);
                this.StopCoroutine(this.moveCoroutine);
                this.moveCoroutine = this.StartCoroutine(this.ReturnToPlayer());
            }
        }
    }
}
