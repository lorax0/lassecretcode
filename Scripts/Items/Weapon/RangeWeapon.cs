using LasSecretRPG.Damageable.Dropping;
using LasSecretRPG.Items.Modifiers;
using LasSecretRPG.Player;
using LasSecretRPG.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace LasSecretRPG.Items.Weapon
{
    [CreateAssetMenu(fileName = "New Range Weapon", menuName = "Items/Weapon/RangeWeapon")]
    public class RangeWeapon : Item, IRangeWeapon, IToolVisible
    {
        public Sprite HandSprite => this.handSprite;
        public IEnumerable<Ammunition> AmmunitionNeeded => this.ammunitionNeeded;
        public int MaxDamage => this.maxDamage;
        public int MinDamage => this.minDamage;
        public int MaxDurability => this.maxDurability;
        public DamageType DamageType => this.damageType;

        [SerializeField] private Sprite handSprite;
        [SerializeField] private int minDamage;
        [SerializeField] private int maxDamage;
        [SerializeField] private int maxDurability;
        [SerializeField] private DamageType damageType;
        [SerializeField] private List<Ammunition> ammunitionNeeded;
        [SerializeField] private List<PercentageDropBonus> percentageDropBonuses;

        public void Use(Transform clickPosition, PlayerController playerController, Ammunition ammunition)
        {
            var bullet = Instantiate(ammunition.AmmunitionPrefab, clickPosition.position, clickPosition.rotation);
            Projectile projectile = bullet.GetComponent<Projectile>();
            projectile.Damage = this.RandomDamage();
            projectile.Owner = playerController.gameObject;
            projectile.PercentageDropBonuses = this.percentageDropBonuses;

        }

        public int RandomDamage() => RandomGenerator.RandomRange(this.minDamage, this.maxDamage);
    }
}