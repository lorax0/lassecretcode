﻿

using LasSecretRPG.Items.Modifiers;

namespace LasSecretRPG.Items.Weapon
{
    public interface IWeapon : IDuratibility
    {
        int MaxDamage { get; }
        int MinDamage { get; }
        DamageType DamageType { get; }

        int RandomDamage();
    }
}