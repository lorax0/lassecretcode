﻿using UnityEngine;

namespace LasSecretRPG.Items.Modifiers
{
    [CreateAssetMenu(fileName = "NewRangeWeaponModifier", menuName = "Items/Modifiers/RangeWepon")]
    public class RangeWeaponModifiers : Modifier
    {

    }
}
