﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Items.Modifiers
{

    [CreateAssetMenu(fileName = "ElementType", menuName = "Items/Modifiers/ElementType")]
    public class ElementType : ScriptableObject
    {
        public string ElementTypeName => this.name;
        public Color Color => this.color;

        [SerializeField] protected Color color;
    }
}
