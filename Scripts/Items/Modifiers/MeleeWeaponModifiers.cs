﻿using UnityEngine;

namespace LasSecretRPG.Items.Modifiers
{
    [CreateAssetMenu(fileName ="NewMeleeWeaponModifier", menuName = "Items/Modifiers/MeleeWepon")]
    public class MeleeWeaponModifiers : Modifier
    {

    }
}
