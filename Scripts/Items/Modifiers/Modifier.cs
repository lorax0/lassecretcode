﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Items.Modifiers
{
    [CreateAssetMenu(fileName = "NewModifier", menuName = "Items/Modifiers/Modifier")]
    public class Modifier : ScriptableObject
    {
        public string ModifierName => this.name;
    }
}
