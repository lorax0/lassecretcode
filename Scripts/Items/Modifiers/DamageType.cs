﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Items.Modifiers
{

    [CreateAssetMenu(fileName = "NewDamageType", menuName = "Items/Modifiers/DamageType")]
    public class DamageType : ScriptableObject
    {
        public string DamageTypeName => this.name;
    }
}
