﻿using UnityEngine;

namespace LasSecretRPG.Items.Modifiers
{
    [CreateAssetMenu(fileName = "New Rarity", menuName = "Items/Rarity")]
    public class Rarity : ScriptableObject
    {
        public string Name => this.name;
        public Color TextColour => this.textColour;

        [SerializeField] private new string name = "New Rarity Name";
        [SerializeField] private Color textColour = new Color(1f, 1f, 1f, 1f);
    }
}
