using UnityEngine;
using LasSecretRPG.Player;
using System;
using LasSecretRPG.Animals;
using UnityEngine.InputSystem;
using System.Linq;
using LasSecretRPG.Statistics;
using System.Collections.Generic;
using LasSecretRPG.Inputs;
using LasSecretRPG.Statistics.Buffs;

namespace LasSecretRPG.Items.Consumable
{
    [CreateAssetMenu(fileName = "New Food", menuName = "Items/ConsumableItem/Food")]
    public class Food : ConsumableItem, IBuffItem, IDuratibility
    {
        public IEnumerable<Buff> Buffs => this.buffs;
        public int MaxDurability => this.maxDurability;
        public int DayToSpoiled => this.dayToSpoiled;
        public Item SpoiledFood => this.spoiledFood;
        
        [SerializeField] private float foodValue;
        [SerializeField] private int maxDurability;
        [SerializeField] private int dayToSpoiled;
        [SerializeField] private Item spoiledFood;
        [SerializeField] private List<Buff> buffs;

        public override void Use(PlayerController player)
        {
            var animalToTame = this.AnimalToTame();
            if (animalToTame != null && animalToTame.CanTame(this))
            {
                animalToTame.Feed(player);
                return;
            }
            player.HungerService.Eat(this.foodValue);
            player.BuffsService.AddBuffs(this.buffs);
        }

        private Animal AnimalToTame() => Physics2D.RaycastAll(MouseRaycast.GetWorldMousePosition(), Vector2.zero).
            Select(t => t.transform.GetComponent<Animal>()).Where(t => t != null).FirstOrDefault();
    }
}