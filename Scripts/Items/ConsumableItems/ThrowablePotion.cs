using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using LasSecretRPG.Inputs;
using LasSecretRPG.Player;
using LasSecretRPG.Statistics;
using LasSecretRPG.Statistics.Buffs;
using UnityEngine;

namespace LasSecretRPG.Items.Consumable
{
    [CreateAssetMenu(fileName = "New Throwable Potion", menuName = "Items/ConsumableItem/ThrowablePotion")]
    public class ThrowablePotion : ConsumableItem, IPotion
    {
        public IEnumerable<Buff> Buffs => this.buffs;

        [SerializeField] private List<Buff> buffs;
        [SerializeField] private GameObject potionObject;
        [SerializeField] private float radius;
        [SerializeField] private float throwPower;
        [SerializeField] private float throwRange;

        public override void Use(PlayerController playerController)
        {
            playerController.StartCoroutine(this.ThrowPotion(playerController));
        }

        private IEnumerator ThrowPotion(PlayerController playerController)
        {
            Rigidbody2D potion = Instantiate(this.potionObject, playerController.transform.position, Quaternion.identity).GetComponent<Rigidbody2D>();
            Vector2 destination = this.GetDestinationPosition(potion.position);
            while (Vector2.Distance(potion.position, destination) > 0.1f)
            {
                yield return new WaitForFixedUpdate();
                potion.position = Vector2.MoveTowards(potion.position, destination, Time.deltaTime * this.throwPower);
            }
            this.AddBuffs(potion.position);
            Destroy(potion.gameObject);
            
        }

        private Vector2 GetDestinationPosition(Vector2 startPosition)
        {
            Vector2 mousePosition = MouseRaycast.GetWorldMousePosition();
            Vector2 direction = (mousePosition - startPosition).normalized;
            return Vector2.Distance(startPosition, mousePosition) <= this.throwRange ? mousePosition : startPosition + direction * this.throwRange;
        }

        private void AddBuffs(Vector2 position)
        {
            IEnumerable<IBuffable> buffables = Physics2D.OverlapCircleAll(position, this.radius).Select(t => t.GetComponent<IBuffable>());
            foreach (var buffable in buffables)
            {
                if (buffable == null) continue;
                buffable.BuffsService.AddBuffs(this.buffs);
            }
        }
    }
}