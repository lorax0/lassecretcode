﻿using System.Text;
using UnityEngine;
using LasSecretRPG.Player;

namespace LasSecretRPG.Items.Consumable
{
    [CreateAssetMenu(fileName = "New Consumable Item", menuName = "Items/Consumable Item")]
    public class ConsumableItem : Item, IUseable
    {
        public virtual bool CanUse(PlayerController playerController)
        {
            return true;
        }

        public virtual void Use(PlayerController playerController)
        {

        }
    }
}
