using System.Collections;
using System.Collections.Generic;
using LasSecretRPG.Player;
using LasSecretRPG.Statistics;
using LasSecretRPG.Statistics.Buffs;
using UnityEngine;

namespace LasSecretRPG.Items.Consumable
{
    [CreateAssetMenu(fileName = "New Drink Potion", menuName = "Items/ConsumableItem/DrinkPotion")]
    public class DrinkPotion : ConsumableItem, IPotion
    {
        public IEnumerable<Buff> Buffs => this.buffs;

        [SerializeField] private List<Buff> buffs;

        public override void Use(PlayerController playerController)
        {
            playerController.BuffsService.AddBuffs(this.buffs);
        }
    }
}