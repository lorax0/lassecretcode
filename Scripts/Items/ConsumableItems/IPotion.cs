using LasSecretRPG.Statistics;
using LasSecretRPG.Statistics.Buffs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Items.Consumable
{
    public interface IPotion : IBuffItem
    {

    }
}