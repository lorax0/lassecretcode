﻿namespace LasSecretRPG.Items
{
    public interface IDuratibility
    {
        int MaxDurability { get; }
    }
}