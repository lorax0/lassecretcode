using LasSecretRPG.Inventory;
using LasSecretRPG.Items.Consumable;
using LasSecretRPG.Items.Modifiers;
using LasSecretRPG.Items.Weapon;
using LasSecretRPG.Utilities;
using LasSecretRPG.Utilities.Time;
using System;
using System.Linq;
using System.Text;
using UnityEngine;

namespace LasSecretRPG.Items
{
    [Serializable]
    public class ItemData : ITooltipData
    {
        public Action OnSpoiled;

        public int Amount { get => this.amount; set => this.amount = value; }
        public bool IsFavourite { get => this.isFavourite; set => this.isFavourite = value; }
        public Item Item => this.item;
        public int CurrentDurability => this.currentDurability;
        public Modifier ItemModifier => this.itemModifier;
        public ElementType ElementType => this.elementType;

        [Min(1)] [SerializeField] protected int amount = 1;
        [SerializeField] protected Item item;

        private Modifier itemModifier;
        private ElementType elementType;
        private int currentDurability;
        private int dayToSpoiled;
        private bool isFavourite;

        public ItemData(Item item, int amount)
        {
            this.item = item;
            this.amount = amount;
        }

        public void Initialize()
        {
            if (this.item is Food food && food.SpoiledFood != null)
            {
                this.dayToSpoiled = food.DayToSpoiled;
                TimeManager.Instance.DayCounter.OnDayEnd += this.DecreaseDay;
            }
        }

        public int DecreaseDurability(int durabilityToDecrease)
        {
            this.currentDurability -= durabilityToDecrease;
            return this.currentDurability;
        }
        
        public void SetDurability(int maxDurability) => this.currentDurability = maxDurability;

        public void DropItem(Vector2 position)
        {
            var itemController = MonoBehaviour.Instantiate(this.item.ItemPrefab, position, Quaternion.identity);
            itemController.gameObject.SetActive(true);
            itemController.ItemData = this;
            itemController.Throw();
        }

        public void RandomizeModifier()
        {
            var possiblyModifiers = this.item.PossiblyModifiers;
            if (possiblyModifiers.Count() == 0) return;
            var randomIndex = RandomGenerator.RandomRange(0, possiblyModifiers.Count() - 1);
            this.itemModifier = possiblyModifiers.ElementAt(randomIndex);
        }

        public void RandomizeElement()
        {
            var possiblyElements = this.item.PossiblyElementsModifier;
            if (possiblyElements.Count() == 0) return;
            var randomIndex = RandomGenerator.RandomRange(0, possiblyElements.Count() - 1);
            this.elementType = possiblyElements.ElementAt(randomIndex);
        }

        public virtual string GetInfoDisplayText()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append(this.item.Rarity.Name).AppendLine();
            builder.Append(this.GetName());
            builder.Append(this.GetColourDamageType());
            builder.Append("<color=green>Use: ").Append(this.item.Description).Append("</color>").AppendLine();
            builder.Append("Max Stack: ").Append(this.item.MaximalItemAmount).AppendLine();
            builder.Append("Sell Price: ").Append(this.item.SellPrice).Append(" Gold");

            return builder.ToString();
        }

        public string GetName()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("<size=35>").Append(this.GetColourName()).Append("</size>\n");
            return builder.ToString();
        }

        private string GetColourName()
        {
            string hexColour = ColorUtility.ToHtmlStringRGB(this.item.Rarity.TextColour);
            if(this.itemModifier == null) return $"<color=#{hexColour}>{this.item.Name}</color>";
            return $"<color=#{hexColour}>{this.itemModifier.ModifierName + " " + this.item.Name}</color>";
        }

        private string GetColourDamageType()
        {
            IWeapon weapon = this.item as IWeapon;
            if (weapon?.DamageType == null) return string.Empty;
            if (this.elementType == null) return weapon.DamageType.DamageTypeName + "\n";
            string hexColour = ColorUtility.ToHtmlStringRGB(this.elementType.Color);
            return $"<color=#{hexColour}>{this.elementType.ElementTypeName + " " + weapon.DamageType.DamageTypeName + "\n"}</color>";
        }

        private void DecreaseDay()
        {
            this.dayToSpoiled--;
            if(this.dayToSpoiled <= 0)
            {
                TimeManager.Instance.DayCounter.OnDayEnd -= this.DecreaseDay;
                this.OnSpoiled?.Invoke();
            }
        }
    }
}