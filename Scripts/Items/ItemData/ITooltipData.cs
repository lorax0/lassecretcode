﻿namespace LasSecretRPG.Inventory
{
    public interface ITooltipData
    {
        string GetInfoDisplayText();

        string GetName();
    }
}