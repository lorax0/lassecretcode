﻿using UnityEngine;

namespace LasSecretRPG.Items
{
    public interface IToolVisible
    {
        Sprite HandSprite { get; }
    }
}