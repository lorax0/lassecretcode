﻿using System;
using System.Collections;
using UnityEngine;
using LasSecretRPG.Interactables;
using LasSecretRPG.Player;
using LasSecretRPG.Utilities;
using LasSecretRPG.Inputs;
using LasSecretRPG.Items.Consumable;

namespace LasSecretRPG.Items
{
    public class ItemController : MonoBehaviour, IAttractable
    {
        public ItemData ItemData { get => this.itemData; set => this.itemData = value; }
        public Transform Transform => this.transform;

        [SerializeField] private ItemData itemData;
        [SerializeField] private float takeDistance;
        [SerializeField] private float attractSpeed;
        [SerializeField] private float throwSpeed;
        [SerializeField] private float throwMaxDistance;

        private void Start()
        {
            if (this.itemData.Item == null) Debug.LogWarning("Item is null");
            if (this.itemData.Item is IDuratibility duratibility) this.itemData.SetDurability(duratibility.MaxDurability);
            if (this.itemData.ItemModifier == null) this.itemData.RandomizeModifier();
            if (this.itemData.ElementType == null) this.itemData.RandomizeElement();
            this.itemData.OnSpoiled += this.CreateSpoiledFood;
            this.itemData.OnSpoiled += () => Destroy(this.gameObject);
            this.itemData.Initialize();
        }

        public void AddItem(PlayerController player)
        {
            if (player.Inventory.CanAddItem(this.itemData))
            {
                player.Inventory.AddItem(this.itemData);
                player.Inventory.DestroyWorldItem(this);
            }
            else
            {
                this.StartCoroutine(this.AttractItem(player));
            }
        }

        public void Attract(PlayerController playerController) => this.StartCoroutine(this.AttractItem(playerController));

        public void StopAttracting() => this.StopAllCoroutines();

        private IEnumerator AttractItem(PlayerController playerController)
        {
            while (Vector2.Distance(this.transform.position, playerController.transform.position) > this.takeDistance)
            {
                this.transform.position = Vector2.MoveTowards(this.transform.position, playerController.transform.position, this.attractSpeed * Time.deltaTime);
                yield return new WaitForFixedUpdate();
            }
            this.AddItem(playerController);
        }

        public void Throw()
        {
            this.StartCoroutine(this.ThrowItem());
        }

        private IEnumerator ThrowItem()
        {
            Collider2D collider = this.GetComponent<Collider2D>();
            collider.enabled = false;
            Vector2 destination = this.GetDestinationThrowPosition();
            
            while (Vector2.Distance(this.transform.position, destination) > 0.1f)
            {
                yield return new WaitForFixedUpdate();
                this.transform.position = Vector2.MoveTowards(this.transform.position, destination, Time.deltaTime * this.throwSpeed);
            }

            collider.enabled = true;
        }

        private Vector2 GetDestinationThrowPosition()
        {
            Vector2 mousePosition = MouseRaycast.GetWorldMousePosition();
            Vector2 direction = (mousePosition - (Vector2)this.transform.position).normalized;
            return Vector2.Distance(this.transform.position, mousePosition) <= this.throwMaxDistance ? mousePosition : (Vector2)this.transform.position + direction * this.throwMaxDistance;
        }

        private void CreateSpoiledFood()
        {
            Food food = this.itemData.Item as Food;
            var itemController = MonoBehaviour.Instantiate(food.SpoiledFood.ItemPrefab, this.transform.position, Quaternion.identity);
            itemController.gameObject.SetActive(true);
        }
        
    }
}
