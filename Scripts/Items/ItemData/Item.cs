﻿using UnityEngine;
using System.Collections.Generic;
using LasSecretRPG.Items.Modifiers;

namespace LasSecretRPG.Items
{
    public class Item : ScriptableObject
    {
        public string Name => this.name;
        public string Description => this.description;
        public Sprite Sprite => this.icon;
        public int MaximalItemAmount => this.maximalItemAmount;
        public int SellPrice => this.sellPrice;
        public Rarity Rarity => this.rarity;
        public float UseCooldown => this.useCooldown;
        public ItemController ItemPrefab => this.itemPrefab;
        public IEnumerable<Modifier> PossiblyModifiers => this.possiblyModifiers;
        public IEnumerable<ElementType> PossiblyElementsModifier => this.possiblyElementsModifier;

        [SerializeField] protected new string name = "New Item Name";
        [SerializeField] protected string description;
        [SerializeField] protected Sprite icon = null;
        [Min(1)] [SerializeField] protected int maximalItemAmount = 1;
        [Min(1)] [SerializeField] protected int sellPrice = 1;
        [SerializeField] protected Rarity rarity;
        [SerializeField] protected float useCooldown;
        [SerializeField] protected ItemController itemPrefab;
        [SerializeField] protected List<Modifier> possiblyModifiers;
        [SerializeField] protected List<ElementType> possiblyElementsModifier;

    }
}
