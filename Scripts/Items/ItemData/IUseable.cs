﻿using LasSecretRPG.Player;

namespace LasSecretRPG.Items
{
    public interface IUseable
    {
        void Use(PlayerController playerController);

        bool CanUse(PlayerController playerController);
    }
}