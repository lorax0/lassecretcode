﻿using UnityEngine;
using System.Collections;
using System;
using LasSecretRPG.Damageable;
using System.Collections.Generic;
using LasSecretRPG.Damageable.Knockback;
using LasSecretRPG.Damageable.Dropping;
using LasSecretRPG.Items.Modifiers;
using LasSecretRPG.Items.Weapon;
using LasSecretRPG.Utilities;

namespace LasSecretRPG.Items.Tools
{
    public class Tool : Item, IMeleeWeapon
    {
        public int Power => this.power;
        public DamageType DamageType => this.damageType;
        public int MaxDamage => throw new NotImplementedException();
        public int MinDamage => throw new NotImplementedException();
        public int MaxDurability => throw new NotImplementedException();
        public bool CanUseShield => throw new NotImplementedException();

        [SerializeField] private int minDamage;
        [SerializeField] private int maxDamage;
        [SerializeField] private int maxDurability;
        [SerializeField] private DamageType damageType;
        [SerializeField] private List<PercentageDropBonus> percentageDropBonuses;
        [SerializeField] private PowerKnockbackTier powerKnockbackTier;

        protected int power;
        
        public bool HaveEnoughPower(int neededPower) => this.power >= neededPower;

        public int RandomDamage() => RandomGenerator.RandomRange(this.minDamage, this.maxDamage);

        public void Attack(IDamageable damageable, Vector2 position)
        {
            damageable.HealthService.TakeDamage(this.RandomDamage(), position, this.percentageDropBonuses, this.powerKnockbackTier);
        }

        public void Use(Transform point)
        {

        }
    }
}