﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace LasSecretRPG.Bestiary
{
    [Serializable]
    public class BestiaryValue
    {
        public Object Value => this.value;
        public int KilledEnemies => this.killedEnemies;

        [SerializeField] private Object value;
        [SerializeField] private int killedEnemies;
    }
}
