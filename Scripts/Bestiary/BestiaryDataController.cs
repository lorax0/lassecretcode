﻿using LasSecretRPG.Enemies;
using LasSecretRPG.MapGeneration;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace LasSecretRPG.Bestiary
{
    public class BestiaryDataController : MonoBehaviour
    {
        public IBestiarable Bestiarable => this.bestiarable;

        [SerializeField] private Image enemyImage;
        [SerializeField] private Text enemyName;
        [SerializeField] private Text enemyBiome;
        [SerializeField] private GameObject bestiarableGameobject;

        private IBestiarable bestiarable;
        private int killed;

        private void Start()
        {
            this.bestiarable = this.bestiarableGameobject.GetComponent<IBestiarable>();
        }

        public void Set(IBestiarable bestiarable)
        {
            this.bestiarable = bestiarable;
        }

        public void Increment()
        {
            this.killed++;
            this.CheckName();
            this.CheckImage();
            this.CheckBiome();
        }

        private void CheckName()
        {
            if (this.killed > 0)
                this.enemyName.text = this.bestiarable.Name;

        }

        private void CheckImage()
        {
            if (this.bestiarable.BestiaryImage.KilledEnemies <= this.killed)
                this.enemyImage.sprite = this.bestiarable.BestiaryImage.Value as Sprite;
        }

        private void CheckBiome()
        {
            if (this.bestiarable.Biome.KilledEnemies <= this.killed)
                this.enemyBiome.text = this.bestiarable.Biome.Value.name;
        }
    }
}