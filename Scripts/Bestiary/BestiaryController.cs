using System;
using System.Collections.Generic;
using System.Linq;
using LasSecretRPG.Enemies;
using UnityEngine;

namespace LasSecretRPG.Bestiary
{
    public class BestiaryController : MonoBehaviour
    {
        [SerializeField] private List<BestiaryDataController> bestiaryDatas;
        
        public void IncrementKilledEnemies(IBestiarable bestiarable)
        {
            BestiaryDataController bestiaryData = this.bestiaryDatas.FirstOrDefault(t => t.Bestiarable.Name == bestiarable.Name);
            bestiaryData.Increment();
        }
    }
}