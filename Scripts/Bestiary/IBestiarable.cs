﻿using LasSecretRPG.MapGeneration;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Bestiary
{
    public interface IBestiarable
    {
        string Name { get; }
        BestiaryValue Biome { get; }
        BestiaryValue BestiaryImage { get; }
    }
}