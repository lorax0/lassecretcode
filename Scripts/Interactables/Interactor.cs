using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using LasSecretRPG.Player;
using System;

namespace LasSecretRPG.Interactables
{
    public class Interactor : MonoBehaviour
    {
        [SerializeField] private InputActionReference input = null;
        [SerializeField] private PlayerController playerController = null;

        private List<IInteractable> interactables = new List<IInteractable>();
        private IInteractable closestInteractable;

        private void Start()
        {
            this.input.action.performed += action => this.CheckForInteraction();
        }

        private void Update()
        {
            this.SetClosestIteractable();
        }

        private void OnEnable()
        {
            this.input.action.Enable();
        }

        private void OnDisable()
        {
            this.input.action.Disable();
        }
        
        private void CheckForInteraction()
        {
            if (this.closestInteractable == null) return;
            this.closestInteractable.Interact(this.playerController);
            
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            this.AddInteractable(other.GetComponent<IInteractable>());
            this.AttractItem(other.GetComponent<IAttractable>());
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            this.RemoveInteractable(other.GetComponent<IInteractable>());
            this.StopAttractingItem(other.GetComponent<IAttractable>());
        }

        private void AttractItem(IAttractable attractable)
        {
            if (attractable == null) return;
            attractable.Attract(this.playerController);
        }

        private void StopAttractingItem(IAttractable attractable)
        {
            if (attractable == null) return;
            attractable.StopAttracting();
        }

        private void AddInteractable(IInteractable interactable)
        {
            if (interactable == null) return;

            this.interactables.Add(interactable);
            this.SetClosestIteractable();
        }

        private void RemoveInteractable(IInteractable interactable)
        {
            if (interactable == null) return;
            if (interactable.IsInteracting(this.playerController)) interactable.Interact(this.playerController);

            this.interactables.Remove(interactable);
            this.SetClosestIteractable();
        }

        private void SetClosestIteractable()
        {
            if(this.closestInteractable != null)
            {
                this.closestInteractable.Unhighlight();
                this.closestInteractable = null;
            }
            if (this.interactables.Count == 0) return;
            float minDistance = Mathf.Infinity;
            foreach (var interactable in this.interactables)
            {
                float distance = Vector2.Distance(interactable.Transform.position, this.playerController.transform.position);
                if (distance > minDistance) continue;
                minDistance = distance;
                this.closestInteractable = interactable;
            }
            this.closestInteractable?.Highlight();
        }
    }

}