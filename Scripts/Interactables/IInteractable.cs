using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LasSecretRPG.Player;

namespace LasSecretRPG.Interactables
{
    public interface IInteractable
    {
        Transform Transform { get; }
        void Interact(PlayerController player);
        bool IsInteracting(PlayerController player);
        void Highlight();
        void Unhighlight();
    }
}