using LasSecretRPG.Player;

namespace LasSecretRPG.Interactables
{
    public interface IAttractable
    {
        void Attract(PlayerController player);

        void StopAttracting();
    }
}