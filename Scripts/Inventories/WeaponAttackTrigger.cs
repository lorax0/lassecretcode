﻿using System.Collections;
using LasSecretRPG.Damageable;
using LasSecretRPG.Items.Weapon;
using UnityEngine;

namespace LasSecretRPG.Inventory
{
    public class WeaponAttackTrigger : MonoBehaviour
    {
        private IMeleeWeapon weapon;
        private new Collider2D collider;

        private void Start()
        {
            this.collider = this.GetComponent<Collider2D>();
        }

        public void Attack(IMeleeWeapon weapon, float time, Inventory inventory)
        {
            if (inventory.ShieldController.IsShieldSet() && !weapon.CanUseShield) inventory.ShieldController.SetShield(null);
            this.weapon = weapon;
            this.collider.enabled = true;
            this.StartCoroutine(this.EndAttack(time));
        }

        private IEnumerator EndAttack(float time)
        {
            yield return new WaitForSeconds(time);
            this.collider.enabled = false;
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            IDamageable damageable = collider.GetComponent<IDamageable>();
            if (damageable != null)
            {
                this.weapon.Attack(damageable, this.transform.position);
            }
        }
    }
}
