using LasSecretRPG.Items;
using LasSecretRPG.Items.Weapon;
using System;
using UnityEngine;

namespace LasSecretRPG.Inventory.Hotbar
{
    [Serializable]
    public class ToolsHandler
    {
        [SerializeField] private SpriteRenderer toolSpriteRenderer;
        [SerializeField] private Animator toolsHandlerAnimator;
        [SerializeField] private AnimationClip attackAnimation;
        [SerializeField] private WeaponAttackTrigger weaponAttackTrigger;

        private bool clockwise;

        public void PlayAttackAnimation(IMeleeWeapon meleeWeapon, Inventory inventory)
        {
            this.weaponAttackTrigger.Attack(meleeWeapon, this.attackAnimation.length, inventory);
            if (this.clockwise)
                this.toolsHandlerAnimator.Play("Tool_Clockwise");
            else
                this.toolsHandlerAnimator.Play("Tool_AntiClockwise");

            this.clockwise = !this.clockwise;
        }

        public void PlayIdleAnimation()
        {
            if (this.clockwise)
                this.toolsHandlerAnimator.Play("Tool_After_AntiClockwise");
            else
                this.toolsHandlerAnimator.Play("Tool_After_Clockwise");
        }


        public void ChangeTool(IToolVisible tool)
        {
            this.toolSpriteRenderer.sprite = tool?.HandSprite;
        }

    }
}