﻿using System;
using LasSecretRPG.Items;
using LasSecretRPG.Items.Consumable;
using LasSecretRPG.Items.Tools;
using LasSecretRPG.Items.Weapon;
using LasSecretRPG.Items.Armory;
using LasSecretRPG.Player;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace LasSecretRPG.Inventory.Slots
{
    public class HotbarSlot : InventorySlot
    {
        public Action OnItemRemoved;
        
        public Animator Animator => this.animator;

        [SerializeField] private Image durabilityImage;

        private Animator animator;

        private void Awake()
        {
            this.animator = this.GetComponent<Animator>();
            this.durabilityImage.gameObject.SetActive(false);
        }

        public void Use(Transform point, PlayerController playerController, Ammunition ammunition = null, int power = 0)
        {
            if (this.itemData?.Item == null) return;
            else if (this.itemData.Item is IRangeWeapon rangeWeapon)
            {
                if (ammunition == null) return;
                rangeWeapon.Use(point, playerController, ammunition);
            }
            else if (this.itemData.Item is BoomerangWeapon boomerang)
            {
                boomerang.ThrowBoomerang(playerController);
            }
            else if (this.itemData.Item is Tool tool)
            {
                if (!tool.HaveEnoughPower(power)) return;
                tool.Use(point);
            }
            else if (this.itemData.Item is Food food)
            {
                food.Use(playerController);
            }
            else if (this.itemData.Item is IUseable item)
            {
                bool canUse = item.CanUse(playerController);
                if (canUse)
                {
                    item.Use(playerController);
                    this.DecreaseItemAmount(1);
                    return;
                }
            }
            if (this.itemData.Item is Shield shield) playerController.Inventory.ShieldController.SetShield(this.itemData);
            else if (this.itemData?.Item is IDuratibility duratibility) this.DecreaseDurability(duratibility);
        }

        public override void AddItem(ItemData itemData)
        {
            base.AddItem(itemData);
            if (itemData.Item is IDuratibility duratibility)
            {
                this.durabilityImage.gameObject.SetActive(true);
                if(duratibility.MaxDurability != 1) this.ChangeDurabilityBar(this.itemData.CurrentDurability/ duratibility.MaxDurability);
            }

        }

        public override void RemoveItem()
        {
            base.RemoveItem();
            this.durabilityImage.gameObject.SetActive(false);
            this.OnItemRemoved?.Invoke();
        }
        
        private void DecreaseDurability(IDuratibility duratibility)
        {
            float currentDurability = this.itemData.DecreaseDurability(1);
            this.ChangeDurabilityBar(currentDurability / duratibility.MaxDurability);
            if (currentDurability <= 0) this.RemoveItem();
        }

        private void ChangeDurabilityBar(float currentDurabilityPercentage) => this.durabilityImage.fillAmount = currentDurabilityPercentage;
    }
}
