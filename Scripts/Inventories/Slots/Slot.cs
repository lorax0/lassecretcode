﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using LasSecretRPG.Items.Armory;
using LasSecretRPG.Items;
using LasSecretRPG.Items.Consumable;

namespace LasSecretRPG.Inventory.Slots
{
    public abstract class Slot : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        public ItemData ItemData => this.itemData;
        public virtual bool IsBlocked => false;
        
        public Action<Slot> OnPointerExitEvent;
        public Action<Slot> OnPointerEnterEvent;
        public Action<Slot> OnPointerClickEvent;
        public Action<PointerEventData> OnBeginDragEvent;
        public Action<PointerEventData> OnDragEvent;
        public Action<PointerEventData> OnEndDragEvent;

        protected ItemData itemData;

        public virtual void AddItem(ItemData itemData)
        {
            this.itemData = itemData;
            itemData.OnSpoiled = null;
            itemData.OnSpoiled += this.OnSpoiledItem;
        }

        public virtual void IncreaseItemAmount(int amount)
        {
            this.itemData.Amount += amount;
        }

        public virtual void DecreaseItemAmount(int amount)
        {
            if (this.itemData.Amount - amount == 0)
            {
                this.RemoveItem();
                return;
            }
            this.itemData.Amount -= amount;
        }

        public virtual bool CanAdd(ItemData itemData) => this.IsEmpty() || this.CanMatchItem(itemData);

        public virtual bool IsCorrectType(ItemData itemData) => true;

        public virtual bool CanRemove() => this.itemData != null;

        public bool IsEmpty() => this.itemData == null;

        public bool IsFull() => this.itemData?.Amount == this.itemData?.Item?.MaximalItemAmount;

        public int GetPossibleAmountOfItemToAdd() => this.itemData.Item.MaximalItemAmount - this.itemData.Amount;

        public virtual bool CanMatchItem(ItemData itemData) => itemData?.Item == this.itemData?.Item && !this.IsFull();

        public void DropItem(Vector2 position)
        {
            this.itemData.DropItem(position);
            this.RemoveItem();
        }
        
        public virtual void RemoveItem()
        {
            this.itemData.OnSpoiled -= this.OnSpoiledItem;
            this.itemData = null;
        }
        
        public void OnBeginDrag(PointerEventData eventData) => this.OnBeginDragEvent?.Invoke(eventData);

        public void OnDrag(PointerEventData eventData) => this.OnDragEvent?.Invoke(eventData);

        public void OnEndDrag(PointerEventData eventData) => this.OnEndDragEvent?.Invoke(eventData);

        public void OnPointerEnter(PointerEventData eventData)
        {
            this.OnPointerEnterEvent?.Invoke(this);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (!this.IsEmpty()) this.OnPointerExitEvent?.Invoke(this);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            this.OnPointerClickEvent?.Invoke(this);
        }
        
        private void OnSpoiledItem()
        {
            Food food = this.itemData.Item as Food;
            ItemData spoiledItemData = new ItemData(food.SpoiledFood, 1);
            spoiledItemData.Initialize();
            this.AddItem(spoiledItemData);
        }
    }
}
