﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace LasSecretRPG.Inventory.Slots
{
    [Serializable]
    public class ImageTextConnection
    {
        [SerializeField] private TextMeshProUGUI itemAmountText = null;
        [SerializeField] private Image image = null;
        
        public void UpdateSlotImage(Sprite itemSprite)
        {
            this.image.gameObject.SetActive(true);
            this.image.sprite = itemSprite;
            if (itemSprite == null)
            {
                this.image.gameObject.SetActive(false);
            }
        }

        public void UpdateSlotAmountText(int? amount)
        {
            if (amount == 1)
            {
                this.itemAmountText.gameObject.SetActive(false);
                return;
            }
            this.itemAmountText.gameObject.SetActive(true);
            this.itemAmountText.text = amount.ToString();
        }

        public void DisactiveImageText()
        {
            this.image.gameObject.SetActive(false);
            this.itemAmountText.gameObject.SetActive(false);
        }

        public void ChangeColor(Color color)
        {
            this.image.color = color;
        }
    }
}
