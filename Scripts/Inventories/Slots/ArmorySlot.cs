﻿using System;
using UnityEngine;
using LasSecretRPG.Items.Armory;
using LasSecretRPG.Items;

namespace LasSecretRPG.Inventory.Slots
{
    public class ArmorySlot : Slot
    {
        public Action<int> OnArmorWear;
        public Action<int> OnArmorUnwear;

        [SerializeField] private UnityEngine.Object armorType;
        [SerializeField] private ArmoryImageTextConnection imageTextConnection;

        public override bool CanAdd(ItemData itemData) => this.IsEmpty() && this.IsCorrectType(itemData);

        public override bool IsCorrectType(ItemData itemData) => itemData.Item.GetType().Name == this.armorType.name;

        public override void AddItem(ItemData itemData)
        {
            base.AddItem(itemData);
            IArmor armor = this.itemData.Item as IArmor;
            this.OnArmorWear?.Invoke(armor.DefencePoints);
            this.imageTextConnection.UpdateArmorSprite(armor.ArmorSprite);
            this.imageTextConnection.UpdateSlotImage(itemData.Item.Sprite);
            this.imageTextConnection.UpdateSlotAmountText(itemData.Amount);
        }

        public override void RemoveItem()
        {
            this.OnArmorUnwear?.Invoke((this.itemData.Item as IDefencePoints).DefencePoints);
            this.imageTextConnection.UpdateSlotImage(null);
            this.imageTextConnection.UpdateSlotAmountText(null);
            this.imageTextConnection.UpdateArmorSprite(null);
            base.RemoveItem();
        }

        public override bool CanMatchItem(ItemData itemData) => false;
    }
}
