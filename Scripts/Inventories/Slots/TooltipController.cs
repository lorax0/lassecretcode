using UnityEngine;

namespace LasSecretRPG.Inventory.Slots
{
    public class TooltipController : MonoBehaviour
    {
        public Tooltip Tooltip => this.tooltip;

        [SerializeField] private Tooltip tooltip;

        private void Update() => this.tooltip.UpdatePosition();

    }
}