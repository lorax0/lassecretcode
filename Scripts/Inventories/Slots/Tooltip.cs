﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

namespace LasSecretRPG.Inventory.Slots
{
    [Serializable]
    public class Tooltip
    {
        [SerializeField] private RectTransform tooltipCanvasRectTransform;
        [SerializeField] private RectTransform background;
        [SerializeField] private TextMeshProUGUI textMeshPro;
        [SerializeField] private Vector2 padding;
        [SerializeField] private RectTransform rectTransform;
        
        public void UpdatePosition()
        {
            Vector2 anchoredPosition = Mouse.current.position.ReadValue() / this.tooltipCanvasRectTransform.localScale.x;

            if(anchoredPosition.x + this.background.rect.width > this.tooltipCanvasRectTransform.rect.width)
            {
                anchoredPosition.x = this.tooltipCanvasRectTransform.rect.width - this.background.rect.width;
            }
            if (anchoredPosition.y + this.background.rect.height > this.tooltipCanvasRectTransform.rect.height)
            {
                anchoredPosition.y = this.tooltipCanvasRectTransform.rect.height - this.background.rect.height;
            }
            this.rectTransform.anchoredPosition = anchoredPosition;
        }

        public void SetText(string text)
        {
            this.SetActive(true);
            this.textMeshPro.SetText(text);
            this.textMeshPro.ForceMeshUpdate();

            Vector2 textSize = this.textMeshPro.GetRenderedValues(false);
            this.background.sizeDelta = textSize + this.padding;
        }

        public void SetActive(bool value) => this.rectTransform.gameObject.SetActive(value);
    }
}
