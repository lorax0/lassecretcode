﻿using LasSecretRPG.Items;
using LasSecretRPG.Items.Armory;
using System;
using UnityEngine;

namespace LasSecretRPG.Inventory.Slots
{
    public class ShieldSlot : Slot
    {
        public Action<Shield> OnArmorWear;
        public Action<Shield> OnArmorUnwear;

        [SerializeField] private ImageTextConnection imageTextConnection;

        public override void AddItem(ItemData itemData)
        {
            base.AddItem(itemData);
            this.imageTextConnection.UpdateSlotImage(itemData.Item.Sprite);
            this.OnArmorWear?.Invoke(itemData.Item as Shield);
        }

        public override void RemoveItem()
        {
            this.OnArmorUnwear?.Invoke(this.itemData.Item as Shield);
            base.RemoveItem();
            this.imageTextConnection.UpdateSlotImage(null);
        }
    }
}
