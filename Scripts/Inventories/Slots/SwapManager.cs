﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

namespace LasSecretRPG.Inventory.Slots
{
    [Serializable]
    public class SwapManager
    {
        private Slot slot;
        private Image itemMoveImage;

        public SwapManager(PointerEventData eventData, Image itemMoveImage, Transform transform) : base()
        {
            this.slot = this.GetComponentFromEventData<Slot>(eventData);
            if (this.slot?.ItemData == null || !this.slot.CanRemove()) return;
            this.itemMoveImage = MonoBehaviour.Instantiate(itemMoveImage, transform);
            this.itemMoveImage.sprite = this.slot.ItemData.Item.Sprite;
            this.itemMoveImage.rectTransform.SetAsLastSibling();
        }

        public void Swap(PointerEventData eventData)
        {
            if (this.slot?.ItemData == null || !this.slot.CanRemove()) return;
            this.DestroyItemImage();
            Slot newSlot = this.GetComponentFromEventData<Slot>(eventData);
            Trash trash = this.GetComponentFromEventData<Trash>(eventData);


            if (trash != null && !this.slot.ItemData.IsFavourite)
            {
                this.Trash(trash);
                return;
            }
            if (newSlot == null)
            {
                if (!this.slot.ItemData.IsFavourite)
                {
                    this.Drop();
                }
                return;
            }
            if (newSlot.IsBlocked) return;
            if (!newSlot.IsCorrectType(this.slot.ItemData)) return;
            if (this.slot == newSlot) return;
            if (!newSlot.CanAdd(this.slot.ItemData))
            {
                if (!this.slot.IsCorrectType(newSlot.ItemData)) return;
                if (!newSlot.CanRemove() || this.slot.IsBlocked) return;
                this.SwapItems(this.slot, newSlot);
                return;
            }
            if (newSlot.IsEmpty())
            {
                newSlot.AddItem(this.slot.ItemData);
                this.slot.RemoveItem();
            }
            else
            {
                int amountOfItemToAdd = newSlot.GetPossibleAmountOfItemToAdd();
                newSlot.IncreaseItemAmount(amountOfItemToAdd);
                this.slot.DecreaseItemAmount(amountOfItemToAdd);
            }
            this.slot = null;
        }

        private void Drop()
        {
            var position = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width / 2, Screen.height / 2));
            this.slot.DropItem(position);
        }

        private void Trash(Trash trash)
        {
            trash.TrashItem(this.slot.ItemData);
            this.slot.RemoveItem();
        }

        public T GetComponentFromEventData<T>(PointerEventData eventData) where T : MonoBehaviour
            => eventData.hovered.Where(t => t.GetComponent<T>() != null).FirstOrDefault()?.GetComponent<T>();

        public void MoveItemImage(PointerEventData eventData)
        {
            if (this.itemMoveImage == null) return;
            this.itemMoveImage.transform.position = eventData.position;
        }

        private void DestroyItemImage()
        {
            MonoBehaviour.Destroy(this.itemMoveImage.gameObject);
            this.itemMoveImage = null;
        }

        private void SwapItems(Slot slot, Slot newSlot)
        {
            var itemFromBasicSlot = slot.ItemData;
            var itemFromNewSlot = newSlot.ItemData;
            slot.RemoveItem();
            newSlot.RemoveItem();
            slot.AddItem(itemFromNewSlot);
            newSlot.AddItem(itemFromBasicSlot);
        }
    }
}