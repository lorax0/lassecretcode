﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace LasSecretRPG.Inventory.Slots
{
    [Serializable]
    public class ArmoryImageTextConnection : ImageTextConnection
    {
        [SerializeField] private SpriteRenderer armorSlotRenderer;

        public void UpdateArmorSprite(Sprite armorSprite)
        {
            this.armorSlotRenderer.sprite = armorSprite;
        }
    }
}
