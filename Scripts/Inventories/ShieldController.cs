﻿using LasSecretRPG.Inventory.Slots;
using LasSecretRPG.Items;
using LasSecretRPG.Items.Armory;
using LasSecretRPG.Player;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

namespace LasSecretRPG.Inventory
{
    [Serializable]
    public class ShieldController
    {
        [SerializeField] private ShieldSlot shieldSlot;
        [SerializeField] private SpriteRenderer shieldRenderer;
        [SerializeField] private InputActionReference useShield;
        [SerializeField] private float timeForPerfectBlock;

        private PlayerController playerController;
        private Coroutine handShield;
        private Coroutine perfectBlock;

        public void Initialize(PlayerController playerController)
        {
            this.playerController = playerController;
            this.useShield.action.started += (action) => this.ChangeShieldActivity(true);
            this.useShield.action.canceled += (action) => this.ChangeShieldActivity(false);
            this.shieldSlot.OnArmorWear += this.ShieldWear;
            this.shieldSlot.OnArmorUnwear += this.ShieldUnwear;
        }

        private void ShieldWear(Shield shield)
        {
            this.playerController.HealthService.IncreaseArmorPoints(shield.DefencePoints);
            this.playerController.BuffsService.AddBuffs(shield.WearBuffs);
            this.playerController.BuffsService.AddBuffs(shield.HandBuffs);
            this.playerController.KnockbackService.Shield = shield;
        }

        private void ShieldUnwear(Shield shield)
        {
            this.playerController.HealthService.DecreaseArmorPoints(shield.DefencePoints);
            this.playerController.BuffsService.RemoveBuffs(shield.WearBuffs);
            this.playerController.BuffsService.RemoveBuffs(shield.HandBuffs);
            this.playerController.KnockbackService.Shield = null;
        }

        private void ChangeShieldActivity(bool activity)
        {
            if (!this.IsShieldSet()) return;
            Shield shield = this.CurrentShield();
            if (activity)
            {
                this.handShield = this.playerController.StartCoroutine(this.HandShield());
                this.perfectBlock = this.playerController.StartCoroutine(this.PerfectBlockCheck());
                this.playerController.BuffsService.AddBuffs(shield.BlockingBuffs);
                return;
            }
            if (this.handShield != null) this.playerController.StopCoroutine(this.handShield);
            this.playerController.BuffsService.RemoveBuffs(shield.BlockingBuffs);
            this.perfectBlock = null;
            this.handShield = null;
        }

        private Shield CurrentShield() => this.shieldSlot.ItemData?.Item as Shield;

        private IEnumerator HandShield()
        {
            Shield shield = this.CurrentShield();
            while (this.playerController.StaminaService.HaveStamina())
            {
                this.playerController.StaminaService.DecreaseStamina(shield.StaminaUsing);
                yield return new WaitForFixedUpdate();
            }
            this.ChangeShieldActivity(false);
        }

        private IEnumerator PerfectBlockCheck()
        {
            yield return new WaitForSeconds(this.timeForPerfectBlock);
            this.perfectBlock = null;
        }

        public void SetShield(ItemData shieldData)
        {
            Shield shield = shieldData?.Item as Shield;
            if (shield == null || this.shieldSlot.ItemData == shieldData)
            {
                this.shieldSlot.RemoveItem();
                this.shieldRenderer.sprite = null;
                return;
            }
            this.shieldRenderer.sprite = shield.ShieldSprite;
            this.shieldSlot.AddItem(shieldData);
        }

        public float GetDamageWithoutBlocked(float damage)
        {
            Shield shield = this.CurrentShield();
            if (shield == null || this.handShield == null) return damage;
            float damageWithoutBlocked = Mathf.Max(0, damage - shield.DamageBlock);
            if (this.IsPerfectBlock())
            {
                this.perfectBlock = null;
                return damageWithoutBlocked >= damage * 1 - shield.MaxPercentageDamageBlock ? damageWithoutBlocked : damage * 1 - shield.MaxPercentageDamageBlock;
            }
            return damageWithoutBlocked >= damage * 1 - shield.MaxPercentageDamageBlock ? damageWithoutBlocked : damage * 1 - shield.MaxPercentageDamageBlock;
        }

        public bool IsShieldSet() => !this.shieldSlot.IsEmpty();

        public bool IsPerfectBlock() => this.perfectBlock != null;
    }
}