﻿using LasSecretRPG.Interactables;
using LasSecretRPG.Items;
using LasSecretRPG.Player;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Inventory.Chest
{
    public class ChestController : MonoBehaviour, IInteractable
    {
        public Transform Transform => this.transform;
        public List<ItemData> ItemsInChest => this.itemsInChest;
        public int ChestSlotsAmount => this.chestSlotsAmount;

        [SerializeField] private int chestSlotsAmount;

        private List<ItemData> itemsInChest = new List<ItemData>();

        private void Awake()
        {
            for (int i = 0; i < this.chestSlotsAmount; i++)
            {
                this.itemsInChest.Add(new ItemData(null, 0));
            }
        }

        public void Interact(PlayerController player)
        {
            if (!player.ChestManager.IsDisplaying())
                player.ChestManager.DisplayChest(this);
            else player.ChestManager.CloseChest(this);
        }

        public bool IsInteracting(PlayerController player) => player.ChestManager.IsDisplaying();

        public void Highlight()
        {

        }

        public void Unhighlight()
        {

        }
    }
}
