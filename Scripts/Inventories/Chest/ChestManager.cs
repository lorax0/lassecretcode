﻿using LasSecretRPG.Inventory.Slots;
using LasSecretRPG.Items;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Inventory.Chest
{
    public class ChestManager : MonoBehaviour
    {
        public Inventory Inventory { get => this.inventory; set => this.inventory = value; }
        public IEnumerable<ChestSlot> Slots => this.slots;

        [SerializeField] private ChestSlot chestSlotPrefab;
        [SerializeField] private Transform chestWindow;
        [SerializeField] private Transform chestSlotsWindow;

        private List<ChestSlot> slots = new List<ChestSlot>();
        private Inventory inventory;

        public void DisplayChest(ChestController chestController)
        {
            this.DisplaySlots(chestController);
            this.chestWindow.gameObject.SetActive(true);
        }

        private void DisplaySlots(ChestController chestController)
        {
            for (int i = 0; i < chestController.ChestSlotsAmount; i++)
            {
                if (i > this.slots.Count - 1) this.CreateSlot();
                this.slots[i].gameObject.SetActive(true);
                this.LoadSlot(this.slots[i], chestController.ItemsInChest[i]);
            }
        }

        private void LoadSlot(ChestSlot chestSlot, ItemData itemData)
        {
            if(itemData?.Item != null) chestSlot.AddItem(itemData);
        }

        public void CloseChest(ChestController chestController)
        {
            this.chestWindow.gameObject.SetActive(false);
            for (int i = 0; i < this.slots.Count; i++)
            {
                Slot slot = this.slots[i];
                chestController.ItemsInChest[i] = slot.ItemData;
                if (slot.ItemData?.Item != null) slot.RemoveItem();
            }
        }

        public bool IsDisplaying() => this.chestWindow.gameObject.activeSelf;

        private void CreateSlot()
        {
            ChestSlot spawnedSlot = Instantiate(this.chestSlotPrefab, this.chestSlotsWindow);
            this.inventory.SubscribeSlotEvents(spawnedSlot);
            this.slots.Add(spawnedSlot);
        }
    }
}
