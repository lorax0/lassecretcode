﻿using LasSecretRPG.Items;
using UnityEngine;

namespace LasSecretRPG.Inventory.Slots
{
    public class ChestSlot : Slot
    {
        [SerializeField] private ImageTextConnection imageTextConnection;

        public override void AddItem(ItemData itemData)
        {
            base.AddItem(itemData);
            this.imageTextConnection.UpdateSlotImage(itemData.Item.Sprite);
            this.imageTextConnection.UpdateSlotAmountText(itemData.Amount);
        }

        public override void IncreaseItemAmount(int amount)
        {
            base.IncreaseItemAmount(amount);
            this.imageTextConnection.UpdateSlotAmountText(this.itemData.Amount);
        }

        public override void DecreaseItemAmount(int amount)
        {
            base.DecreaseItemAmount(amount);
            this.imageTextConnection.UpdateSlotAmountText(this.itemData?.Amount);
        }

        public override void RemoveItem()
        {
            base.RemoveItem();
            this.imageTextConnection.UpdateSlotImage(null);
            this.imageTextConnection.UpdateSlotAmountText(null);
        }
    }
}
