﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using LasSecretRPG.Inputs;
using UnityEngine.InputSystem;
using LasSecretRPG.Player;
using LasSecretRPG.Quest;
using LasSecretRPG.Items;
using LasSecretRPG.Inventory.Slots;
using LasSecretRPG.Inventory.Chest;
using LasSecretRPG.Items.Armory;
using LasSecretRPG.Items.Weapon;
using System;

namespace LasSecretRPG.Inventory
{
    public class Inventory : MonoBehaviour
    {
        public PlayerController PlayerController
        {
            get => this.playerController;
            set
            {
                this.playerController = value;
                this.hotbar.PlayerController = value;
            }
        }
        public IEnumerable<InventorySlot> InventorySlots => this.inventorySlots;
        public Hotbar.Hotbar Hotbar => this.hotbar;
        public ShieldController ShieldController => this.shieldController;

        [SerializeField] private List<InventorySlot> inventorySlots;
        [SerializeField] private List<ArmorySlot> amorySlots;
        [SerializeField] private Hotbar.Hotbar hotbar;
        [SerializeField] private TooltipController tooltipController;
        [SerializeField] private Image itemMovePrefab;
        [SerializeField] private bool hotbarBlock;
        [SerializeField] private InputActionReference quickStackAction;
        [SerializeField] private ShieldController shieldController;

        private SwapManager swapManager;
        private QuestManager questManager;
        private PlayerController playerController;

        private void Start()
        {
            this.questManager = QuestManager.Instance;
            this.SetSlots();
            this.CreateTooltip();
            this.shieldController.Initialize(this.playerController);
        }

        private void CreateTooltip()
        {
            this.tooltipController.Tooltip.SetActive(false);
        }

        private void SetSlots()
        {
            this.inventorySlots.InsertRange(0, this.hotbar.Slots);
            foreach (var slot in this.inventorySlots)
            {
                this.SubscribeSlotEvents(slot);
            }
            foreach (var slot in this.amorySlots)
            {
                slot.OnArmorWear += (int armorPoints) => this.playerController.HealthService.IncreaseArmorPoints(armorPoints);
                slot.OnArmorUnwear += (int armorPoints) => this.playerController.HealthService.DecreaseArmorPoints(armorPoints);
                this.SubscribeSlotEvents(slot);
            }
        }

        public void SubscribeSlotEvents(Slot slot)
        {
            slot.OnBeginDragEvent += this.OnBeginDragItemEvent;
            slot.OnDragEvent += this.OnDragItemEvent;
            slot.OnEndDragEvent += this.OnEndDragItemEvent;
            slot.OnPointerEnterEvent += this.OnPointerEnterEvent;
            slot.OnPointerExitEvent += this.OnPointerExitEvent;
            slot.OnPointerClickEvent += this.OnPointerClickEvent;
        }

        public bool HaveItems(IEnumerable<ItemData> neededItems)
        {
            foreach(var itemData in neededItems)
            {
                if (!this.HaveItem(itemData)) return false;
            }
            return true;
        }
        
        public void RemoveItems(IEnumerable<ItemData> neededItems)
        {
            foreach (var itemData in neededItems)
            {
                this.RemoveItem(itemData);
            }
        }

        private bool HaveItem(ItemData itemData)
        {
            foreach(var slot in this.inventorySlots)
            {
                if (slot.ItemData?.Item == itemData.Item && slot.ItemData?.Amount >= itemData.Amount) return true;
            }
            return false;
        }

        public bool HaveItem(Item item, int amount)
        {
            foreach (var slot in this.inventorySlots)
            {
                if (slot.ItemData?.Item == item && slot.ItemData?.Amount >= amount) return true;
            }
            return false;
        }

        private void RemoveItem(ItemData itemData)
        {
            foreach(var slot in this.inventorySlots)
            {
                if (slot.ItemData?.Item == itemData.Item && slot.ItemData?.Amount >= itemData.Amount)
                {
                    slot.DecreaseItemAmount(itemData.Amount);
                    return;
                }
            }
        }

        public void DestroyWorldItem(ItemController itemController) => Destroy(itemController.gameObject);

        public bool CanAddItem(ItemData itemData) => this.inventorySlots.Where(t => t.CanMatchItem(itemData)).FirstOrDefault() != null || this.inventorySlots.Where(t => t.IsEmpty()).FirstOrDefault() != null;

        public void AddItem(ItemData itemData)
        {
            Slot slotWithMatchingItem = this.inventorySlots.Where(t => t.CanMatchItem(itemData)).FirstOrDefault();
            if (slotWithMatchingItem != null)
            {
                slotWithMatchingItem.IncreaseItemAmount(itemData.Amount);
                this.CheckQuests();
                return;
            }
            Slot emptySlot = this.GetEmptySlot();
            if(emptySlot != null)
            {
                emptySlot.AddItem(itemData);
                this.CheckQuests();
            }
        }

        public void DestroyTooltip() => this.tooltipController.Tooltip.SetActive(false);
        
        private Slot GetEmptySlot()
        {
            IEnumerable<Slot> emptySlots = this.inventorySlots.Where(t => t.IsEmpty());
            if (this.hotbarBlock)
            {
                Slot emptySlot = emptySlots.Where(t => (t as HotbarSlot) == null).FirstOrDefault();
                return emptySlot == null ? emptySlots.FirstOrDefault() : emptySlot;
            }
            return emptySlots.FirstOrDefault();
        }

        private void OnBeginDragItemEvent(PointerEventData eventData)
        {
            this.swapManager = new SwapManager(eventData, this.itemMovePrefab, this.transform);
            Slot slot = this.swapManager.GetComponentFromEventData<Slot>(eventData);   
            this.OnPointerExitEvent(slot);
        }

        private void OnDragItemEvent(PointerEventData eventData) => this.swapManager?.MoveItemImage(eventData);

        private void OnEndDragItemEvent(PointerEventData eventData)
        {
            this.swapManager.Swap(eventData);
            Slot newSlot = this.swapManager.GetComponentFromEventData<Slot>(eventData);
            this.swapManager = null;
            if (newSlot == null) return;
            this.OnPointerEnterEvent(newSlot);
        }
        
        private void OnPointerEnterEvent(Slot slot)
        {
            //TODO: Add animation
            if (this.swapManager != null || slot.ItemData == null) return; //In future maybe add comparision of items
            this.tooltipController.Tooltip.SetText(slot.ItemData.GetInfoDisplayText());
        }

        private void OnPointerExitEvent(Slot slot)
        {
            //TODO: Add animation
            if (slot?.ItemData == null) return;
            this.tooltipController.Tooltip.SetActive(false);
        }

        private void OnPointerClickEvent(Slot slot)
        {
            if (InputActionManager.IsPressedButton(this.quickStackAction.action))
            {
                Slot newSlot = this.GetQuickStackSlot(slot);
                if (newSlot == null) return;
                newSlot.AddItem(slot.ItemData);
                slot.RemoveItem();
            }

            this.OnPointerExitEvent(slot);
        }

        private Slot GetQuickStackSlot(Slot slot)
        {
            if (this.playerController.ChestManager.IsDisplaying())
            {
                if (slot is ChestSlot) return this.inventorySlots.FirstOrDefault(t => t.CanAdd(slot.ItemData));
                else return this.playerController.ChestManager.Slots.FirstOrDefault(t => t.CanAdd(slot.ItemData));
            }
            else if (slot.ItemData.Item is IArmor && !(slot is ArmorySlot)) return this.amorySlots.FirstOrDefault(t => t.CanAdd(slot.ItemData));
            else return this.inventorySlots.FirstOrDefault(t => t.CanAdd(slot.ItemData));
            
        }

        public void SortInventory()
        {
            IEnumerable<ItemData> sortedItems = this.inventorySlots.Select(t => t.ItemData).Where(t => t?.Item != null).OrderBy(t => t.Item.GetType().Name).
                ThenBy(t => t.Item.name).ToList();

            for (int i = 0; i < this.inventorySlots.Count(); i++)
            {
                InventorySlot slot = this.inventorySlots.ElementAt(i);
                if (!slot.IsEmpty()) slot.RemoveItem();
                if (i < sortedItems.Count())
                {
                    ItemData itemData = sortedItems.ElementAt(i);
                    if (itemData?.Item != null) slot.AddItem(itemData);
                }
            }
        }

        private void CheckQuests()
        {
            IEnumerable<QuestData> quests = this.questManager.ActiveQuests.Where(t => this.IsCorrectQuest(t)).ToList();
            foreach (var quest in quests)
            {
                if (quest == null) continue;
                this.questManager.FinishQuest(quest);
            }
        }

        private bool IsCorrectQuest(QuestData questData)
        {
            if (questData.Type != QuestNodeType.Item) return false;
            if (!this.HaveItem(questData.Item, questData.AmountOfItem)) return false;
            return true;
        }
        
    }
}
