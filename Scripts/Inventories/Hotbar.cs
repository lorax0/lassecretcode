﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using LasSecretRPG.Player;
using System;
using LasSecretRPG.Buildings;
using LasSecretRPG.Items.Weapon;
using LasSecretRPG.Items;
using LasSecretRPG.Inventory.Slots;

namespace LasSecretRPG.Inventory.Hotbar
{
    public class Hotbar : MonoBehaviour
    {
        public IEnumerable<HotbarSlot> Slots => this.slots;
        public PlayerController PlayerController { get => this.playerController; set => this.playerController = value; }

        [SerializeField] private List<HotbarSlot> slots;
        [SerializeField] private Transform firePoint;
        [SerializeField] private InputActionReference hotbar;
        [SerializeField] private InputActionReference mouseScroll;
        [SerializeField] private InputActionReference useSlot;
        [SerializeField] private ToolsHandler toolsHandler;
        [SerializeField] private float cooldownOfChangingItem;
        [SerializeField] private PlayerController playerController;

        private float counter;
        private HotbarSlot currentSlot;

        private void Start()
        {
            this.SetActions();
            this.SetHotbarSlots();
        }

        private void SetHotbarSlots()
        {
            foreach(var slot in this.slots)
            {
                slot.OnItemRemoved += () => this.toolsHandler.ChangeTool(null);
                slot.OnItemRemoved += this.playerController.BuildingService.DisablePreview;
            }
        }

        private void SetActions()
        {
            this.hotbar.action.performed += action => this.HandleKeyboardInput(int.Parse(action.control.name));
            this.mouseScroll.action.performed += scrollInput => this.HandleMouseInput(scrollInput.ReadValue<float>());
            this.useSlot.action.performed += action => this.UseItem();
        }

        private void Update()
        {
            if (this.counter > 0f) this.counter -= Time.deltaTime;
            
        }

        private void OnEnable()
        {
            this.hotbar.asset.Enable();
        }

        private void OnDisable()
        {
            this.hotbar.asset.Disable();
        }
        
        private void UseItem()
        {
            if (this.currentSlot == null || this.counter > 0f) return;
            var currentItem = this.currentSlot.ItemData?.Item;
            if (currentItem == null) return;
            if(currentItem is IRangeWeapon rangeWeapon) this.Shoot(this.currentSlot, currentItem, this.GetAmmunition(rangeWeapon));
            else this.currentSlot.Use(this.firePoint, this.playerController);
            if (currentItem is IMeleeWeapon meleeWeapon) this.toolsHandler.PlayAttackAnimation(meleeWeapon, this.playerController.Inventory);
            this.counter = this.currentSlot.ItemData == null ? 0: this.currentSlot.ItemData.Item.UseCooldown;
        }

        private void Shoot(HotbarSlot currentSlot, Item currentItem, Ammunition ammunition) => currentSlot.Use(this.firePoint, this.playerController, ammunition);

        private Ammunition GetAmmunition(IRangeWeapon item)
        {
            IEnumerable<Slot> slots = this.playerController.Inventory.InventorySlots;
            IEnumerable<Ammunition> possibleAmmunition = item.AmmunitionNeeded;
            Slot slot = slots.Where(t => !t.IsEmpty() && (t.ItemData.Item is Ammunition) && possibleAmmunition.Contains(t.ItemData.Item)).FirstOrDefault();
            if (slot == null) return null;
            var ammuniton = slot.ItemData.Item as Ammunition;
            slot.DecreaseItemAmount(1);
            return ammuniton;
        }

        private void HandleMouseInput(float mouseScrollY)
        {
            var currentIndex = this.GetIndexCurrentSlot();
            if (mouseScrollY > 0)
            {
                if (currentIndex + 1 < this.slots.Count)
                {
                    this.SetHotbar(currentIndex + 1);
                }
                else
                {
                    this.SetHotbar(0);
                }
            }

            if (mouseScrollY < 0)
            {
                if (currentIndex - 1 >= 0)
                {
                    this.SetHotbar(currentIndex - 1);
                }
                else
                {
                    this.SetHotbar(this.slots.Count - 1);
                }
            }
        }
        
        private ItemData GetCurrentItem() => this.currentSlot.ItemData;

        private int GetIndexCurrentSlot() => this.slots.IndexOf(this.currentSlot);

        private void HandleKeyboardInput(int input)
        {
            var newInput = input == 0 ? 9 : input - 1;
            this.SetHotbar(newInput);
        }

        private void SetHotbar(int index)
        {
            var newCurrentSlot = this.slots[index];
            if (this.currentSlot == newCurrentSlot) return;

            this.currentSlot?.Animator?.SetBool("using", false);

            this.currentSlot = newCurrentSlot;
            newCurrentSlot.Animator.SetBool("using", true);
            this.toolsHandler.ChangeTool(this.currentSlot?.ItemData?.Item as IToolVisible);
            this.ChangeCooldownForNewItem();

            if(newCurrentSlot.ItemData?.Item is Building building)
                this.playerController.BuildingService.EnablePreview(building);
            else 
                this.playerController.BuildingService.DisablePreview();
        }

        private void ChangeCooldownForNewItem()
        {
            this.counter += this.cooldownOfChangingItem;
        }
    }
}
