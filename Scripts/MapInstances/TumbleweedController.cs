using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace LasSecretRPG.MapInstances
{
    public class TumbleweedController : MonoBehaviour
    {
        [SerializeField] private new Rigidbody2D rigidbody;
        [SerializeField] private float movementSpeed;
        [SerializeField] private TumbleweedManager tumbleweedManager;

        private Vector2 direction;

        private void Start()
        {
            this.tumbleweedManager.TumbleweedControllers.Add(this);
        }

        public void MoveTumbleweed(Vector2 direction)
        {
            this.direction = direction;
        }

        private void Update()
        {
            this.transform.position += (Vector3)this.direction.normalized * this.movementSpeed * Time.deltaTime;
        }
    }
}