using LasSecretRPG.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.MapInstances
{
    public class TumbleweedManager : MonoBehaviour
    {
        public List<TumbleweedController> TumbleweedControllers => this.tumbleweedControllers;

        [SerializeField] private float minTime;
        [SerializeField] private float maxTime;

        private List<TumbleweedController> tumbleweedControllers = new List<TumbleweedController>();
        private Coroutine wind;

        private void Start()
        {
            this.AddWind();
        }

        private void AddWind()
        {
            if(this.wind != null) this.StopCoroutine(this.wind);
            this.wind = this.StartCoroutine(this.MoveTumbleweeds());
        }

        private IEnumerator MoveTumbleweeds()
        {
            Vector2 direction = this.GetRandomDirection();
            foreach (var tumbleweed in this.tumbleweedControllers)
            {
                tumbleweed.MoveTumbleweed(direction);
            }
            yield return new WaitForSeconds(this.RandomSeconds());
            this.AddWind();
        }

        private float RandomSeconds() => RandomGenerator.RandomRange(this.minTime, this.maxTime);

        private Vector2 GetRandomDirection()
        {
            float random = RandomGenerator.RandomRange(0f, 260f);
            return new Vector2(Mathf.Cos(random), Mathf.Sin(random));
        }
    }
}