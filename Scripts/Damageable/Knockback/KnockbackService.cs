﻿using LasSecretRPG.Items.Armory;
using LasSecretRPG.Utilities;
using System;
using System.Collections;
using System.Timers;
using UnityEngine;

namespace LasSecretRPG.Damageable.Knockback
{
    [Serializable]
    public class KnockbackService
    {
        public Action<Vector2, float> OnKnock;

        public Transform Transform { get => this.transform; set => this.transform = value; }
        public Shield Shield { get => this.shield; set => this.shield = value; }

        [SerializeField] private ResistanceKnockbackTier resistanceKnockbackTier;
        [SerializeField] private float knockbackPower = 1f;

        private Transform transform;
        private float knockbackChanceDebuffPooler;
        private Timer timer;
        private Shield shield;

        public void CheckKnockback(Vector3 enemyPosition, PowerKnockbackTier powerKnockbackTier)
        {
            ResistanceKnockbackTier resistanceTier = this.GetRestistanceTier();
            int knockbackTierDifference = resistanceTier.TierIndex - powerKnockbackTier.TierIndex;
            if (knockbackTierDifference >= 2) return;
            bool isKnocking = knockbackTierDifference >= 0 ? RandomGenerator.RandomRange(0f, 100f) > resistanceTier.ResistancePercentage - this.knockbackChanceDebuffPooler : true;

            if (isKnocking) this.Knock(enemyPosition, powerKnockbackTier);
            else this.IncreaseKnockbackChance();
        }

        private void IncreaseKnockbackChance()
        {
            ResistanceKnockbackTier resistanceTier = this.GetRestistanceTier();
            this.knockbackChanceDebuffPooler += resistanceTier.KnockbackChanceDebuff;
            this.timer = new Timer();
            this.timer.Elapsed += (object source, ElapsedEventArgs e) => this.ResetDebuffs();
            this.timer.Interval = resistanceTier.DebuffTime * 1000f;
            this.timer.Enabled = true;
        }

        private void Knock(Vector3 enemyPosition, PowerKnockbackTier powerKnockbackTier)
        {
            ResistanceKnockbackTier resistanceTier = this.GetRestistanceTier();
            int tierDifference = powerKnockbackTier.TierIndex - resistanceTier.TierIndex;
            float bonusKnockbackPower = tierDifference > 0 ? 1 + powerKnockbackTier.GetBonusKnockbackPower(tierDifference) : 1f;
            Vector3 directory = (this.transform.position - enemyPosition).normalized;
            //TODO: Knockback animation
            this.OnKnock?.Invoke(directory * this.knockbackPower * bonusKnockbackPower, resistanceTier.Duration);
            this.ResetDebuffs();
        }

        private void ResetDebuffs()
        {
            this.knockbackChanceDebuffPooler = 0f;
            this.timer = null;
        }

        public IEnumerator Push(Vector2 direction, float duration)
        {
            Rigidbody2D rigidbody = this.transform.GetComponent<Rigidbody2D>();
            rigidbody.velocity = direction;
            while (duration > 0)
            {
                yield return new WaitForEndOfFrame();
                duration -= Time.deltaTime;
            }
            rigidbody.velocity = Vector2.zero;
        }

        private ResistanceKnockbackTier GetRestistanceTier() => this.shield != null ? this.shield.ResistanceKnockbackTier : this.resistanceKnockbackTier;
    }
}