﻿using UnityEngine;

namespace LasSecretRPG.Damageable.Knockback
{
    public abstract class KnockbackTier : ScriptableObject
    {
        public int TierIndex => this.tierIndex;

        [SerializeField] private int tierIndex;
    }
}
