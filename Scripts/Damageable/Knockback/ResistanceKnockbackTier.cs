﻿using UnityEngine;

namespace LasSecretRPG.Damageable.Knockback
{
    [CreateAssetMenu(fileName = "NewResistanceKnockbackTier", menuName = "Damageable/ResistanceKnockbackTier")]
    public class ResistanceKnockbackTier : KnockbackTier
    {
        public float ResistancePercentage => this.resistancePercentage;
        public float KnockbackChanceDebuff => this.knockbackChanceDebuff;
        public float DebuffTime => this.debuffTime;
        public float Duration => this.duration;

        [Range(0, 100)] [SerializeField] private float resistancePercentage;
        [Range(0, 100)] [SerializeField] private float knockbackChanceDebuff;
        [SerializeField] private float debuffTime;
        [SerializeField] private float duration;
    }
}
