﻿using UnityEngine;

namespace LasSecretRPG.Damageable.Knockback
{
    [CreateAssetMenu(fileName = "NewPowerKnockbackTier", menuName = "Damageable/PowerKnockbackTier")]
    public class PowerKnockbackTier : KnockbackTier
    {
        [SerializeField] private float bonusPowerPercentageOnTierDifference;

        public float GetBonusKnockbackPower(int tierDifference) => (this.bonusPowerPercentageOnTierDifference * tierDifference) / 100;
    }
}
