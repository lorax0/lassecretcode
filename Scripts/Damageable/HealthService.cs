﻿using LasSecretRPG.Damageable.Dropping;
using LasSecretRPG.Damageable.Knockback;
using LasSecretRPG.Items;
using LasSecretRPG.UI;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Damageable
{
    [Serializable]
    public class HealthService
    {
        public Action<IEnumerable<PercentageDropBonus>> OnDieEvent;
        public Action<Vector3, PowerKnockbackTier, float> OnTakeDamageEvent;
        public Func<float, float> GetDamageWithoutBlocked;

        public Transform Transform { get => this.transform; set => this.transform = value; }
        public bool IsBlocking { get => this.isBlocking; set => this.isBlocking = value; }

        [SerializeField] private float currentHealth;
        [SerializeField] private float maxHealth;
        [SerializeField] private ImageBar healthBar;

        private int armorPoints;
        private Transform transform;
        private bool isBlocking;

        public void TakeDamage(float damage, Vector3 killerPosition, IEnumerable<PercentageDropBonus> percentageDropBonuses, PowerKnockbackTier enemyKnockbackTier)
        {
            if (this.isBlocking) return;
            this.OnTakeDamageEvent?.Invoke(killerPosition, enemyKnockbackTier, damage);

            this.TakeDamage(damage);

            if (this.healthBar.IsSet) this.healthBar.UpdateBar(this.currentHealth, this.maxHealth);
        }

        public void TakeDamage(float damage)
        {
            float damageWithoutBlocked = damage;
            if (this.GetDamageWithoutBlocked != null)
                damageWithoutBlocked = this.GetDamageWithoutBlocked.Invoke(damage);

            this.currentHealth = Mathf.Max(this.currentHealth - damageWithoutBlocked, 0);
            if (this.currentHealth <= 0) this.Die(null);
        }
        
        public void Die(IEnumerable<PercentageDropBonus> percentageDropBonuses)
        {
            this.OnDieEvent?.Invoke(percentageDropBonuses);
            MonoBehaviour.Destroy(this.transform.gameObject);
        }

        public void IncreaseArmorPoints(int armorPoints)
        {
            this.armorPoints += armorPoints;
        }

        public void DecreaseArmorPoints(int armorPoints)
        {
            this.armorPoints -= armorPoints;
        }

        public void IncreaseHealth(float health)
        {
            this.currentHealth = Mathf.Min(this.currentHealth + health, this.maxHealth);

            if (this.healthBar.IsSet) this.healthBar.UpdateBar(this.currentHealth, this.maxHealth);
        }
    }
}