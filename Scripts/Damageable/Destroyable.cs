﻿using LasSecretRPG.Items.Tools;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

namespace LasSecretRPG.Damageable
{
    public class Destroyable : DropController
    {
        [SerializeField] protected List<Object> toolTypes;
        [SerializeField] protected int requireToolPower;
        
        public bool IsEnoughToolPower(Tool tool) => tool.Power >= this.requireToolPower;

        public bool IsCorrectToolType(Tool tool) => this.toolTypes.Where(t => t.GetType() == tool.GetType()).FirstOrDefault() != null;

    }
}
