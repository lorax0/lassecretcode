using LasSecretRPG.Damageable.Knockback;

namespace LasSecretRPG.Damageable
{
    public interface IDamageable
    {
        HealthService HealthService { get; }
    }
}
