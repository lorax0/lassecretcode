using LasSecretRPG.Utilities;
using UnityEngine;
using LasSecretRPG.Damageable.Knockback;
using System.Collections.Generic;
using LasSecretRPG.Damageable.Dropping;
using LasSecretRPG.Quest;

namespace LasSecretRPG.Damageable
{
    public class DamageableUnit : DropController
    {
        public KnockbackService KnockbackService => this.knockbackService;

        [SerializeField] protected KnockbackService knockbackService;
        
        protected DamagePopup damagePopup;

        public override void Start()
        {
            base.Start();
            this.damagePopup = DamagePopup.Instance;
            this.healthService.OnTakeDamageEvent += (Vector3 killerPosition, PowerKnockbackTier powerKnockbackTier, float damage) => this.damagePopup.DisplayText(damage.ToString(), this.transform.position);
            this.healthService.OnTakeDamageEvent += (Vector3 killerPosition, PowerKnockbackTier powerKnockbackTier, float damage) => this.knockbackService.CheckKnockback(killerPosition, powerKnockbackTier);
            this.knockbackService.OnKnock += (Vector2 direction, float duration) => this.StartCoroutine(this.knockbackService.Push(direction, duration));
            this.knockbackService.Transform = this.gameObjectToDestroy;
            this.healthService.OnDieEvent += (IEnumerable<PercentageDropBonus> percentageDropBonuses) => QuestManager.Instance.UpdateQuest(this);
        }
    }
}
