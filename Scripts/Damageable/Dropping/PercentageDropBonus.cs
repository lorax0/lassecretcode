﻿using LasSecretRPG.Items;
using System;
using UnityEngine;

namespace LasSecretRPG.Damageable.Dropping
{
    [Serializable]
    public class PercentageDropBonus
    {
        public float PercentageBonus => this.percentageBonus;
        public Item Item => this.item;

        [SerializeField] private float percentageBonus;
        [SerializeField] private Item item;
    }
}
