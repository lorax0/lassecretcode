﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using LasSecretRPG.Items;
using LasSecretRPG.Utilities;

namespace LasSecretRPG.Damageable.Dropping
{
    [Serializable]
    public class ItemDropGroup
    {
        public Item Item => this.item;

        [SerializeField] protected List<ItemDropPercentage> itemDropPercentages;
        [SerializeField] protected Item item;

        public void SortDropPercentage()
        {
            this.itemDropPercentages = this.itemDropPercentages.OrderBy(t => t.Amount).ToList();
        }

        public ItemData GetRandomizeItemData(float bonusPercentage)
        {
            int amount = 0;
            for (int i = 0; i < this.itemDropPercentages.Count; i++)
            {
                float randomNumber = RandomGenerator.RandomRange(0f, 100f) + bonusPercentage;
                if(randomNumber <= this.itemDropPercentages[i].Percentage)
                {
                    amount = this.itemDropPercentages[i].Amount;
                }
            }
            return amount == 0 ? null : new ItemData(this.item, amount);
        }
    }
}