﻿using System;
using UnityEngine;

namespace LasSecretRPG.Damageable.Dropping
{
    [Serializable]
    public class ItemDropPercentage
    {
        public int Amount => this.amount;
        public float Percentage => this.percentage;

        [Min(1)] [SerializeField] protected int amount = 1;
        [SerializeField] protected float percentage;
    }
}