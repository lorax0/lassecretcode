﻿using LasSecretRPG.Damageable.Dropping;
using LasSecretRPG.Items;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LasSecretRPG.Damageable
{
    public abstract class DropController : MonoBehaviour, IDamageable
    {
        public HealthService HealthService => this.healthService;
    
        [SerializeField] protected Transform gameObjectToDestroy;
        [SerializeField] protected List<ItemDropGroup> itemDropGroups;
        [SerializeField] protected HealthService healthService;

        public virtual void Start()
        {
            this.healthService.OnDieEvent += (IEnumerable<PercentageDropBonus> percentageDropBonuses) => this.RandomizeItems(percentageDropBonuses);
            this.healthService.Transform = this.gameObjectToDestroy;
            this.InitializeItemDrop();
        }

        private void InitializeItemDrop()
        {
            foreach (var itemDropGroup in this.itemDropGroups)
            {
                itemDropGroup.SortDropPercentage();
            }
        }

        private void RandomizeItems(IEnumerable<PercentageDropBonus> percentageDropBonuses)
        {
            foreach (var itemDropGroup in this.itemDropGroups)
            {
                PercentageDropBonus percentageDropBonus = percentageDropBonuses.FirstOrDefault(t => t.Item == itemDropGroup.Item);
                float percentageBonus = percentageDropBonus == null ? 0 : percentageDropBonus.PercentageBonus;
                ItemData itemToDrop = itemDropGroup.GetRandomizeItemData(percentageBonus);
                itemToDrop.DropItem(this.transform.position);
            }
        }
    }
}