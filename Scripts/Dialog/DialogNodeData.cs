﻿using LasSecretRPG.Quest;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Dialog
{
    [Serializable]
    public class DialogNodeData
    {
        public string NodeId;
        public string DialogText;
        public float DialogTextTime;
        public Vector2 Position;
        public QuestData Quest;
    }
}