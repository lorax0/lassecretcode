﻿using System;
using System.Collections.Generic;

namespace LasSecretRPG.Dialog
{
    [Serializable]
    public class NodeLinkData
    {
        public string BaseNodeId;
        public string PortName;
        public float DialogTextTime;
        public string TargetNodeId;
    }
}