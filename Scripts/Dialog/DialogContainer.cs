﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Dialog
{
    [Serializable]
    public class DialogContainer : ScriptableObject
    {
        public List<NodeLinkData> NodeLinks = new List<NodeLinkData>();
        public List<DialogNodeData> DialogNodeData = new List<DialogNodeData>();

    }
}