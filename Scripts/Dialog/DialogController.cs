using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Dialog
{
    public class DialogController : MonoBehaviour
    {
        [SerializeField] protected DialogContainer dialog;

        protected DialogManager dialogManager;

        private void Start()
        {
            this.dialogManager = DialogManager.Instance;
            this.Interact();
        }

        public void Interact()
        {
            this.dialogManager.DisplayDialog(this.dialog);
        }
    }
}