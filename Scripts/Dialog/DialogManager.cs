using LasSecretRPG.Quest;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace LasSecretRPG.Dialog
{
    public class DialogManager : MonoBehaviour
    {
        public static DialogManager Instance => instance;
        protected static DialogManager instance;

        [SerializeField] protected GameObject dialogWindow;
        [SerializeField] protected GameObject messageContent;
        [SerializeField] protected GameObject answersContent;
        [SerializeField] protected TextMeshProUGUI messagePrefab;
        [SerializeField] protected TextMeshProUGUI answerPrefab;

        protected QuestManager questManager;

        private void Awake()
        {
            this.SetSingleton();
        }

        private void Start()
        {
            this.questManager = QuestManager.Instance;
        }

        private void SetSingleton()
        {
            if (instance == null) instance = this;
            else Destroy(this.gameObject);
        }

        public void DisplayDialog(DialogContainer dialog)
        {
            this.dialogWindow.SetActive(true);
            var firstMessage = dialog.NodeLinks.First();
            this.StartCoroutine(this.CreateMessage(dialog, firstMessage));
        }

        private IEnumerator CreateMessage(DialogContainer dialog, NodeLinkData nodeLinkData)
        {
            var dialogNode = this.GetDialogById(nodeLinkData.TargetNodeId, dialog);
            var answers = this.GetPossiblyAnswers(dialog, nodeLinkData);
            if (!string.IsNullOrEmpty(nodeLinkData.PortName))
            {
                Instantiate(this.messagePrefab, this.messageContent.transform).text = nodeLinkData.PortName;
                yield return new WaitForSeconds(nodeLinkData.DialogTextTime);
            }
            Instantiate(this.messagePrefab, this.messageContent.transform).text = dialogNode.DialogText;
            yield return new WaitForSeconds(dialogNode.DialogTextTime);
            if (dialogNode.Quest != null) this.AddQuestToActive(dialogNode.Quest);
            if (answers.Count() != 0) this.CreateAnswers(answers, dialog);
            else this.EndDialog();
        }

        private IEnumerable<NodeLinkData> GetPossiblyAnswers(DialogContainer dialog, NodeLinkData nodeLinkData)
        {
            var answers = this.GetAnswersById(nodeLinkData.TargetNodeId, dialog).Where(t => this.CanBeAnswer(t, dialog));

            return answers;
        }

        private bool CanBeAnswer(NodeLinkData answer, DialogContainer dialog)
        {
            var nextNode = this.GetDialogById(answer.TargetNodeId, dialog);
            if (nextNode.Quest == null) return true;
            return this.questManager.IsUnlock(nextNode.Quest);
        }

        private void AddQuestToActive(QuestData questData)
        {
            this.questManager.AddQuest(questData);
        }

        private void EndDialog()
        {
            this.ClearAnswerContent();
            this.ClearMessageContent();
            this.dialogWindow.SetActive(false);
        }

        private void ClearMessageContent()
        {
            Transform messageContentTransform = this.messageContent.transform;
            for (int i = 0; i < messageContentTransform.childCount; i++)
            {
                var child = messageContentTransform.GetChild(i);
                Destroy(child.gameObject);
            }
        }

        private void ClearAnswerContent()
        {
            Transform answersContentTransform = this.answersContent.transform;
            for (int i = 0; i < answersContentTransform.childCount; i++)
            {
                var child = answersContentTransform.GetChild(i);
                Destroy(child.gameObject);
            }
        }

        private void Answer(NodeLinkData answer, DialogContainer dialog)
        {
            this.ClearAnswerContent();
            this.StartCoroutine(this.CreateMessage(dialog, answer));
        }

        private void CreateAnswers(IEnumerable<NodeLinkData> answers, DialogContainer dialog)
        {
            foreach(var answer in answers)
            {
                var answerPrefab = Instantiate(this.answerPrefab, this.answersContent.transform);
                answerPrefab.text = answer.PortName;
                answerPrefab.GetComponent<Button>().onClick.AddListener(() => this.Answer(answer, dialog));
            }
        }

        private DialogNodeData GetDialogById(string id, DialogContainer dialog) => dialog.DialogNodeData.Find(x => x.NodeId == id);

        private IEnumerable<NodeLinkData> GetAnswersById(string id, DialogContainer dialog) => dialog.NodeLinks.Where(x => x.BaseNodeId == id);
    }
}