﻿using LasSecretRPG.Enemies;
using LasSecretRPG.Items;
using System;
using UnityEngine;

namespace LasSecretRPG.Quest
{
    [Serializable]
    public class QuestNodeData
    {
        public string NodeId;
        public string QuestName;
        public Vector2 Position;
        public QuestNodeType questType;

        public Item Item;
        public int AmountOfItem;
        public int AmountOfEnemy;
        public GameObject UnitToKill;
        public GameObject Waypoint;

        public void SetNodeData(IQuestNode node)
        {
            this.NodeId = node.Id;
            this.QuestName = node.Title;
            this.Position = node.Position;
            this.questType = node.Type;
            this.Item = node.Item;
            this.AmountOfItem = node.AmountOfItem;
            this.AmountOfEnemy = node.AmountOfEnemy;
            this.UnitToKill = node.UnitToKill;
            this.Waypoint = node.Waypoint;
        }
    }
}