using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Quest
{
    public enum QuestNodeType
    {
        Default, Item, Kill, Waypoint
    }
}