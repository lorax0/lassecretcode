﻿using LasSecretRPG.Damageable;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace LasSecretRPG.Quest
{
    public class QuestManager : MonoBehaviour
    {
        public static QuestManager Instance => instance;
        protected static QuestManager instance;

        public List<QuestData> ActiveQuests => this.activeQuests;

        [SerializeField] protected List<QuestData> activeQuests = new List<QuestData>();

        protected QuestContainer questContainer;

        private void Awake()
        {
            this.SetSingleton();
        }

        private void Start()
        {
            this.SetQuestContainer();
        }

        public void FinishQuest(QuestData quest)
        {
            quest.IsFinished = true;
            this.activeQuests.Remove(quest);
            Debug.Log("Finished" + quest.name);
        }

        public void UpdateQuest(QuestData quest)
        {
            quest.AmountOfKilledEnemy++;
            if (quest.AmountOfKilledEnemy >= quest.AmountOfEnemy) this.FinishQuest(quest);
        }

        private void SetQuestContainer() => this.questContainer = Resources.LoadAll("GameData/Quests", typeof(QuestContainer)).Select(t => (QuestContainer)t).FirstOrDefault();

        private void SetSingleton()
        {
            if (instance == null) instance = this;
            else Destroy(this.gameObject);
        }

        public void AddQuest(QuestData questData)
        {
            this.activeQuests.Add(questData);
        }

        public bool IsUnlock(QuestData quest)
        {
            var linkNode = this.questContainer.NodeLinks.Where(t => t.TargetNodeId == quest.NodeId).FirstOrDefault();

            if (linkNode.TargetNodeId == this.questContainer.QuestNodeData[0].NodeId) return true;
            var nodeWithPrimaryQuest = this.questContainer.QuestNodeData.Where(t => t.NodeId == linkNode.BaseNodeId).FirstOrDefault();
            if (nodeWithPrimaryQuest == null) Debug.LogError("Quest order is not valid");
            if (!this.GetQuestAssetFromNode(nodeWithPrimaryQuest).IsFinished) return false;
            
            return true;
        }

        public void UpdateQuest(DamageableUnit damageableUnit)
        {
            IEnumerable<QuestData> quests = this.ActiveQuests.Where(t => this.IsCorrectQuest(t, damageableUnit));
            foreach (var quest in quests)
            {
                if (quest == null) continue;
                this.UpdateQuest(quest);
            }
        }

        private bool IsCorrectQuest(QuestData questData, DamageableUnit damageableUnit)
        {
            if (questData.Type != QuestNodeType.Kill) return false;
            if (questData.UnitToKill.GetComponent<DamageableUnit>() != damageableUnit) return false;
            return true;
        }

        private QuestData GetQuestAssetFromNode(QuestNodeData nodeWithPrimaryQuest)
        {
            var quests = Resources.LoadAll("GameData/Quests", typeof(QuestData)).Select(t => (QuestData)t);
            foreach (var quest in quests)
            {
                if (quest.NodeId == nodeWithPrimaryQuest.NodeId) return quest;
            }
            return null;
        }
    }
}