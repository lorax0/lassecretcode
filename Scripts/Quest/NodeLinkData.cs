﻿using System;

namespace LasSecretRPG.Quest
{
    [Serializable]
    public class NodeLinkData
    {
        public string BaseNodeId;
        public string TargetNodeId;
    }
}