﻿using LasSecretRPG.Items;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Quest
{
    public class QuestData : ScriptableObject
    {
        public QuestNodeType Type { get => this.type; set => this.type = value; }
        public string NodeId { get => this.nodeId; set => this.nodeId = value; }
        public bool IsFinished { get => this.isFinished; set => this.isFinished = value; }
        public Item Item { get => this.item; set => this.item = value; }
        public int AmountOfItem { get => this.amountOfItem; set => this.amountOfItem = value; }
        public int AmountOfEnemy { get => this.amountOfEnemy; set => this.amountOfEnemy = value; }
        public int AmountOfKilledEnemy { get => this.AmountOfKilledEnemy; set => this.AmountOfKilledEnemy = value; }
        public GameObject UnitToKill { get => this.unitToKill; set => this.unitToKill = value; }
        public GameObject Waypoint { get => this.waypoint; set => this.waypoint = value; }

        [SerializeField] private QuestNodeType type;
        [SerializeField] private string nodeId;
        [SerializeField] private bool isFinished;
        [SerializeField] private Item item;
        [SerializeField] private int amountOfItem;
        [SerializeField] private int amountOfEnemy;
        [SerializeField] private int amountOfKilledEnemy;
        [SerializeField] private GameObject unitToKill;
        [SerializeField] private GameObject waypoint;
    }
}
