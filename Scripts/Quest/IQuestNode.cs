using LasSecretRPG.Enemies;
using LasSecretRPG.Items;
using LasSecretRPG.Quest;
using UnityEngine;

namespace LasSecretRPG.Quest
{
    public interface IQuestNode
    {
        string Id { get; set; }
        string Title { get; }
        Vector2 Position { get; }
        bool EntryPoint { get; set; }
        QuestNodeType Type { get; set; }
        Item Item { get; }
        int AmountOfItem { get; }
        int AmountOfEnemy { get; }
        GameObject UnitToKill { get; }
        GameObject Waypoint { get; }
    }
}