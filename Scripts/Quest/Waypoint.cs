﻿using LasSecretRPG.Player;
using System;
using System.Linq;
using UnityEngine;

namespace LasSecretRPG.Quest
{
    public class Waypoint : MonoBehaviour
    {
        private QuestManager questManager;

        private void Start()
        {
            this.questManager = QuestManager.Instance;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            PlayerController player = collision.GetComponent<PlayerController>();
            if (player == null) return;
            QuestData quest = this.questManager.ActiveQuests.Where(t => this.IsCorrectQuest(t)).FirstOrDefault();
            if (quest == null) return;
            this.questManager.FinishQuest(quest);
        }

        private bool IsCorrectQuest(QuestData questData)
        {
            if (questData.Type != QuestNodeType.Waypoint) return false;
            if (questData.Waypoint.transform.position != this.transform.position) return false;
            return true;
        }
    }
}
