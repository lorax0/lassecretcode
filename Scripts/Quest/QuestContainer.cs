﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Quest
{
    [Serializable]
    public class QuestContainer : ScriptableObject
    {
        public List<NodeLinkData> NodeLinks = new List<NodeLinkData>();
        public List<QuestNodeData> QuestNodeData = new List<QuestNodeData>();

    }
}