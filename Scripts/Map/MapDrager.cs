﻿using LasSecretRPG.Inputs;
using System;
using UnityEngine;

namespace LasSecretRPG.Map
{
    public class MapDrager
    {
        public Action<Vector2> OnDragging;

        private Vector2 oldMousePosition;
        private float mouseDraggingMultiplier;

        public MapDrager(float mouseDraggingMultiplier)
        {
            this.mouseDraggingMultiplier = mouseDraggingMultiplier;
            this.oldMousePosition = MouseRaycast.GetMousePosition();
        }

        public void Update()
        {
            Vector2 mousePosition = MouseRaycast.GetMousePosition();
            Vector2 direction = this.oldMousePosition - mousePosition;
            this.OnDragging?.Invoke(direction * this.mouseDraggingMultiplier);
            this.oldMousePosition = mousePosition;
        }
    }
}
