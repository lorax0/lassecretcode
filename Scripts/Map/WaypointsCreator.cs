﻿using LasSecretRPG.Inputs;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LasSecretRPG.Map
{
    public class WaypointsCreator : MonoBehaviour
    {
        public Camera MapCamera { get => this.mapCamera; set => this.mapCamera = value; }
        
        [SerializeField] private MapWaypoint waypoint;
        [SerializeField] private Transform waypointsParent;
        [SerializeField] private Button createWaypointButton;
        [SerializeField] private Image colorPreview;
        [SerializeField] private Slider redSlider;
        [SerializeField] private Slider greenSlider;
        [SerializeField] private Slider blueSlider;

        private Camera mapCamera;

        private void Start()
        {
            this.redSlider.onValueChanged.AddListener((float value) => this.ChangeColor());
            this.greenSlider.onValueChanged.AddListener((float value) => this.ChangeColor());
            this.blueSlider.onValueChanged.AddListener((float value) => this.ChangeColor());
        }

        private void ChangeColor()
        {
            Color color = new Color(this.redSlider.value / 255, this.greenSlider.value / 255, this.blueSlider.value / 255);
            this.colorPreview.color = color;
        }

        public void CreateWaypoint()
        {
            Vector2 position = this.mapCamera.ScreenToWorldPoint(this.transform.position);
            MapWaypoint waypoint = Instantiate(this.waypoint, position, Quaternion.identity, this.waypointsParent);
            waypoint.GetComponent<SpriteRenderer>().color = this.colorPreview.color;
            this.gameObject.SetActive(false);
        }
    }
}
