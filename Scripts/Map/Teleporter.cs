using LasSecretRPG.Interactables;
using LasSecretRPG.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LasSecretRPG.Map
{
    public class Teleporter : MonoBehaviour, IInteractable
    {
        public Transform Transform => this.transform;
        public Transform TeleportPosition => this.teleportPosition;

        [SerializeField] private Transform teleportPosition;

        public void Interact(PlayerController player)
        {
            player.MapController.gameObject.SetActive(true);
            player.MapController.ResetMap();
            player.MapController.ActiveTeleporting(player, this);
        }

        public void Teleport(PlayerController playerController, Teleporter teleporter)
        {
            playerController.MapController.gameObject.SetActive(false);
            playerController.transform.position = teleporter.TeleportPosition.position;
        }

        public void Highlight()
        {

        }

        public void Unhighlight()
        {

        }

        public bool IsInteracting(PlayerController player) => player.MapController.gameObject.activeSelf;
    }
}