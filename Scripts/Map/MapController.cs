using System;
using UnityEngine;
using UnityEngine.InputSystem;
using LasSecretRPG.Utilities;
using LasSecretRPG.Inputs;
using System.Linq;
using LasSecretRPG.Player;

namespace LasSecretRPG.Map
{
    public class MapController : MonoBehaviour
    {
        public Action<bool> OnMapActive;
        
        [SerializeField] private Camera mapCamera;
        [SerializeField] private WaypointsCreator waypointsCreator;
        [SerializeField] private InputActionReference zoomInput;
        [SerializeField] private InputActionReference moveInput;
        [SerializeField] private InputActionReference waypointCreateInput;
        [SerializeField] private InputActionReference waypointDeleteInput;
        [SerializeField] private InputActionReference mouseDraggingInput;
        [SerializeField] private InputActionReference teleportInput;
        [SerializeField] private float mapMovementSpeed;
        [SerializeField] private float mouseDraggingMultiplier;
        [SerializeField] private float mapZoomSpeed;
        [SerializeField] private float minZoomValue;
        [SerializeField] private int maxWaypointAmount;

        private InputManager inputManager;
        private MapDrager mapDrager;
        private bool isActiveTeleporting;
        private Teleporter selectedTeleporter;
        private PlayerController playerController;
        private const float zoomMultiplier = 0.001f;

        private void Awake()
        {
            this.inputManager = InputManager.Instance;
            this.waypointsCreator.MapCamera = this.mapCamera;
            this.zoomInput.action.performed += scrollInput => this.Zoom(scrollInput.ReadValue<float>());
            this.waypointCreateInput.action.performed += (InputAction.CallbackContext context) => this.CreateWaypoint();
            this.waypointDeleteInput.action.performed += (InputAction.CallbackContext context) => this.DeleteWaypoint();
            this.mouseDraggingInput.action.performed += (InputAction.CallbackContext context) => this.OnMouseDragging();
            this.teleportInput.action.performed += (InputAction.CallbackContext context) => this.Teleport();
        }

        private void OnEnable()
        {
            this.inputManager.ActiveActionMap("Map");
            this.OnMapActive?.Invoke(true);
        }

        private void OnDisable()
        {
            this.inputManager.ActiveActionMap("Player");
            this.OnMapActive?.Invoke(false);
        }

        private void Update()
        {
            this.CheckKeyboardMove();
            this.mapDrager?.Update();
        }

        private void Zoom(float zoomValue)
        {
            float zoom = zoomValue * zoomMultiplier * this.mapZoomSpeed;
            this.mapCamera.orthographicSize = Mathf.Max(this.minZoomValue, this.mapCamera.orthographicSize - zoom);
        }
        
        private void CheckKeyboardMove() => this.Move(this.moveInput.action.ReadValue<Vector2>().normalized);
        
        private void Move(Vector2 direction) => this.mapCamera.transform.position += (Vector3) direction * this.mapMovementSpeed* Time.deltaTime;
        
        private void OnMouseDragging()
        {
            if (this.waypointsCreator.gameObject.activeSelf) return;
            if(this.mapDrager != null)
            {
                this.mapDrager = null;
                return;
            }
            this.mapDrager = new MapDrager(this.mouseDraggingMultiplier);
            this.mapDrager.OnDragging += this.Move;
        }

        private void CreateWaypoint()
        {
            if (this.isActiveTeleporting) return;
            Vector2 position = MouseRaycast.GetMousePosition();
            this.waypointsCreator.transform.position = position;
            this.waypointsCreator.gameObject.SetActive(true);
        }

        private void DeleteWaypoint()
        {
            if (this.isActiveTeleporting) return;
            MapWaypoint waypoint = MouseRaycast.GetGameobjectsOnMousePosition<MapWaypoint>().FirstOrDefault();
            if (waypoint == null) return;
            Destroy(waypoint.gameObject);
        }

        private void Teleport()
        {
            if (!this.isActiveTeleporting) return;
            Teleporter teleporter = MouseRaycast.GetGameobjectsOnMousePosition<Teleporter>(this.mapCamera).FirstOrDefault();
            if (teleporter == null) return;
            this.isActiveTeleporting = false;
            this.mapDrager = null;
            this.selectedTeleporter.Teleport(this.playerController, teleporter);
        }

        public void ActiveTeleporting(PlayerController playerController, Teleporter teleporter)
        {
            this.isActiveTeleporting = true;
            this.selectedTeleporter = teleporter;
            this.playerController = playerController;
        }

        public void ResetMap()
        {
            this.mapCamera.transform.localPosition = Vector3.zero;
        }
    }
}