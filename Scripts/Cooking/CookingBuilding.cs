using System;
using LasSecretRPG.Buildings;
using UnityEngine;

namespace LasSecretRPG.Cooking
{
    [CreateAssetMenu(fileName = "NewCookingBuilding", menuName = "Items/Building/CookingBuilding")]
    public class CookingBuilding : Building
    {
        public Sprite CookingBuildingImage => this.CookingBuildingImage;

        [SerializeField] private Sprite cookingBuildingImage;
    }
}