﻿using LasSecretRPG.Crafting;
using LasSecretRPG.Items;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace LasSecretRPG.Cooking
{
    [Serializable]
    public class CookingService
    {
        public Action<bool> OnCookingPanelActivityChange;

        [SerializeField] private GameObject cookingPanel;
        [SerializeField] private Button sumbitButton;
        [SerializeField] private Button closeButton;
        [SerializeField] private Image progressBar;
        [SerializeField] private CookingSlot resultSlot;
        [SerializeField] private List<CookingRecipe> recipes;
        [SerializeField] private List<CookingSlot> cookingSlots;
        [SerializeField] private CookingRecipe defaultRecipe;

        public void ActiveCookingWindow(CookingController cookingController, IEnumerable<ItemData> itemsInSlots)
        {
            CookingBuilding cookingBuilding = cookingController.Building as CookingBuilding;
            this.cookingPanel.SetActive(true);
            this.OnCookingPanelActivityChange?.Invoke(true);
            this.SetCookingSlots(itemsInSlots);
            this.closeButton.onClick.AddListener(() => this.CloseCookingWindow(cookingController));
            this.SetProgress(cookingController);
        }

        private void SetCookingSlots(IEnumerable<ItemData> itemsInSlots)
        {
            if (itemsInSlots == null) return;
            for (int i = 0; i < this.cookingSlots.Count; i++)
            {
                var itemInSlot = itemsInSlots.ElementAt(i);
                if (itemInSlot == null) continue;
                this.cookingSlots[i].AddItem(itemInSlot);
            }
        }

        private void UpdateProgressBar(float progressNormalized, CookingController cookingController)
        {
            if (progressNormalized >= 1f)
            {
                this.SetResult(cookingController);
                return;
            }
            this.progressBar.fillAmount = progressNormalized;
        }

        private void SetProgress(CookingController cookingController)
        {
            if (!cookingController.IsCooking())
            {
                this.UnlockCooking(cookingController);
                return;
            }
            float progressInPercentage = cookingController.GetProgress();
            cookingController.OnProgress += (float percentage) => this.UpdateProgressBar(percentage, cookingController);
            this.LockCooking();
            this.UpdateProgressBar(progressInPercentage, cookingController);
        }

        private void UnlockCooking(CookingController cookingController)
        {
            foreach (var slot in this.cookingSlots)
            {
                slot.ChangeLock(false);
            }
            this.sumbitButton.onClick.AddListener(() => this.StartCooking(cookingController));
        }

        private void LockCooking()
        {
            foreach (var slot in this.cookingSlots)
            {
                slot.ChangeLock(true);
            }
            this.sumbitButton.onClick.RemoveAllListeners();
        }

        private void StartCooking(CookingController cookingController)
        {
            if (!this.resultSlot.IsEmpty()) return;
            foreach (var recipe in this.recipes)
            {
                if (!this.IsCorrectRecipe(recipe)) continue;
                this.StartCooking(cookingController, recipe);
                return;
            }
            if (this.cookingSlots.Where(t => !t.IsEmpty()).Count() != 0)
            {
                this.StartCooking(cookingController, this.defaultRecipe);
            }
        }

        private void StartCooking(CookingController cookingController, CookingRecipe recipe)
        {
            cookingController.StartCooking(recipe, this.cookingSlots.Select(t => t.ItemData));
            this.LockCooking();
            cookingController.OnProgress += (float percentage) => this.UpdateProgressBar(percentage, cookingController);
        }

        private void ClearSlots()
        {
            foreach (var cookingSlot in this.cookingSlots)
            {
                if (cookingSlot.IsEmpty()) continue;
                cookingSlot.RemoveItem();
            }
        }

        public void CloseCookingWindow(CookingController cookingController)
        {
            cookingController.OnProgress = null;
            this.closeButton.onClick.RemoveAllListeners();
            this.sumbitButton.onClick.RemoveAllListeners();
            if (!cookingController.IsCooking()) this.DropCookingItems(cookingController);
            else this.ClearSlots();
            this.progressBar.fillAmount = 0f;
            this.cookingPanel.SetActive(false);
            this.OnCookingPanelActivityChange?.Invoke(false);
            if (!this.resultSlot.IsEmpty()) this.resultSlot.DropItem(cookingController.transform.position);
        }

        public bool IsActiveWindow() => this.cookingPanel.activeSelf;

        private void DropCookingItems(CookingController cookingController)
        {
            foreach (var cookingSlot in this.cookingSlots)
            {
                if (cookingSlot.IsEmpty()) continue;
                cookingSlot.DropItem(cookingController.transform.position);
            }
        }

        private void SetResult(CookingController cookingController)
        {
            this.progressBar.fillAmount = 0f;
            this.ClearSlots();
            this.UnlockCooking(cookingController);
            this.resultSlot.AddItem(cookingController.CurrentRecipe.Result);
            cookingController.ItemsInSlots = null;
            cookingController.CurrentRecipe = null;
            cookingController.OnProgress = null;
        }

        private bool IsCorrectRecipe(CookingRecipe recipe)
        {
            IEnumerable<Item> neededItems = recipe.NeededItems.Select(t => t.Item).OrderBy(t => t.Name);
            IEnumerable<Item> slotItems = this.cookingSlots.Select(t => t.ItemData?.Item).Where(t => t != null).OrderBy(t => t.Name);
            if (slotItems.Count() != neededItems.Count()) return false;
            for (int i = 0; i < neededItems.Count(); i++)
            {
                if (neededItems.ElementAt(i) != slotItems.ElementAt(i)) return false;
            }
            return true;
        }

    }
}
