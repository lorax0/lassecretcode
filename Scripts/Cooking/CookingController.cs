﻿using LasSecretRPG.Buildings;
using LasSecretRPG.Interactables;
using LasSecretRPG.Items;
using LasSecretRPG.Player;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LasSecretRPG.Cooking
{
    public class CookingController : BuildingController, IInteractable
    {
        public Action<float> OnProgress;

        public CookingRecipe CurrentRecipe { get => this.currentRecipe; set => this.currentRecipe = value; }
        public IEnumerable<ItemData> ItemsInSlots { get => this.itemsInSlots; set => this.itemsInSlots = value; }
        public Transform Transform => this.transform;

        private CookingRecipe currentRecipe;
        private float counter;
        private IEnumerable<ItemData> itemsInSlots;

        public void Interact(PlayerController playerController)
        {
            if(!playerController.CookingService.IsActiveWindow())
                playerController.CookingService.ActiveCookingWindow(this, this.itemsInSlots);
            else
                playerController.CookingService.CloseCookingWindow(this);
        }

        public void StartCooking(CookingRecipe recipe, IEnumerable<ItemData> itemsInSlots)
        {
            this.itemsInSlots = itemsInSlots.ToList();
            this.StartCoroutine(this.StartCookingTimer(recipe));
        }

        public float GetProgress() => (1 - this.counter / this.currentRecipe.TimeToCook);

        private IEnumerator StartCookingTimer(CookingRecipe recipe)
        {
            //TODO: Add animation
            this.currentRecipe = recipe;
            this.counter = this.currentRecipe.TimeToCook;
            while (this.counter >= 0f)
            {
                this.counter -= Time.deltaTime;
                yield return new WaitForFixedUpdate();
                this.OnProgress?.Invoke(this.GetProgress());
            }
        }

        public bool IsCooking() => this.currentRecipe != null;

        public void Unhighlight()
        {
            //TODO: 
        }

        public void Highlight()
        {
            //TODO: 
        }

        public bool IsInteracting(PlayerController player) => player.CookingService.IsActiveWindow();
    }
}
