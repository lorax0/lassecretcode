﻿using LasSecretRPG.Inventory.Slots;
using LasSecretRPG.Items;
using UnityEngine;

namespace LasSecretRPG.Cooking
{
    public class CookingSlot : Slot
    {
        public override bool IsBlocked => this.isBlockedForAdding;

        [SerializeField] private bool isBlockedForAdding;
        [SerializeField] private ImageTextConnection imageTextConnection;

        private bool isLocked;
        
        public override void AddItem(ItemData itemData)
        {
            base.AddItem(itemData);
            this.imageTextConnection.UpdateSlotImage(itemData.Item.Sprite);
            this.imageTextConnection.UpdateSlotAmountText(itemData.Amount);
        }

        public override void IncreaseItemAmount(int amount)
        {
            base.IncreaseItemAmount(amount);
            this.imageTextConnection.UpdateSlotAmountText(this.itemData.Amount);
        }

        public override void DecreaseItemAmount(int amount)
        {
            base.DecreaseItemAmount(amount);
            this.imageTextConnection.UpdateSlotAmountText(this.itemData?.Amount);
        }

        public override void RemoveItem()
        {
            base.RemoveItem();
            this.imageTextConnection.UpdateSlotImage(null);
            this.imageTextConnection.UpdateSlotAmountText(null);
        }

        public override bool CanMatchItem(ItemData itemData) => false;
        
        public void ChangeLock(bool isLocked)
        {
            this.isLocked = isLocked;
        }

        public override bool CanAdd(ItemData itemData) => this.isLocked ? false : base.CanAdd(itemData);

        public override bool CanRemove() => !this.isLocked && base.CanRemove();
    }
}
