﻿using LasSecretRPG.Crafting;
using System;
using UnityEngine;

namespace LasSecretRPG.Cooking
{
    [CreateAssetMenu(fileName = "NewCookingRecipe", menuName = "Cooking/CookingRecipe")]
    public class CookingRecipe : Recipe
    {
        public float TimeToCook => this.timeToCook;

        [SerializeField] private float timeToCook;
    }
}
